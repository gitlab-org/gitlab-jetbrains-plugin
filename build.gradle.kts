import com.github.gradle.node.npm.task.NpmInstallTask
import com.github.gradle.node.npm.task.NpmTask
import groovy.json.JsonSlurper
import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.DetektCreateBaselineTask
import org.jetbrains.changelog.Changelog
import org.jetbrains.intellij.tasks.PrepareSandboxTask
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.dsl.KotlinVersion
import org.jetbrains.kotlin.parsing.parseBoolean
import java.net.MalformedURLException
import java.net.URL

// Read properties from gradle.properties to be used in here
fun properties(key: String) = project.findProperty(key)?.toString() ?: error("Property $key not found")

plugins {
  java
  jacoco
  alias(libs.plugins.jetbrains.kotlin.jvm)
  alias(libs.plugins.jetbrains.kotlin.serialization)
  alias(libs.plugins.jetbrains.intellij)
  alias(libs.plugins.jetbrains.changelog)
  alias(libs.plugins.gitlab.arturbosch.detekt)
  alias(libs.plugins.apollographql.apollo3)
  alias(libs.plugins.github.gmazzo.buildconfig)
  alias(libs.plugins.github.gradle.node)
  alias(libs.plugins.sentry)
}

group = "com.gitlab.plugin"

val webviewSourceDir = "${project.projectDir}/webview"
val e2eMetricsSourceDir = "${project.projectDir}/scripts/e2e-test-metrics"

// Contains configuration populated from environment variables
inner class EnvConfig {
  val alphaReleaseChannel = "alpha"

  private val ciEnv by lazy { System.getenv("CI") ?: "" }
  private val ciPipelineIidEnv by lazy { System.getenv("CI_PIPELINE_IID") ?: "" }
  private val ciCommitShaEnv by lazy { System.getenv("CI_COMMIT_SHORT_SHA") ?: "" }
  private val releaseChannelEnv by lazy { System.getenv("RELEASE_CHANNEL") ?: "" }
  private val snowPlowCollectorUrlEnv by lazy { System.getenv("SNOWPLOW_COLLECTOR_URL") ?: "" }
  private val snowPlowEmitterBatchSizeEnv by lazy { System.getenv("SNOWPLOW_EMITTER_BATCH_SIZE") ?: "" }
  private val gitLabGraphqlEndpointEnv by lazy { System.getenv("GITLAB_GRAPHQL_ENDPOINT") ?: "" }
  private val sentryTrackingEnabledEnv by lazy { System.getenv("SENTRY_TRACKING_ENABLED") ?: "true" }
  private val redirectLsLogsToConsoleEnv by lazy { System.getenv("REDIRECT_LS_LOGS_CONSOLE") ?: "" }
  private val useLocalLspEnv by lazy { System.getenv("USE_LOCAL_LSP") ?: "" }

  var releaseChannel: String = ""
  var isLocalBuild: Boolean = false
  var snowPlowCollectorUrl: String = "https://snowplowprd.trx.gitlab.net"
  var snowPlowEmitterBatchSize: Int = 50
  var gitLabGraphqlEndpoint: String = "https://gitlab.com/api/graphql"
  var sentryTrackingEnabled: Boolean = false
  var redirectLsLogsToConsole: Boolean = false
  var useLocalLsp: Boolean = false

  init {
    validateEnvVariables()
    calculatePluginVersion()

    releaseChannel = releaseChannelEnv
    isLocalBuild = ciEnv.isEmpty()

    if (snowPlowCollectorUrlEnv.isNotEmpty()) {
      snowPlowCollectorUrl = snowPlowCollectorUrlEnv
    }

    if (snowPlowEmitterBatchSizeEnv.isNotEmpty()) {
      snowPlowEmitterBatchSize = snowPlowEmitterBatchSizeEnv.toInt()
    }

    if (gitLabGraphqlEndpointEnv.isNotEmpty()) {
      gitLabGraphqlEndpoint = gitLabGraphqlEndpointEnv
    }

    if (sentryTrackingEnabledEnv.isNotEmpty()) {
      sentryTrackingEnabled = sentryTrackingEnabledEnv == "true"
    }

    if (redirectLsLogsToConsoleEnv.isNotEmpty()) {
      redirectLsLogsToConsole = parseBoolean(redirectLsLogsToConsoleEnv)
    }

    if (useLocalLspEnv.isNotEmpty()) {
      useLocalLsp = useLocalLspEnv == "true"
    }
  }

  private fun validateEnvVariables() {
    var errors = false

    if (releaseChannelEnv !in listOf("", alphaReleaseChannel)) {
      errors = true
      logger.error("Unknown release channel \"$releaseChannelEnv\" from environment variable RELEASE_CHANNEL")
    }

    if (!validateUrl(snowPlowCollectorUrl)) {
      errors = true
      logger.error(
        "Invalid Snowplow collector url \"$snowPlowCollectorUrlEnv\" from environment variable SNOWPLOW_COLLECTOR_URL"
      )
    }

    if (!validateUrl(gitLabGraphqlEndpoint)) {
      errors = true
      logger.error(
        "Invalid GitLab GraphQL endpoint: \"$gitLabGraphqlEndpoint\" from environment variable GITLAB_GRAPHQL_ENDPOINT"
      )
    }

    if (errors) {
      throw GradleException("One or more environment variables are invalid.")
    }
  }

  private fun validateUrl(url: String): Boolean {
    try {
      URL(url)
    } catch (e: MalformedURLException) {
      return false
    }
    return true
  }

  private fun calculatePluginVersion() {
    val isAlphaChannel = releaseChannelEnv == alphaReleaseChannel
    val versionSuffix = if (isAlphaChannel) {
      "-alpha.$ciPipelineIidEnv+$ciCommitShaEnv"
    } else {
      ""
    }
    version = "${properties("plugin.version")}$versionSuffix"
  }
}

val envConfig = EnvConfig()

repositories {
  mavenCentral()
  maven("https://packages.jetbrains.team/maven/p/ij/intellij-dependencies")
}

kotlin {
  compilerOptions {
    jvmTarget = JvmTarget.JVM_17
    languageVersion = KotlinVersion.KOTLIN_1_9
    apiVersion = KotlinVersion.KOTLIN_1_9
  }
}

buildConfig {
  packageName(group.toString())

  buildConfigField(
    "String",
    "PLUGIN_NAME",
    "\"${rootProject.name}\""
  )

  buildConfigField(
    "Boolean",
    "REDIRECT_LS_LOGS_CONSOLE",
    envConfig.redirectLsLogsToConsole
  )

  buildConfigField(
    "String",
    "RELEASE_CHANNEL",
    "\"${envConfig.releaseChannel}\""
  )

  buildConfigField(
    "String",
    "SNOWPLOW_COLLECTOR_URL",
    "\"${envConfig.snowPlowCollectorUrl}\""
  )

  buildConfigField(
    "Integer",
    "SNOWPLOW_EMITTER_BATCH_SIZE",
    envConfig.snowPlowEmitterBatchSize
  )

  buildConfigField(
    "Boolean",
    "SENTRY_TRACKING_ENABLED",
    envConfig.sentryTrackingEnabled && !envConfig.isLocalBuild
  )

  buildConfigField(
    "Boolean",
    "SHOW_DESIGN_SYSTEM_PREVIEW",
    envConfig.isLocalBuild
  )

  buildConfigField(
    "Boolean",
    "USE_LOCAL_LSP",
    envConfig.useLocalLsp
  )

  buildConfigField(
    "Boolean",
    "DUO_TERMINAL_ACTIONS_ENABLED",
    envConfig.isLocalBuild || envConfig.releaseChannel == envConfig.alphaReleaseChannel
  )

  buildConfigField(
    "Boolean",
    "DUO_FIX_QUICK_FIX_ENABLED",
    envConfig.isLocalBuild || envConfig.releaseChannel == envConfig.alphaReleaseChannel
  )

  buildConfigField(
    "Boolean",
    "DUO_FIX_INTENTION_ENABLED",
    envConfig.isLocalBuild || envConfig.releaseChannel == envConfig.alphaReleaseChannel
  )

  buildConfigField(
    "Boolean",
    "USE_LS_WEBVIEW",
    // This will be false until the full integration will be achieved
    false
  )
}

tasks.named("sentryCollectSourcesJava") {
  mustRunAfter("generateBuildConfig", "generateGitLabApolloSources")
}

tasks.named("generateSentryBundleIdJava") {
  mustRunAfter("generateBuildConfig", "generateGitLabApolloSources")
}

detekt {
  buildUponDefaultConfig = true // preconfigure defaults
  allRules = true // activate all available (even unstable) rules.
  config.setFrom("detekt.yml")
}

tasks.withType<Detekt>().configureEach {
  jvmTarget = JavaVersion.VERSION_17.toString()

  exclude("com/gitlab/plugin/graphql/**") // ignore generated sources

  reports {
    html.required.set(true) // observe findings in your browser with structure and code snippets
  }
}

tasks.withType<DetektCreateBaselineTask>().configureEach {
  exclude("com/gitlab/plugin/graphql/**") // ignore generated sources
}

configurations.implementation {
  exclude("org.jetbrains.kotlinx", "kotlinx-coroutines-core")
}

dependencies {
  implementation(libs.apollo.graphql.runtime) {
    // okio is still on version 3.2.0 on the latest apollo3 version (3.8.4) which causes CVE-2023-3635 scan.
    exclude(group = "com.squareup.okio", module = "okio")
  }
  implementation(libs.kotlinx.datetime)
  implementation(libs.ktor.client.auth)
  implementation(libs.ktor.client.contentNegotiation)
  implementation(libs.ktor.client.logging)
  implementation(libs.ktor.client.okhttp)
  implementation(libs.ktor.serialization.gson)
  implementation(libs.ktor.serialization.kotlinxJson)
  implementation(libs.logback.classic)
  implementation(libs.snowplow.java.tracker) {
    capabilities {
      requireCapability("com.snowplowanalytics:snowplow-java-tracker-okhttp-support")
      api(libs.jackson.datatype)
    }
  }
  implementation(libs.lsp4j)
  implementation(libs.kotlinResult)
  implementation(libs.commonmark)

  testImplementation(libs.apollo.graphql.mockserver)
  testImplementation(libs.apollo.graphql.testingSupport)
  testImplementation(libs.junit.jupiter)
  testImplementation(libs.junit.jupiter.api)
  testImplementation(libs.junit.jupiter.params)
  testImplementation(libs.kotest.assertions.core)
  testImplementation(libs.kotest.framework.datatest)
  testImplementation(libs.kotest.runner.junit5)
  testImplementation(libs.kotlin.test.junit)
  testImplementation(libs.kotlinx.coroutines.test)
  testImplementation(libs.ktor.client.mock)
  testImplementation(libs.mockk)
  testImplementation(libs.mockserver.junit.jupiter)
  testImplementation(libs.okhttp3.logging.interceptor)
  testImplementation(libs.remote.fixtures)
  testImplementation(libs.remote.robot)
  testImplementation(libs.turbine)
  testImplementation(libs.video.recorder.junit5) {
    exclude(group = "log4j", module = "log4j")
  }

  testRuntimeOnly(libs.junit.jupiter.engine)
  testRuntimeOnly(libs.junit.platform.launcher)
  testRuntimeOnly(libs.junit.platform.suite.engine)

  // Linting
  detektPlugins(libs.detekt.formatting)
}

apollo {
  service("GitLab") {
    generateDataBuilders.set(true)
    packageName.set("com.gitlab.plugin.graphql")
    introspection {
      endpointUrl = envConfig.gitLabGraphqlEndpoint
      schemaFile.set(file("src/main/graphql/schema.graphqls"))
    }

    fun mapGitLabScalar(
      graphQLName: String,
      targetName: String = "com.gitlab.plugin.graphql.scalars.$graphQLName",
      expression: String = "${targetName}Adapter",
    ) = mapScalar(graphQLName, targetName, expression)

    mapGitLabScalar("AiModelID")
    mapGitLabScalar("NamespaceID")
    mapGitLabScalar("UserID")
  }
}

// Configure Gradle IntelliJ Plugin
// Read more: https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html
intellij {
  version = properties("platform.version")
  type = properties("platform.type")

  plugins = listOf("Git4Idea", "org.jetbrains.plugins.terminal")
}

// Configure Gradle Changelog Plugin - read more: https://github.com/JetBrains/gradle-changelog-plugin
changelog {
  groups = listOf("Added", "Changed", "Removed", "Fixed")

  repositoryUrl =
    System.getenv("CI_PROJECT_URL") ?: "https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin"
}

node {
  download = !envConfig.isLocalBuild
  version = properties("node.version")
  npmInstallCommand = "ci"
  nodeProjectDir = file(webviewSourceDir)
}

tasks {
  // Set the JVM compatibility versions
  withType<JavaCompile> {
    sourceCompatibility = properties("platform.java.version")
    targetCompatibility = properties("platform.java.version")
  }

  wrapper {
    gradleVersion = properties("gradle.version")
  }

  patchPluginXml {
    sinceBuild = properties("plugin.minBuild")
    untilBuild = properties("plugin.maxBuild")

    changeNotes = provider {
      changelog.renderItem(
        changelog
          .getLatest()
          .withHeader(false)
          .withEmptySections(false),
        Changelog.OutputType.HTML
      )
    }
  }

  signPlugin {
    certificateChain = System.getenv("CERTIFICATE_CHAIN")
    privateKey = System.getenv("PRIVATE_KEY")
    password = System.getenv("PRIVATE_KEY_PASSWORD")
  }

  publishPlugin {
    token = System.getenv("PUBLISH_TOKEN")
    channels = listOf(envConfig.releaseChannel)
  }

  test {
    description = "Runs the test suite. Set project property `-Pe2eTests` to run the E2E tests."
    useJUnitPlatform()

    if (!project.hasProperty("e2eTests")) {
      filter {
        excludeTestsMatching("com.gitlab.plugin.e2eTest.*")
      }
    } else {
      testLogging {
        events("passed", "skipped", "failed", "standardOut", "standardError")
      }
      filter {
        includeTestsMatching("com.gitlab.plugin.e2eTest.*")
      }
    }
  }

  withType<Test> {
    configure<JacocoTaskExtension> {
      isIncludeNoLocationClasses = true
      excludes = listOf("jdk.internal.*")
    }
  }

  jacocoTestReport {
    reports {
      xml.required.set(true)
      html.required.set(false)
    }
    classDirectories.setFrom(instrumentCode)
  }

  jacocoTestCoverageVerification {
    classDirectories.setFrom(instrumentCode)
  }

  runPluginVerifier {
    ideVersions = listOf("${properties("platform.type")}-${properties("platform.version")}")
    localPaths = emptyList()
  }

  runIde {
    systemProperty("useLocalServer", System.getProperty("useLocalServer"))
  }

  runIdeForUiTests {
    systemProperty("ide.mac.message.dialogs.as.sheets", "false")
    systemProperty("jb.privacy.policy.text", "<!--999.999-->")

    // https://youtrack.jetbrains.com/issue/IJPL-59368/IDE-crashes-due-to-chrome-sandbox-is-owned-by-root-and-has-mode-error-when-IDE-is-launching-the-JCEF-in-a-sandbox
    systemProperty("ide.browser.jcef.sandbox.enable", "false")

    // requirement to use JCefBrowserFixture
    systemProperty("ide.browser.jcef.jsQueryPoolSize", "10000")

    // to enable debugging of browser
    systemProperty("ide.browser.jcef.debug.port", -1)
    systemProperty("idea.is.internal", "true")
    systemProperty("ide.browser.jcef.contextMenu.devTools.enabled", "true")

    systemProperty("jb.consents.confirmation.enabled", "false")
    systemProperty("ide.mac.file.chooser.native", "false")
    systemProperty("jbScreenMenuBar.enabled", "false")
    systemProperty("apple.laf.useScreenMenuBar", "false")

    systemProperty("robot-server.port", "8082") // default port 8580
    systemProperty("idea.trust.all.projects", "true")
    systemProperty("ide.show.tips.on.startup.default.value", "false")

    // Avoid slow operations assertion https://youtrack.jetbrains.com/issue/IDEA-275489
    systemProperty("ide.slow.operations.assertion", "false")
  }

  downloadRobotServerPlugin {
    version = "0.11.23"
  }

  register<NpmInstallTask>("installE2EMetrics") {
    npmCommand.set(listOf("install"))
    workingDir.set(file(e2eMetricsSourceDir))
  }

  register<NpmTask>("runE2EMetrics") {
    dependsOn("installE2EMetrics")

    workingDir.set(file(e2eMetricsSourceDir))
    npmCommand.set(listOf("run", "test:e2e:metrics"))
  }

  register<NpmInstallTask>("installWebView") {
    npmCommand.set(listOf("ci"))
    workingDir.set(file(webviewSourceDir))
    inputs.files(fileTree(webviewSourceDir))
    outputs.dir("$webviewSourceDir/dist")
  }

  // Build webview for Duo Chat
  register<NpmTask>("npmRunBuild") {
    dependsOn("installWebView")

    npmCommand.set(listOf("run", "build"))
    workingDir.set(file(webviewSourceDir))
    inputs.files(fileTree(webviewSourceDir))
    outputs.dir("$webviewSourceDir/dist")
  }

  register<Copy>("copyWebviewAssets") {
    dependsOn("npmRunBuild")
    from("$webviewSourceDir/dist")
    into("${project.projectDir}/src/main/resources/webview")
  }

  val copyLanguageServerFiles: PrepareSandboxTask.() -> Unit = {
      if (envConfig.useLocalLsp) {
          val languageServerDir = layout.projectDirectory.dir("tmp/language-server")
          if (!languageServerDir.asFile.exists()) {
            throw GradleException("tmp/language-server does not exist. Please ensure you have locally cloned the gitlab-lsp project (https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp) and are running `npm run watch -- --editor=jetbrains --editor-path=path/to/your/jetbrains/plugin/project` in that project's directory.")
          }
          from(languageServerDir) {
              into("${intellij.pluginName.get()}/lib/gitlab-lsp/tmp/language-server")
          }
      } else {
          dependsOn("lspDownloadBinaries")

          from(layout.buildDirectory.dir("gitlab-lsp/bin")) {
              into("${intellij.pluginName.get()}/lib/gitlab-lsp/bin")
          }
      }
  }

  prepareSandbox(copyLanguageServerFiles)
  prepareTestingSandbox(copyLanguageServerFiles)
  prepareUiTestingSandbox(copyLanguageServerFiles)

  withType<ProcessResources> {
    dependsOn("copyWebviewAssets")
  }

  register("lspDownloadGenericPackageJson") {
    val outputDir = layout.buildDirectory.dir("gitlab-lsp")
    val outputFile = outputDir.get().file("generic_packages.json")

    outputs.dir(outputDir)

    doLast {
      val url =
        "https://gitlab.com/api/v4/projects/gitlab-org%2Feditor-extensions%2Fgitlab-lsp/packages?package_type=generic&sort=desc"

      URL(url).openStream().use { input ->
        outputFile.asFile.outputStream().use { output ->
          input.copyTo(output)
        }
      }
    }
  }

  register("lspDownloadGenericPackageFilesJson") {
    dependsOn("lspDownloadGenericPackageJson")

    val buildDir = layout.buildDirectory.get().asFile
    val gitlabLspDir = buildDir.resolve("gitlab-lsp")

    inputs.file(gitlabLspDir.resolve("generic_packages.json"))
    outputs.dir(gitlabLspDir)

    @Suppress("UNCHECKED_CAST")
    doLast {
      val slurper = JsonSlurper()

      // Parse package.json to get gitlab-lsp version
      val gitlabLspVersion = slurper.parse(file("package.json"))
        .let { it as? Map<String, Any> ?: error("Unexpected format for package.json.") }
        .let { it["dependencies"] as? Map<String, String> ?: error("Invalid dependencies in package.json.") }
        .let {
          it["@gitlab-org/gitlab-lsp"]
            ?: error("Unable to find @gitlab-org/gitlab-lsp under dependencies in package.json.")
        }

      // Parse generic_packages.json to find package URL
      val lspPackages = slurper.parse(gitlabLspDir.resolve("generic_packages.json")) as? List<Map<String, Any>>
        ?: error("Unable to parse generic_packages.json")

      val packageUrl = lspPackages.find { it["version"] == gitlabLspVersion }
        ?.let { it["id"] as? Number }
        ?.let { "https://gitlab.com/api/v4/projects/gitlab-org%2Feditor-extensions%2Fgitlab-lsp/packages/$it/package_files" }
        ?: error("Unable to find generic package for @gitlab-org/gitlab-lsp v$gitlabLspVersion.")

      // Download package_files.json
      URL(packageUrl).openStream().use { input ->
        gitlabLspDir.resolve("package_files.json").outputStream().use { output ->
          input.copyTo(output)
        }
      }
    }
  }

  register("lspDownloadBinaries") {
    dependsOn("lspDownloadGenericPackageFilesJson")

    val buildDir = layout.buildDirectory.get().asFile
    val gitlabLspDir = buildDir.resolve("gitlab-lsp")
    val binDir = gitlabLspDir.resolve("bin")

    inputs.file(gitlabLspDir.resolve("package_files.json"))
    outputs.dir(binDir)

    @Suppress("UNCHECKED_CAST")
    doLast {
      val packageFiles = JsonSlurper().parse(gitlabLspDir.resolve("package_files.json")) as? List<Map<String, Any>>
        ?: error("Unexpected format for package_files.json")

      binDir.mkdirs()

      packageFiles.forEach { file ->
        val id = file["id"] as? Number ?: error("Invalid value for id for package file")
        val fileName = file["file_name"] as? String ?: error("Invalid value for file_name for package file with id $id")
        val output = binDir.resolve(fileName)
        val url = "https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp/-/package_files/$id/download"

        println("Downloading $fileName...")

        ant.withGroovyBuilder {
          "get"(
            "src" to url,
            "dest" to output
          )
        }

        output.setExecutable(true, false)
      }
    }
  }
}
