action.com.gitlab.plugin.actions.ToggleDuoAction.text=GitLab Duo: Enable Code Suggestions
action.com.gitlab.plugin.actions.ToggleDuoAction.description=Use GitLab Duo to offer code suggestions

# Status bar and status widget popup text
action.ShowDuoSettings.text=Show Settings
action.ShowDocumentation.text=Show Documentation
action.EnableAllCodeSuggestions.text=Enable All Code Suggestions
action.DisableAllCodeSuggestions.text=Disable All Code Suggestions
action.DuoChatStatus.text=Duo Chat: Enabled
action.CodeSuggestionsStatus.text=Code Suggestions: Enabled
group.com.gitlab.plugin.actions.status.StatusActions.text=GitLab Duo

notification-group.general.name=GitLab Duo general notification
notification-group.important.name=GitLab Duo important notification
notification.title.gitlab-duo=GitLab Duo
status-icon.tooltip.enabled=GitLab Duo Code Suggestions enabled
status-icon.tooltip.disabled=GitLab Duo Code Suggestions disabled
status-icon.tooltip.error=GitLab Duo Code Suggestions error
status-icon.tooltip.loading=GitLab Duo Code Suggestions loading
status-icon.display-name=GitLab Duo Indicator
notification-action.settings=GitLab Duo Settings
notification-action.link.upgrade-gitlab=How to upgrade GitLab
notification.duo-code-suggestions-disabled=Code Suggestions has been disabled for this project

# Settings UI
settings.ui.gitlab.connection-section-title=Connection
settings.ui.gitlab.authentication-section-title=Authentication
settings.ui.gitlab.enable-label=Use GitLab Duo to offer code suggestions as you type
settings.ui.gitlab.url=URL to GitLab instance
settings.ui.gitlab.oauth-tab-heading=OAuth
settings.ui.gitlab.oauth-login-link-text=Login with GitLab account
settings.ui.gitlab.oauth-revoke-link-text=Revoke access to GitLab account
settings.ui.gitlab.oauth-authenticated-comment=Authenticated
settings.ui.gitlab.oauth-enabled-comment=Unavailable, because you're currently authenticated with OAuth through your GitLab account.
settings.ui.gitlab.oauth-re-authenticate=Can't refresh your token. To re-authenticate with OAuth, go to GitLab Duo settings.
settings.ui.gitlab.pat-tab-heading=PAT
settings.ui.gitlab.token=GitLab Personal Access Token
settings.ui.gitlab.create-token=Generate token on GitLab...
settings.ui.gitlab.docs-link-label=For setup instructions for Code Suggestions, see the GitLab documentation.
settings.ui.gitlab.enable-telemetry=Enable telemetry
settings.ui.gitlab.telemetry-comment=Send anonymized usage data to support plugin improvements.
settings.ui.gitlab.additional-languages-comment=Use <b>,</b> to separate \
  <a href="https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocumentItem">Language IDs</a>
settings.ui.gitlab.1password-tab-heading=1Password CLI
settings.ui.gitlab.1password-cli-gitlab-docs-label=Instructions
settings.ui.gitlab.1password-cli-integrate-label=Integrate with 1Password CLI
settings.ui.gitlab.1password-cli-secret-reference-label=Secret reference:
settings.ui.gitlab.1password-secret-reference-invalid=Please enter a valid 1Password secret reference (Example: "op://Private/GitLab Personal Access Token/token").
settings.ui.gitlab.1password-accounts-label=1Password account:
settings.ui.gitlab.1password-no-accounts=<icon src='AllIcons.General.Warning'>\\&nbsp; No 1Password account found. Make sure you have signed in to the 1Password desktop app and installed the 1Password CLI integration.
settings.ui.gitlab.1password-retry-get-accounts-link-text=Refresh
settings.ui.gitlab.personal-access-token-required=Enter either a Personal Access Token (like "glpat-xxxxxx"),\nor enable the 1Password CLI integration.
settings.ui.gitlab.auth-method-required=Enable OAuth by authorizing your GitLab account, \nenter a personal access token (like "glpat-xxxxxx"),\nor enable the 1Password CLI integration.
settings.ui.gitlab.ignore-certificate-errors-label=Ignore certificate errors
settings.ui.gitlab.ignore-certificate-errors-description=<icon src='AllIcons.General.Warning'>\\&nbsp; Use only if your machine connects to your GitLab instance through a proxy. \
  <br><a href="https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin#ignore-certificate-errors">Understand the risk.</a>
settings.ui.gitlab.ignore-certificate-lengthy-description=<br>These errors alert you of <b>potential security breaches</b>. \
  <br>Accept only if you are certain the problem is caused by the proxy: \
  <ul> \
    <li>You have verified the certificate chain is valid using your system browser.</li> \
    <li>Your machine's administrator confirms this certificate is safe to accept.</li> \
  </ul>
settings.ui.group.name=GitLab Duo
settings.ui.error.url.invalid=Enter a valid GitLab URL (Example: https://gitlab.com).
settings.ui.gitlab.enable-duo-chat=Enable GitLab Duo Chat
settings.ui.gitlab.enable-duo-code-suggestions=Enable Code Suggestions
settings.ui.gitlab.languages.title=Code Suggestions Enabled Languages
settings.ui.gitlab.verify-setup=Verify setup
settings.ui.gitlab.verify-setup.unavailable=Cannot verify at the moment.
settings.ui.gitlab.verify-setup.success=Success! The server host and Personal Access Token are correctly configured.
settings.ui.gitlab.verify-setup.unexpected-error=An unexpected error occurred. Please try again.
settings.ui.gitlab.verify-setup.unexpected-error.details=If the error persists, see the error notification for more details, or report this error.
settings.ui.gitlab.verify-setup.empty-token=Personal access token is empty.
settings.ui.gitlab.verify-setup.empty-token.details=<a href="https://gitlab.com/-/profile/personal_access_tokens?name=GitLab%20Duo%20For%20JetBrains&scopes=api">How to create a personal access token</a>
settings.ui.gitlab.verify-setup.server-version.unable-to-validate=Unable to determine the GitLab version of {0}.
settings.ui.gitlab.verify-setup.server-version.unable-to-validate.details=Check that your GitLab instance URL is correct, then try again.
settings.ui.gitlab.verify-setup.server-version.upgrade-required=GitLab {0} is not supported. Upgrade to GitLab {1} or later.
settings.ui.gitlab.verify-setup.server-version.upgrade-required.details=Please <a href="https://docs.gitlab.com/ee/update/">upgrade</a> your GitLab instance to the latest version.
settings.ui.gitlab.verify-setup.server-version.supported=GitLab {0} is supported.
settings.ui.gitlab.verify-setup.current-user.authorized=Authorized to fetch the current user.
settings.ui.gitlab.verify-setup.code-suggestions.enabled=Code Suggestions is enabled.
settings.ui.gitlab.verify-setup.code-suggestions.disabled=Code Suggestions is disabled.
settings.ui.gitlab.verify-setup.code-suggestions.disabled.details=<a href="https://docs.gitlab.com/ee/subscriptions/subscription-add-ons.html#assign-gitlab-duo-seats">Assign GitLab Duo Pro seats</a>
settings.ui.gitlab.verify-setup.code-suggestions.manually-disabled=Code Suggestions manually disabled in settings.
settings.ui.gitlab.verify-setup.code-suggestions.duo-disabled=Code Suggestions are disabled because GitLab Duo features are disabled for this project.
settings.ui.gitlab.verify-setup.code-suggestions.duo-disabled.details=<a href="https://docs.gitlab.com/ee/user/gitlab_duo/turn_on_off.html#turn-off-for-a-project">Toggle GitLab Duo for a project</a>
settings.ui.gitlab.verify-setup.code-suggestions.open-tabs=Send open tabs as context
settings.ui.gitlab.verify-setup.duo-chat.enabled=GitLab Duo Chat is enabled.
settings.ui.gitlab.verify-setup.duo-chat.disabled=GitLab Duo Chat is disabled.
settings.ui.gitlab.verify-setup.duo-chat.disabled.details=<a href="https://docs.gitlab.com/ee/subscriptions/subscription-add-ons.html#assign-gitlab-duo-seats">Assign GitLab Duo Pro seats</a>
settings.ui.gitlab.verify-setup.duo-chat.manually-disabled=GitLab Duo Chat manually disabled in settings.
settings.ui.gitlab.verify-setup.duo-chat.duo-disabled=GitLab Duo Chat is disabled because GitLab Duo features are disabled for this project.
settings.ui.gitlab.verify-setup.duo-chat.duo-disabled.details=<a href="https://docs.gitlab.com/ee/user/gitlab_duo/turn_on_off.html#turn-off-for-a-project">Toggle GitLab Duo for a project</a>
settings.ui.personal-access-token.computed=<computed>
settings.ui.personal-access-token.loading=Loading...
settings.ui.gitlab.advanced.stream-code-generations=Stream code generations
settings.ui.gitlab.advanced.pass-ca-cert=Pass CA certificate from Duo to the Language Server
settings.ui.gitlab.advanced.ca=Certificate authority (CA)
settings.ui.gitlab.advanced.cert=Certificate
settings.ui.gitlab.advanced.cert-key=Certificate key

# Startup Notifications
notification.startup.get-started.title=Get started with GitLab Duo Code Suggestions
notification.startup.get-started.message=GitLab Duo Code Suggestions [{0}] is ready to use with this project.

# Exception Notifications
notification.exception.unauthorized.message=The authentication token is invalid.
notification.exception.offline.message=Cannot reach GitLab server. It appears to be offline.
notification.exception.insufficient-scope.message=Personal Access Token does not have the required scope level.
notification.exception.proxy-failure.message=Proxy is misconfigured.
notification.exception.response.message=Request failed with error: {0}
notification.exception.chat-retry-timeout.message=Unable to retrieve chat response.
notification.exception.gitlab-instance-misconfiguration.message=Gitlab instance configuration error: {0}
notification.exception.server-closed-connection=GitLab unexpectedly closed the connection. Please try again.
notification.exception.general-error.message=An unexpected error occurred.

# Error reporting
error-reporter.action-text=Open GitLab Issue

# Shared
duo-chat.not-licensed=GitLab Duo Chat is a paid feature, part of GitLab Duo Pro. For access, contact your GitLab administrator.
code-suggestions.not-licensed=Code Suggestions is now a paid feature, part of GitLab Duo Pro. To upgrade, contact your GitLab administrator.
code-suggestions.gitlab-version-unsupported=GitLab Duo Code Suggestions requires GitLab version 16.8 or later.

# Personal Access Token
pat.accepted.not-active=Personal Access Token is not active. Try again with an active token.
pat.accepted.missing-scope=Personal Access Token is missing {0} scope. Try again with a token containing the correct scope.
pat.accepted.valid=Personal Access Token is valid.

pat.refused.message=Invalid personal access token for {0} ({1}). Confirm that your token is valid for this URL.
pat.unknown.message=Failed to validate personal access token on {0}.

# OAuth
oauth.token.invalid=OAuth token is invalid.
oauth.token.valid=OAuth token is valid.
oauth.login.needed=Your session has expired. Sign in again.

# GitLab Language Server
settings.ui.group.language-server.name=GitLab Language Server
settings.ui.group.language-server.description=Provides advanced code suggestion features across various IDEs.
settings.ui.language-server.restart=Restart Language Server

# Onboarding
toolwindow.stripe.onboarding=Help me use GitLab Duo

# Design Preview
toolwindow.stripe.design-preview=Design System Preview

# Features
feature.authentication = Authentication
feature.code_suggestions=Code Suggestions
feature.chat=Duo Chat
action.terminal.explain-code=Explain with Duo
action.terminal.explain-code.description=Explain selected code with Duo Chat
