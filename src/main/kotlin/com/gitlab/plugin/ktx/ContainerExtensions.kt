package com.gitlab.plugin.ktx

import java.awt.Container
import javax.swing.JLabel

// This function creates individual JLabels for each word in the text. Makes individual words wrap in WrapLayouts
fun Container.splitTextIntoJLabels(
  text: String,
  delimiter: String = " "
) {
  text.split(" ").forEach { add(JLabel("$it$delimiter")) }
}
