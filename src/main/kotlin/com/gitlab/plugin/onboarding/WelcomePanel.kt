package com.gitlab.plugin.onboarding

import com.gitlab.plugin.ui.components.WrappableLabel
import com.intellij.util.ui.JBFont
import java.awt.Dimension
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.JButton
import javax.swing.JPanel

@Suppress("MagicNumber")
class WelcomePanel(
  onButtonClicked: () -> Unit,
) : JPanel() {

  private val title = WrappableLabel("Welcome to GitLab Duo") {
    alignmentX = RIGHT_ALIGNMENT
    font = JBFont.h0().asBold()
  }

  private val description = WrappableLabel("Boost your productivity with GitLab Duo.") { alignmentX = RIGHT_ALIGNMENT }

  private val features = WrappableLabel(
    "Write code faster with the code completion and generation features in your IDE. GitLab Duo Code " +
      "Suggestions and Duo Chat integrate AI features into the editor you already use."
  ) { alignmentX = RIGHT_ALIGNMENT }

  private val button = JButton("Get started").apply {
    alignmentX = RIGHT_ALIGNMENT
    addActionListener { onButtonClicked() }
  }

  init {
    layout = BoxLayout(this, BoxLayout.PAGE_AXIS)

    add(title)
    add(Box.createRigidArea(Dimension(0, 8)))
    add(description)
    add(Box.createRigidArea(Dimension(0, 8)))
    add(features)
    add(Box.createRigidArea(Dimension(0, 16)))
    add(button)
  }
}
