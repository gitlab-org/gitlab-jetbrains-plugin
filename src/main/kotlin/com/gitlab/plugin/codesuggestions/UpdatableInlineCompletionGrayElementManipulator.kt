package com.gitlab.plugin.codesuggestions

import com.gitlab.plugin.codesuggestions.render.UpdatableInlineCompletionGrayElement
import com.intellij.codeInsight.inline.completion.elements.InlineCompletionElement
import com.intellij.codeInsight.inline.completion.elements.InlineCompletionElementManipulator

class UpdatableInlineCompletionGrayElementManipulator : InlineCompletionElementManipulator {
  override fun isApplicable(element: InlineCompletionElement): Boolean {
    return element is UpdatableInlineCompletionGrayElement
  }

  override fun truncateFirstSymbol(element: InlineCompletionElement): InlineCompletionElement? {
    element as UpdatableInlineCompletionGrayElement

    return when {
      element.text.length > 1 -> UpdatableInlineCompletionGrayElement(element.text.drop(1), element.editor)
      else -> null
    }
  }

  /*
   * This method is not present between 2023.3 until 2024.1.4.
   * It is added in 2024.2 and is necessary for word by word or line by line acceptance
   */
  @Suppress("Unused")
  fun substring(element: InlineCompletionElement, startOffset: Int, endOffset: Int): InlineCompletionElement? {
    element as UpdatableInlineCompletionGrayElement

    if (startOffset >= endOffset) {
      return null
    }

    return UpdatableInlineCompletionGrayElement(
      element.text.substring(startOffset, endOffset),
      element.editor,
      isPartiallyAccepted = true
    )
  }
}
