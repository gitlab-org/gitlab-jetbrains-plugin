package com.gitlab.plugin.codesuggestions

import com.gitlab.plugin.codesuggestions.listeners.SwitchInlineCompletionEventListener
import com.gitlab.plugin.codesuggestions.services.SUGGESTION_TRACKER_KEY
import com.gitlab.plugin.codesuggestions.services.SuggestionsTracker
import com.gitlab.plugin.codesuggestions.services.SwitchInlineCompletionSuggestionService
import com.gitlab.plugin.ui.GitLabIcons
import com.intellij.codeInsight.inline.completion.InlineCompletionProviderPresentation
import com.intellij.icons.AllIcons
import com.intellij.openapi.Disposable
import com.intellij.openapi.application.runInEdt
import com.intellij.openapi.components.service
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.observable.util.addComponent
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Disposer
import java.awt.event.HierarchyEvent
import java.awt.event.MouseEvent
import javax.swing.JComponent
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.JSeparator
import javax.swing.SwingConstants

@Suppress("UnstableApiUsage")
class InlineCompletionTooltipPresentation : InlineCompletionProviderPresentation, SwitchInlineCompletionEventListener {
  companion object {
    const val LOADING_LABEL_TEXT = "1/?"
  }

  private lateinit var previous: JLabel
  private lateinit var information: JLabel
  private lateinit var next: JLabel

  private val disposable: Disposable = Disposer.newDisposable()

  override fun getTooltip(project: Project?): JComponent {
    previous = JLabel(AllIcons.Actions.ArrowCollapse)
    information = JLabel(LOADING_LABEL_TEXT)
    next = JLabel(AllIcons.Actions.ArrowExpand)

    val panel = JPanel().apply {
      addComponent(JLabel("GitLab Duo Code Suggestions", GitLabIcons.NotificationIcon, JLabel.LEFT))
    }

    val suggestionsTracker = project?.currentEditor?.getUserData(SUGGESTION_TRACKER_KEY)
      ?.takeIf { !it.isStreaming }
      ?: return panel

    panel.addHierarchyListener { event ->
      val hierarchyChanged = event.id == HierarchyEvent.HIERARCHY_CHANGED
      val showingChanged = event.changeFlags.toInt().and(HierarchyEvent.SHOWING_CHANGED) != 0

      if (hierarchyChanged && showingChanged) {
        if (panel.isShowing) {
          if (suggestionsTracker.response == null) {
            project.service<SwitchInlineCompletionSuggestionService>().loadCompletions(suggestionsTracker)
          }

          information.text = suggestionsTracker.getIndexInformation()
        } else {
          disposable.dispose()
        }
      }
    }

    panel.apply {
      addComponent(JSeparator(SwingConstants.VERTICAL))
      addComponent(previous)
      addComponent(information)
      addComponent(next)
    }

    previous.addMouseListener(MouseAdapter(project, SwitchInlineCompletionSuggestionMode.PREVIOUS))
    next.addMouseListener(MouseAdapter(project, SwitchInlineCompletionSuggestionMode.NEXT))

    project
      .messageBus
      .connect(disposable)
      .subscribe(SwitchInlineCompletionEventListener.SWITCH_INLINE_COMPLETION_TOPIC, this)

    return panel
  }

  private class MouseAdapter(
    val project: Project,
    val mode: SwitchInlineCompletionSuggestionMode
  ) : java.awt.event.MouseAdapter() {
    override fun mouseClicked(e: MouseEvent) {
      val editor = project.currentEditor
        ?: return

      project.service<SwitchInlineCompletionSuggestionService>().switch(editor, mode)
    }
  }

  override fun onRequestMultipleSuggestions() = runInEdt {
    information.text = LOADING_LABEL_TEXT
  }

  override fun onReceivedMultipleSuggestions(suggestionsTracker: SuggestionsTracker) = runInEdt {
    information.text = suggestionsTracker.getIndexInformation()
  }

  override fun onSwitchInlineCompletion(suggestionsTracker: SuggestionsTracker) = runInEdt {
    information.text = suggestionsTracker.getIndexInformation()
  }
}

private val Project?.currentEditor
  get() = this?.let { project -> FileEditorManager.getInstance(project).selectedTextEditor }
