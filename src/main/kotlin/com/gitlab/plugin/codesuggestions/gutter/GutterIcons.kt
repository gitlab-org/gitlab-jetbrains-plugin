package com.gitlab.plugin.codesuggestions.gutter

import com.gitlab.plugin.ui.BaseGutterIconRenderer
import com.intellij.openapi.util.IconLoader
import javax.swing.Icon

object GutterIcons {
  object Loading : BaseGutterIconRenderer() {
    override fun getIcon(): Icon {
      return IconLoader.getIcon("/icons/gitlab-code-suggestions-loading.svg", javaClass)
    }
  }

  object Ready : BaseGutterIconRenderer() {
    override fun getIcon(): Icon {
      return IconLoader.getIcon("/icons/gitlab-code-suggestions-enabled.svg", javaClass)
    }
  }
}
