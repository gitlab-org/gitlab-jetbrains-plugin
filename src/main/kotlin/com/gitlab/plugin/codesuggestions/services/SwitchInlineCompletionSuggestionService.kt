package com.gitlab.plugin.codesuggestions.services

import com.gitlab.plugin.codesuggestions.SwitchInlineCompletionSuggestionMode
import com.gitlab.plugin.codesuggestions.listeners.SwitchInlineCompletionEventListener
import com.gitlab.plugin.lsp.codesuggestions.LanguageServerCompletionStrategy
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Key
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

val SUGGESTION_TRACKER_KEY = Key.create<SuggestionsTracker>("EDITOR_SUGGESTION_TRACKER")

@Service(Service.Level.PROJECT)
class SwitchInlineCompletionSuggestionService(
  private val project: Project,
  private val coroutineScope: CoroutineScope
) {
  private val logger = logger<SwitchInlineCompletionSuggestionService>()

  private val publisher = project
    .messageBus
    .syncPublisher(SwitchInlineCompletionEventListener.SWITCH_INLINE_COMPLETION_TOPIC)

  fun switch(editor: Editor, mode: SwitchInlineCompletionSuggestionMode) = coroutineScope.launch {
    val tracker = editor.getUserData(SUGGESTION_TRACKER_KEY)
      ?: return@launch logger.debug("No suggestion tracker found for current editor.")

    if (tracker.isStreaming) {
      return@launch logger.debug("Cannot switch suggestion tracker for a streaming suggestion.")
    }

    if (tracker.response == null) {
      executeLoadCompletions(editor, tracker)
    }

    logger.debug("Switching inline completion suggestion. mode=$mode")
    tracker.switch(mode)
    publisher.onSwitchInlineCompletion(tracker)

    editor.putUserData(SUGGESTION_TRACKER_KEY, tracker)
  }

  fun loadCompletions(tracker: SuggestionsTracker) = coroutineScope.launch {
    executeLoadCompletions(tracker.editor, tracker)
  }

  private suspend fun executeLoadCompletions(editor: Editor, tracker: SuggestionsTracker) {
    logger.debug("Loading multiple completions for current editor.")
    publisher.onRequestMultipleSuggestions()

    tracker.context.isInvoked = true
    tracker.response = project.service<LanguageServerCompletionStrategy>().generateCompletions(tracker.context)

    publisher.onReceivedMultipleSuggestions(tracker)
    editor.putUserData(SUGGESTION_TRACKER_KEY, tracker)
  }
}
