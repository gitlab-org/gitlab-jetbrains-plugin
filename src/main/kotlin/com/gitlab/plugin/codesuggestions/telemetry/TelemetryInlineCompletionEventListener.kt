package com.gitlab.plugin.codesuggestions.telemetry

import com.gitlab.plugin.codesuggestions.render.UpdatableInlineCompletionGrayElement
import com.gitlab.plugin.exceptions.ExceptionTracker
import com.intellij.codeInsight.inline.completion.InlineCompletionEventAdapter
import com.intellij.codeInsight.inline.completion.InlineCompletionEventType
import com.intellij.codeInsight.inline.completion.logs.InlineCompletionUsageTracker
import com.intellij.openapi.components.service
import com.intellij.util.application
import org.jetbrains.annotations.ApiStatus

@ApiStatus.Experimental
class TelemetryInlineCompletionEventListener(
  private val telemetryService: TelemetryService
) : InlineCompletionEventAdapter {
  private val exceptionTracker by lazy { application.service<ExceptionTracker>() }

  override fun onShow(event: InlineCompletionEventType.Show) = handleExceptions {
    val element = (event.element as? UpdatableInlineCompletionGrayElement)

    if (element == null || element.isPartiallyAccepted) {
      return@handleExceptions
    }

    telemetryService.shown()
  }

  override fun onHide(event: InlineCompletionEventType.Hide) = handleExceptions {
    if (!event.isCurrentlyDisplaying || event.finishType == InlineCompletionUsageTracker.ShownEvents.FinishType.SELECTED) {
      return@handleExceptions
    }

    telemetryService.rejected()
  }

  override fun onInsert(event: InlineCompletionEventType.Insert) = handleExceptions {
    telemetryService.accepted()
  }

  @Suppress("TooGenericExceptionCaught")
  private fun handleExceptions(block: () -> Unit) = try {
    block()
  } catch (e: Exception) {
    exceptionTracker.handle(e)
  }
}
