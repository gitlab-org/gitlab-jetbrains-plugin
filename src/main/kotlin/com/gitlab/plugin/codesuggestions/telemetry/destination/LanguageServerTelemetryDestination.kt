package com.gitlab.plugin.codesuggestions.telemetry.destination

import com.gitlab.plugin.codesuggestions.telemetry.Event
import com.gitlab.plugin.lsp.messages.CodeSuggestionsTelemetryContext
import com.gitlab.plugin.lsp.messages.CodeSuggestionsTelemetryParams
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project

class LanguageServerTelemetryDestination(private val project: Project) : TelemetryDestination {
  private val logger = logger<LanguageServerTelemetryDestination>()

  /**
   * Documents the event method, which sends telemetry data to the GitLab Language Server.
   *
   * @param event The event to be sent as telemetry data.
   */
  override fun receive(event: Event) {
    val params = CodeSuggestionsTelemetryParams(
      action = event.action,
      context = CodeSuggestionsTelemetryContext(
        trackingId = event.context.trackingId,
        optionId = event.context.optionId
      ),
    )

    logger.info("Sending telemetry data to the GitLab Language Server: $params")
    project.service<GitLabLanguageServerService>().lspServer?.telemetry(params)
  }
}
