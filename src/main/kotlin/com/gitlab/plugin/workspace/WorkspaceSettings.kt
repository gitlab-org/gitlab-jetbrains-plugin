package com.gitlab.plugin.workspace

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.authentication.TokenProviderType

data class WorkspaceSettings(
  val url: String,
  val codeSuggestionsEnabled: Boolean,
  val duoChatEnabled: Boolean,
  val token: String,
  val telemetry: Telemetry,
  val ignoreCertificateErrors: Boolean,
  val tokenProviderType: TokenProviderType
)

data class Telemetry(
  val enabled: Boolean,
  val trackingUrl: String = BuildConfig.SNOWPLOW_COLLECTOR_URL,
)
