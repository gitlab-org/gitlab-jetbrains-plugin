package com.gitlab.plugin.actions.chat

import com.gitlab.plugin.chat.model.ChatRecord

/**
 * Action will explain currently selected code with GitLab Duo Chat
 */
class ExplainCodeChatAction : SelectedContextChatActionBase(
  content = EXPLAIN_CODE_CHAT_CONTENT,
  recordType = ChatRecord.Type.EXPLAIN_CODE
) {
  companion object {
    const val EXPLAIN_CODE_CHAT_CONTENT = "/explain"
  }
}
