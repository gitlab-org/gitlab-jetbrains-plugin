package com.gitlab.plugin.actions.chat

import com.gitlab.plugin.chat.model.ChatRecord

/**
 * Action will try to fix currently selected code with GitLab Duo Chat
 */
class FixCodeChatAction : SelectedContextChatActionBase(
  content = FIX_CODE_CHAT_CONTENT,
  recordType = ChatRecord.Type.FIX_CODE
) {
  companion object {
    const val FIX_CODE_CHAT_CONTENT = "/fix"
  }
}
