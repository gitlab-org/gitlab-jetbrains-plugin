package com.gitlab.plugin.actions.chat

import com.gitlab.plugin.chat.quickchat.QuickChatOpenMethod
import com.gitlab.plugin.chat.quickchat.services.QuickChatService
import com.gitlab.plugin.services.DuoChatStateService
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.components.service
import com.intellij.ui.codeFloatingToolbar.CodeFloatingToolbar

class OpenQuickChatAction : AnAction() {
  override fun actionPerformed(e: AnActionEvent) {
    val project = e.project ?: return
    val editor = e.getData(CommonDataKeys.EDITOR) ?: return

    val openMethod = when (e.place) {
      "keyboard shortcut" -> QuickChatOpenMethod.SHORTCUT
      else -> QuickChatOpenMethod.CLICK_BUTTON
    }

    project.service<QuickChatService>().openChat(editor, openMethod)

    val codeToolbar = CodeFloatingToolbar.getToolbar(editor) ?: return
    codeToolbar.scheduleHide()
  }

  override fun update(e: AnActionEvent) {
    val project = e.project
      ?: return

    e.presentation.isEnabledAndVisible = project.service<DuoChatStateService>().duoChatEnabled
  }

  override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT
}
