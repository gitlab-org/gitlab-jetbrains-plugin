package com.gitlab.plugin.actions.status

import com.gitlab.plugin.GitLabBundle
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.options.ShowSettingsUtil

class ShowDuoSettings : AnAction() {
  override fun actionPerformed(e: AnActionEvent) {
    val project = e.project ?: return
    ShowSettingsUtil.getInstance().showSettingsDialog(project, GitLabBundle.message("settings.ui.group.name"))
  }
}
