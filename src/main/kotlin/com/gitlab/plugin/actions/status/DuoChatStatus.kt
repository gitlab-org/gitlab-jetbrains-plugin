package com.gitlab.plugin.actions.status

import com.gitlab.plugin.services.DuoChatStateService
import com.gitlab.plugin.ui.GitLabIcons
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.application.runReadAction
import com.intellij.openapi.components.service

class DuoChatStatus : AnAction() {
  /**
   * Do nothing when clicked.
   */
  override fun actionPerformed(e: AnActionEvent) = Unit

  override fun getActionUpdateThread() = ActionUpdateThread.BGT

  /**
   * Disable the action for now.
   */
  override fun update(e: AnActionEvent) {
    val duoChatStateService = e.project?.service<DuoChatStateService>()
      ?: return

    runReadAction {
      e.presentation.isEnabled = false

      val engagedCheck = duoChatStateService.getEngagedCheck()
      if (engagedCheck == null) {
        e.presentation.icon = GitLabIcons.Actions.DuoChatEnabled
        e.presentation.text = "Duo Chat: Enabled"
      } else {
        e.presentation.icon = GitLabIcons.Actions.DuoChatDisabled
        e.presentation.text = "Duo Chat: Disabled (${engagedCheck.checkId})"
      }
    }
  }
}
