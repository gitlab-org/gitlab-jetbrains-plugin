package com.gitlab.plugin.actions.status

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.lsp.params.CodeCompletion
import com.gitlab.plugin.lsp.params.Settings
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.components.service
import com.intellij.openapi.util.Computable
import com.intellij.util.application
import org.eclipse.lsp4j.DidChangeConfigurationParams

class EnableAllCodeSuggestions : AnAction() {
  override fun actionPerformed(e: AnActionEvent) {
    val enabled = application.runReadAction(
      Computable {
        DuoPersistentSettings.getInstance().codeSuggestionsEnabled
      }
    )

    if (!enabled) {
      application.runWriteAction {
        if (DuoPersistentSettings.getInstance().toggleCodeSuggestions()) {
          e.project?.service<GitLabLanguageServerService>()?.lspServer?.workspaceService?.didChangeConfiguration(
            DidChangeConfigurationParams(
              Settings(codeCompletion = CodeCompletion(enabled = true))
            )
          )
        } else {
          // skip notification since another thread modified the value.
        }
      }
    }
  }

  override fun getActionUpdateThread() = ActionUpdateThread.BGT

  override fun update(event: AnActionEvent) {
    application.runReadAction {
      event.presentation.isVisible = !DuoPersistentSettings.getInstance().codeSuggestionsEnabled
    }
  }
}
