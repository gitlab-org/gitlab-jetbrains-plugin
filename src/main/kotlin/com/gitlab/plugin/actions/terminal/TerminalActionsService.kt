package com.gitlab.plugin.actions.terminal

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.actions.chat.DUO_CHAT_TOOL_WINDOW_ID
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindowManager
import com.intellij.terminal.JBTerminalWidget
import com.intellij.util.ui.UIUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Service(Service.Level.PROJECT)
class TerminalActionsService(private val project: Project, private val coroutineScope: CoroutineScope) {
  fun isTextSelectedInTerminal(): Boolean {
    return getTerminalSelectedText().isNotBlank()
  }

  fun triggerDuoExplain(): Boolean {
    val terminalSelectedText = getTerminalSelectedText()

    if (terminalSelectedText.isBlank()) {
      return false
    }

    val duoChatToolWindow = ToolWindowManager.getInstance(project).getToolWindow(DUO_CHAT_TOOL_WINDOW_ID)
    if (duoChatToolWindow?.isVisible == true) {
      duoChatToolWindow.activate(null)
    }
    val chatService = project.service<ChatService>()

    coroutineScope.launch {
      chatService.processNewUserPrompt(
        NewUserPromptRequest(
          "/explain",
          ChatRecord.Type.EXPLAIN_CODE,
          context = ChatRecordContext(
            ChatRecordFileContext(
              fileName = "", // there is not a file in the terminal
              selectedText = terminalSelectedText
            )
          )
        )
      )
    }
    return true
  }

  // Wrapper to allow testing of feature flags from BuildConfig, which cannot be mocked
  fun areTerminalActionsEnabled(): Boolean = BuildConfig.DUO_TERMINAL_ACTIONS_ENABLED

  private fun getTerminalSelectedText(): String {
    val terminalToolWindow = ToolWindowManager.getInstance(project).getToolWindow("Terminal")
    val terminalWidget = UIUtil.findComponentOfType(terminalToolWindow?.component, JBTerminalWidget::class.java)
    return terminalWidget?.selectedText.orEmpty()
  }
}
