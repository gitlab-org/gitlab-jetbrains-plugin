package com.gitlab.plugin

import com.gitlab.plugin.services.Status

class GitLabSettingsChangeEvent(val status: Status = Status.DISABLED)
