package com.gitlab.plugin.ui.designsystem.components

import com.intellij.ui.EditorNotificationPanel
import com.intellij.ui.InlineBanner
import javax.swing.Icon

/**
 * A customizable InlineBanner for use in IntelliJ IDEA plugins.
 *
 * This class extends the JetBrains InlineBanner and provides a convenient way to create and configure a banner
 * with various options such as status, icon, actions, and more.
 *
 * Usage:
 * ```
 * val banner = InlineBanner(
 *     text = "Custom banner message",
 *     status = EditorNotificationPanel.Status.Warning,
 *     icon = MyIcons.WARNING_ICON,
 *     showCloseButton = true,
 *     closeAction = { /* Custom close logic */ },
 *     gearAction = "Settings" to { /* Open settings */ },
 *     actions = listOf(
 *         "Action 1" to { /* Action 1 logic */ },
 *         "Action 2" to { /* Action 2 logic */ }
 *     )
 * )
 *
 * // Then add the banner to your UI:
 * myPanel.add(banner)
 * ```
 *
 * @param text The main message to display in the banner.
 * @param status The status of the banner (Info, Warning, etc.). Affects the banner's appearance.
 * @param icon An optional icon to display in the banner.
 * @param showCloseButton Whether to show a close button on the banner.
 * @param closeAction An optional action to perform when the close button is clicked.
 * @param gearAction An optional pair of (tooltip, action) for the gear (settings) button.
 * @param actions A list of name-action pairs for additional action buttons on the banner.
 */
class InlineBanner(
  text: String,
  status: EditorNotificationPanel.Status = EditorNotificationPanel.Status.Info,
  icon: Icon? = null,
  showCloseButton: Boolean = true,
  closeAction: (() -> Unit)? = null,
  gearAction: Pair<String, () -> Unit>? = null,
  actions: List<Pair<String, () -> Unit>> = emptyList()
) : InlineBanner(text, status) {

  init {
    icon?.let { setIcon(it) }
    showCloseButton(showCloseButton)
    closeAction?.let { setCloseAction(Runnable(it)) }
    gearAction?.let { (tooltip, action) -> setGearAction(tooltip, Runnable(action)) }
    actions.forEach { (name, action) -> addAction(name, Runnable(action)) }
  }
}
