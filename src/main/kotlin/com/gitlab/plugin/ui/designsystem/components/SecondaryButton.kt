package com.gitlab.plugin.ui.designsystem.components

import javax.swing.JButton

class SecondaryButton(text: String) : JButton(text)
