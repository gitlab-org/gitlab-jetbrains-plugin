package com.gitlab.plugin.ui.designsystem.components

import com.intellij.openapi.fileEditor.FileEditor
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.ui.EditorNotificationPanel
import com.intellij.ui.EditorNotificationProvider
import java.util.function.Function
import javax.swing.Icon
import javax.swing.JComponent

/**
 * Abstract base class for creating default banners.
 *
 * To use this class, create a subclass and implement all abstract properties and methods.
 *
 * To register your banner implementation in your plugin.xml, add the following within the <extensions> tag:
 *
 * ```
 * <extensions defaultExtensionNs="com.intellij">
 *     <editorNotificationProvider implementation="com.your.package.YourBannerImplementation"/>
 * </extensions>
 * ```
 *
 * Replace "com.your.package.YourBannerImplementation" with the fully qualified name of your Banner subclass.
 */
abstract class DefaultBanner : EditorNotificationProvider {
  /** The main text to be displayed in the banner */
  protected abstract val text: String

  /** The status of the banner, which affects its appearance */
  protected abstract val status: EditorNotificationPanel.Status

  /** An optional icon to be displayed in the banner */
  protected abstract val icon: Icon?

  /** A list of action pairs (label to display, action to perform) */
  protected abstract val actions: List<Pair<String, Runnable>>

  /** Whether to show a close button on the banner */
  protected abstract val showCloseButton: Boolean

  /** An optional action to perform when the close button is clicked */
  protected abstract val closeAction: Runnable?

  /**
   * Creates and returns a function that produces a JComponent (the banner) for a given FileEditor.
   * This method is called by the IDE to determine if and what kind of notification should be shown.
   */
  override fun collectNotificationData(project: Project, file: VirtualFile): Function<in FileEditor, out JComponent?> {
    return Function { fileEditor ->
      if (isApplicable(project, file)) {
        createNotificationPanel(fileEditor)
      } else {
        null
      }
    }
  }

  /**
   * Creates an EditorNotificationPanel with the configured properties.
   * This method is called internally when the banner needs to be displayed.
   */
  private fun createNotificationPanel(fileEditor: FileEditor): EditorNotificationPanel {
    return EditorNotificationPanel(fileEditor, status).apply {
      text(this@DefaultBanner.text)
      this@DefaultBanner.icon?.let { icon(it) }
      actions.forEach { (actionText, actionHandler) ->
        createActionLabel(actionText, actionHandler)
      }
      if (showCloseButton) {
        closeAction?.let { setCloseAction(it) }
      }
    }
  }

  /**
   * Determines whether the banner should be shown for a given project and file.
   * Subclasses should implement this method to define when the banner is applicable.
   *
   * @param project The current project
   * @param file The file being edited
   * @return true if the banner should be shown, false otherwise
   */
  protected abstract fun isApplicable(project: Project, file: VirtualFile): Boolean
}
