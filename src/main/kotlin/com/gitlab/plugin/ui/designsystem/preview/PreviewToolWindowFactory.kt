package com.gitlab.plugin.ui.designsystem.preview

import com.gitlab.plugin.BuildConfig
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory
import com.intellij.ui.components.JBScrollPane
import com.intellij.ui.content.ContentFactory
import javax.swing.BorderFactory
import javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER
import javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED

internal class PreviewToolWindowFactory : ToolWindowFactory, DumbAware {

  override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
    val component = JBScrollPane(
      PreviewPanel(),
      VERTICAL_SCROLLBAR_AS_NEEDED,
      HORIZONTAL_SCROLLBAR_NEVER
    ).apply {
      border = BorderFactory.createEmptyBorder()
    }

    val content = ContentFactory.getInstance().createContent(
      component,
      "",
      false
    )

    toolWindow.contentManager.addContent(content)
  }

  override suspend fun isApplicableAsync(project: Project): Boolean {
    return BuildConfig.SHOW_DESIGN_SYSTEM_PREVIEW
  }
}
