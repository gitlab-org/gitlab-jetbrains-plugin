package com.gitlab.plugin.ui

import com.intellij.openapi.editor.markup.GutterIconRenderer

abstract class BaseGutterIconRenderer : GutterIconRenderer() {
  // GutterIconRenderer enforces override of equals and hash code
  override fun equals(other: Any?): Boolean {
    return (other != null && other is GutterIconRenderer && other.icon == icon)
  }

  override fun hashCode(): Int {
    return icon.hashCode()
  }
}
