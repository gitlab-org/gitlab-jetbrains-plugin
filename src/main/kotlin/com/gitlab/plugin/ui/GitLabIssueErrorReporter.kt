package com.gitlab.plugin.ui

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.util.launchUrl
import com.intellij.openapi.application.ApplicationInfo
import com.intellij.openapi.diagnostic.ErrorReportSubmitter
import com.intellij.openapi.diagnostic.IdeaLoggingEvent
import com.intellij.openapi.diagnostic.SubmittedReportInfo
import com.intellij.openapi.util.SystemInfo
import com.intellij.util.Consumer
import io.ktor.http.*
import java.awt.Component

// default character limit set by cloudflare - 6000 (leaving space for other headers including cookie)
const val HEADER_CHARACTER_LIMIT = 10_000

// Leave space for 25% more characters from URL encoding
const val URL_ENCODED_HEADER_CHARACTER_LIMIT = HEADER_CHARACTER_LIMIT.div(1.25).toInt()

private const val STACK_TRACE_PLACEHOLDER = "{{STACK_TRACE}}"

class GitLabIssueErrorReporter : ErrorReportSubmitter() {
  override fun getReportActionText() = GitLabBundle.message("error-reporter.action-text")

  override fun submit(
    events: Array<out IdeaLoggingEvent>,
    additionalInfo: String?,
    parentComponent: Component,
    consumer: Consumer<in SubmittedReportInfo>
  ): Boolean {
    events.forEach { event ->
      launchUrl(gitlabIssueUrl(event, additionalInfo.orEmpty()))
    }

    consumer.consume(SubmittedReportInfo(SubmittedReportInfo.SubmissionStatus.NEW_ISSUE))

    return true
  }

  private fun gitlabIssueUrl(event: IdeaLoggingEvent, additionalInfo: String): String {
    val issueDescription = buildIssueDescription(event, additionalInfo)

    return buildString {
      append("https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/new?")
      append("issuable_template=Bug")
      append("&")
      append("issue[description]=${issueDescription.encodeURLParameter()}")
    }
  }

  // Inserts the stack trace into the issue description after first truncating it based on the length of the template
  private fun buildIssueDescription(event: IdeaLoggingEvent, additionalInfo: String): String {
    val template = buildIssueDescriptionTemplate(additionalInfo)
    val stackTraceCharacterLimit = URL_ENCODED_HEADER_CHARACTER_LIMIT - template.length + STACK_TRACE_PLACEHOLDER.length
    val truncatedStackTrace = event.throwableText.take(stackTraceCharacterLimit)

    return template.replace(STACK_TRACE_PLACEHOLDER, truncatedStackTrace)
  }

  // Returns a template for the issue description with a placeholder for the stack trace, since the stack trace will need
  // to be truncated based on the length of this template before it be swapped in for the placeholder.
  private fun buildIssueDescriptionTemplate(additionalInfo: String): String {
    return """
      ## Debug information

      <details><summary>Stack trace</summary>

      ```
      $STACK_TRACE_PLACEHOLDER
      ```

      </details>

      ### Environment

      - Plugin version: ${getPluginVersion()}
      - IDE: ${getIdeInfo()}
      - JDK: ${getJdkInfo()}
      - OS: ${getOsInfo()}

      ### Additional information

      $additionalInfo
    """.trimIndent()
  }

  private fun getIdeInfo(): String {
    val appInfo = ApplicationInfo.getInstance()
    return "${appInfo.fullApplicationName}; Build: ${appInfo.build.asString()}"
  }

  private fun getJdkInfo(): String {
    return with(System.getProperties()) {
      getProperty("java.version", "unknown") +
        "; VM: " + getProperty("java.vm.name", "unknown") +
        "; Vendor: " + getProperty("java.vendor", "unknown")
    }
  }

  private fun getOsInfo(): String {
    return "${SystemInfo.getOsNameAndVersion()}; Arch: ${SystemInfo.OS_ARCH}"
  }

  private fun getPluginVersion(): String {
    return GitLabBundle.plugin().version ?: "unknown"
  }
}
