package com.gitlab.plugin.ui.components

import com.intellij.ui.JBColor
import java.awt.Cursor
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.JTextPane
import javax.swing.text.SimpleAttributeSet
import javax.swing.text.StyleConstants
import javax.swing.text.StyledDocument

class HyperlinkTextPane : JTextPane() {
  private val linkRanges = mutableListOf<Pair<Int, Int>>()
  private val linkListeners = mutableListOf<() -> Unit>()

  init {
    isEditable = false
    isOpaque = false
    addMouseListener(LinkMouseListener())
    addMouseMotionListener(LinkMouseListener())
  }

  fun addText(text: String) {
    document.insertString(document.length, text, null)
  }

  fun addHyperlink(text: String, listener: () -> Unit) {
    val doc = document as StyledDocument
    val hyperlink = SimpleAttributeSet()
    StyleConstants.setForeground(hyperlink, JBColor.BLUE)

    doc.insertString(doc.length, text, hyperlink)
    linkRanges.add(Pair(doc.length - text.length, doc.length))
    linkListeners.add(listener)
  }

  private inner class LinkMouseListener : MouseAdapter() {
    override fun mouseClicked(e: MouseEvent) {
      val offset = viewToModel2D(e.point)
      val linkIndex = linkRanges.indexOfFirst { offset in it.first until it.second }
      if (linkIndex != -1) {
        linkListeners[linkIndex].invoke()
      }
    }

    override fun mouseMoved(e: MouseEvent) {
      val offset = viewToModel2D(e.point)
      cursor = if (linkRanges.any { offset in it.first until it.second }) {
        Cursor(Cursor.HAND_CURSOR)
      } else {
        Cursor.getDefaultCursor()
      }
    }
  }
}
