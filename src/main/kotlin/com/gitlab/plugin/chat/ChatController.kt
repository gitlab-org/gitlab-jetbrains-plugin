package com.gitlab.plugin.chat

import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.codesnippets.InsertCodeSnippetService
import com.gitlab.plugin.chat.context.*
import com.gitlab.plugin.chat.exceptions.ChatException
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.chat.telemetry.trackFeedback
import com.gitlab.plugin.chat.view.abstraction.ChatView
import com.gitlab.plugin.chat.view.model.*
import com.gitlab.plugin.services.chat.ChatApiClient
import com.gitlab.plugin.util.project.workspaceFolder
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.datetime.LocalDateTime
import java.util.*

@Suppress("TooManyFunctions")
class ChatController(
  private val chatView: ChatView,
  private val project: Project,
  private val contextManager: AiContextItemManager,
  private val coroutineScope: CoroutineScope,
  private val chatHistory: ChatHistory = ChatHistory(),
  private val logger: Logger = logger<ChatController>(),
) {
  private val chatApiClient: ChatApiClient = project.service()

  init {
    chatView.onMessage { message ->
      coroutineScope.launch {
        handleViewOnMessage(message)
      }
    }
  }

  suspend fun clearChatWindow(subscriptionId: String) {
    val actionResponse = chatApiClient.clearChat(subscriptionId) ?: return
    if (actionResponse.aiAction.errors.isEmpty()) {
      chatHistory.clear()
      chatView.clear()
    }
  }

  suspend fun processNewUserPrompt(prompt: NewUserPromptRequest) {
    val subscriptionId = UUID.randomUUID().toString()
    val includeAdditionalContext = contextManager.shouldIncludeAdditionalContext()

    val subscription = chatApiClient.subscribeToUpdates(
      subscriptionId,
      includeAdditionalContext,
      ::handleAiMessageUpdate
    )

    val actionResponse = chatApiClient.processNewUserPrompt(
      subscriptionId = subscriptionId,
      question = prompt.content,
      context = prompt.context,
      aiContextItems = prompt.aiContextItems
    )

    if (actionResponse == null) {
      subscription?.cancel()
      return
    }

    if (actionResponse.aiAction.errors.isNotEmpty()) {
      subscription?.cancel()
      throw ChatException("Error processing new user prompt: ${actionResponse.aiAction.errors.joinToString(", ")}")
    }

    val record = ChatRecord(
      id = subscriptionId,
      role = ChatRecord.Role.USER,
      type = prompt.type,
      state = ChatRecord.State.READY,
      requestId = actionResponse.aiAction.requestId,
      context = prompt.context,
      content = prompt.content,
      contentHtml = null,
      extras = prompt.aiContextItems?.let { contextItems ->
        ChatRecord.Metadata(contextItems = contextItems)
      }
    )

    record.addToChat()
    chatView.show()

    if (record.type == ChatRecord.Type.CLEAR_CHAT) {
      return clearChatWindow(subscriptionId)
    }

    // if the chat starts a new conversation, we don't need an assistant response
    if (record.type == ChatRecord.Type.NEW_CONVERSATION) {
      subscription?.cancel()
      return
    }

    ChatRecord.pendingAssistantResponse(record).addToChat()
    if (subscription == null) {
      logger.debug("Unable to subscribe to chat messages. subscriptionId=$subscriptionId requestId=${record.requestId}")
      chatApiClient.getMessages(record.requestId, includeAdditionalContext).forEach { handleAiMessageUpdate(it) }
    }
  }

  private suspend fun handleViewOnMessage(message: ChatViewMessage) = when (message) {
    is AppReadyMessage -> {
      chatHistory.records.forEach { chatView.addRecord(NewRecordMessage(it)) }

      refreshContextCategories()
      refreshCurrentContextItems()
    }

    is NewPromptMessage -> {
      processNewUserPrompt(
        NewUserPromptRequest(
          content = message.content,
          type = ChatRecord.Type.fromContent(message.content),
          context = project.service<ChatRecordContextService>().getChatRecordContextOrNull(),
          aiContextItems = when {
            contextManager.shouldIncludeAdditionalContext() -> contextManager.retrieveSelectedContextItemsWithContent()
            else -> null
          }
        )
      )

      clearSelectedContextItems()
    }

    is CancelPromptMessage -> {
      chatApiClient.cancelPrompt(message.canceledPromptRequestId)
    }

    is TrackFeedbackMessage -> {
      trackFeedback(message)
    }

    is InsertCodeSnippetMessage -> {
      project.service<InsertCodeSnippetService>().insertCodeSnippet(message)
    }

    is SlashCommandsOpenedMessage -> {
      refreshContextCategories()
    }

    is ContextItemAddedMessage -> {
      addContextItem(message.item)
    }

    is ContextItemRemovedMessage -> {
      removeContextItem(message.item)
    }

    is ContextItemSearchQueryMessage -> {
      searchContextItems(message.category, message.query)
    }

    else -> logger.warn("Unhandled chat-webview message: $message")
  }

  private fun handleAiMessageUpdate(message: AiMessage) {
    val record = chatHistory.findViaRequestId(message.requestId, ChatRecord.Role.fromValue(message.role))
    if (record == null) {
      logger.warn("No record found for requestId: ${message.requestId}")
      return
    }

    record.chunkId = message.chunkId
    record.content = message.content
    record.contentHtml = message.contentHtml
    record.errors.addAll(message.errors)
    record.extras = message.extras

    val timestamp = runCatching { LocalDateTime.parse(message.timestamp) }.getOrNull()
    if (timestamp != null) record.timestamp = timestamp

    record.state = ChatRecord.State.READY
    chatView.updateRecord(UpdateRecordMessage(record))
  }

  private fun ChatRecord.addToChat() {
    chatHistory.addRecord(this)
    chatView.addRecord(NewRecordMessage(this))
  }

  fun selectionChanged(startLine: Int?, endLine: Int?) {
    val lineRange = when {
      startLine == null || endLine == null -> "None"
      startLine == endLine -> startLine.toString()
      else -> "$startLine-$endLine"
    }

    coroutineScope.launch {
      contextManager.getCurrentItems()
        .find { it.category == AiContextCategory.SNIPPET }
        ?.let { contextManager.remove(it) }

      contextManager.add(
        AiContextItem(
          id = "selection",
          category = AiContextCategory.SNIPPET,
          content = lineRange,
          metadata = AIContextItemMetadata(
            title = "Selected lines",
            enabled = true,
            subType = AIContextProviderType.SNIPPET
          )
        )
      )

      refreshCurrentContextItems()
    }
  }

  private fun clearSelectedContextItems() {
    coroutineScope.launch {
      contextManager.clearSelectedContextItems()
      refreshCurrentContextItems()
    }
  }

  private fun addContextItem(item: AiContextItem) {
    logger.info("Adding context item. item=${item.id}")

    coroutineScope.launch {
      contextManager.add(item)

      refreshCurrentContextItems()
    }
  }

  private fun removeContextItem(item: AiContextItem) {
    logger.info("Removing context item. item=${item.id}")

    coroutineScope.launch {
      contextManager.remove(item)

      refreshCurrentContextItems()
    }
  }

  private suspend fun searchContextItems(category: AiContextCategory, query: String) {
    logger.info("Searching for context items. category=$category, query=$query")

    coroutineScope.launch {
      chatView.setContextItemSearchResults(
        ContextItemsSearchResultMessage(
          results = contextManager.query(
            AiContextSearchQuery(
              category = category,
              query = query,
              workspaceFolders = listOfNotNull(
                project.workspaceFolder?.let {
                  AiContextItemWorkspaceFolder(it.uri, it.name)
                }
              )
            )
          )
        )
      )
    }
  }

  private fun refreshCurrentContextItems() {
    logger.info("Refreshing current context items.")

    coroutineScope.launch {
      chatView.setCurrentContextItems(
        ContextCurrentItemsResultMessage(
          items = contextManager.retrieveSelectedContextItemsWithContent()
        )
      )
    }
  }

  private fun refreshContextCategories() {
    logger.info("Refreshing context categories.")

    coroutineScope.launch {
      val result = contextManager.getAvailableCategories()

      chatView.setContextItemCategories(
        ContextCategoriesResultMessage(
          categories = result
        )
      )
    }
  }
}
