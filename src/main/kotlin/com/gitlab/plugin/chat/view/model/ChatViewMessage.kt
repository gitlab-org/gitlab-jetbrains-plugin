package com.gitlab.plugin.chat.view.model

import com.gitlab.plugin.chat.context.AiContextCategory
import com.gitlab.plugin.chat.context.AiContextItem
import com.gitlab.plugin.chat.model.ChatRecord
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

private const val EVENT_TYPE_KEY = "eventType"

@Serializable
sealed class ChatViewMessage {
  companion object {
    // Event types
    const val NEW_RECORD = "newRecord"
    const val UPDATE_RECORD = "updateRecord"
    const val NEW_PROMPT = "newPrompt"
    const val TRACK_FEEDBACK = "trackFeedback"
    const val APP_READY = "appReady"
    const val CLEAR = "clear"
    const val CANCEL_PROMPT = "cancelPrompt"
    const val INSERT_CODE_SNIPPET = "insertCodeSnippet"
    const val PROJECT_STATE_CHANGED = "projectStateChanged"
    const val SLASH_COMMANDS_OPENED = "slashCommandsOpened"

    const val CONTEXT_ITEM_ADDED = "contextItemAdded"
    const val CONTEXT_ITEM_REMOVED = "contextItemRemoved"
    const val CONTEXT_ITEM_SEARCH_QUERY = "contextItemSearchQuery"

    const val CONTEXT_CURRENT_ITEMS_RESULT = "contextCurrentItemsResult"
    const val CONTEXT_ITEMS_SEARCH_RESULT = "contextItemSearchResult"
    const val CONTEXT_CATEGORIES_RESULT = "contextCategoriesResult"

    private val format = Json {
      classDiscriminator = EVENT_TYPE_KEY
      encodeDefaults = true
      explicitNulls = false
    }

    fun fromJson(json: String): ChatViewMessage = format.decodeFromString(serializer(), json)
  }

  fun toJson(): String = format.encodeToString(serializer(), this)
}

/**
 * Message indicating that a new prompt has been submitted by the user
 * @property content The content of the new prompt.
 */
@Serializable
@SerialName(ChatViewMessage.NEW_PROMPT)
data class NewPromptMessage(
  val content: String
) : ChatViewMessage()

/**
 * Message indicating that the user has submitted feedback
 * @property content Type of content of the message
 * @property data Additional data attached to the message
 */
@Serializable
@SerialName(ChatViewMessage.TRACK_FEEDBACK)
data class TrackFeedbackMessage(
  val content: String,
  val data: Data
) : ChatViewMessage() {
  @Serializable
  data class Data(
    val improveWhat: String,
    val didWhat: String,
    val feedbackChoices: List<String>
  )
}

/**
 * Message indicating that a new chat record should be shown in the UI
 * @property record A chat record
 */
@Serializable
@SerialName(ChatViewMessage.NEW_RECORD)
data class NewRecordMessage(
  val record: ChatRecord
) : ChatViewMessage()

/**
 * Message indicating that a chat record should be updated in the UI
 * @property record A chat record
 */
@Serializable
@SerialName(ChatViewMessage.UPDATE_RECORD)
data class UpdateRecordMessage(
  val record: ChatRecord
) : ChatViewMessage()

/**
 * Message indicating that a chat record is cancelled in the UI
 * @property canceledPromptRequestId A chat record
 */
@Serializable
@SerialName(ChatViewMessage.CANCEL_PROMPT)
data class CancelPromptMessage(
  val canceledPromptRequestId: String
) : ChatViewMessage()

/**
 * Message indicating that the Vue application has been initialized
 */
@Serializable
@SerialName(ChatViewMessage.APP_READY)
data object AppReadyMessage : ChatViewMessage()

/**
 * Message indicating that the chat records should be cleared in the UI
 */
@Serializable
@SerialName(ChatViewMessage.CLEAR)
data object ClearChatMessage : ChatViewMessage()

/**
 * Message indicating that a code snippet should be inserted into the editor
 */
@Serializable
@SerialName(ChatViewMessage.INSERT_CODE_SNIPPET)
data class InsertCodeSnippetMessage(
  val snippet: String
) : ChatViewMessage()

/**
 * Message indicating that the state of the editor as been updated
 */
@Serializable
@SerialName(ChatViewMessage.PROJECT_STATE_CHANGED)
data class ProjectStateChangedMessage(
  val hasSelectedEditorInWindow: Boolean
) : ChatViewMessage()

/**
 * Message indicating that the slash commands menu has been opened
 */
@Serializable
@SerialName(ChatViewMessage.SLASH_COMMANDS_OPENED)
data object SlashCommandsOpenedMessage : ChatViewMessage()

@Serializable
@SerialName(ChatViewMessage.CONTEXT_CURRENT_ITEMS_RESULT)
data class ContextCurrentItemsResultMessage(
  val items: List<AiContextItem>
) : ChatViewMessage()

@Serializable
@SerialName(ChatViewMessage.CONTEXT_ITEMS_SEARCH_RESULT)
data class ContextItemsSearchResultMessage(
  val results: List<AiContextItem>
) : ChatViewMessage()

@Serializable
@SerialName(ChatViewMessage.CONTEXT_CATEGORIES_RESULT)
data class ContextCategoriesResultMessage(
  val categories: List<AiContextCategory>
) : ChatViewMessage()

@Serializable
@SerialName(ChatViewMessage.CONTEXT_ITEM_ADDED)
data class ContextItemAddedMessage(
  val item: AiContextItem
) : ChatViewMessage()

@Serializable
@SerialName(ChatViewMessage.CONTEXT_ITEM_REMOVED)
data class ContextItemRemovedMessage(
  val item: AiContextItem
) : ChatViewMessage()

@Serializable
@SerialName(ChatViewMessage.CONTEXT_ITEM_SEARCH_QUERY)
data class ContextItemSearchQueryMessage(
  val query: String,
  val category: AiContextCategory
) : ChatViewMessage()
