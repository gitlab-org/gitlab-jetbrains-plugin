package com.gitlab.plugin.chat.view.abstraction

import com.gitlab.plugin.chat.view.model.*

/**
 * Represents a chat view
 */
interface ChatView {
  /**
   * Displays the chat view to the user.
   */
  fun show()

  /**
   * Clear existing chat records.
   */
  fun clear()

  /**
   * Updates an existing chat record.
   *
   * @param message The [UpdateRecordMessage] containing the updated record
   */
  fun updateRecord(message: UpdateRecordMessage)

  /**
   * Adds a new chat record to the chat view
   *
   * @param message The [NewRecordMessage] containing the new record
   */
  fun addRecord(message: NewRecordMessage)

  /**
   * Sends an update about the current project state
   *
   * @param message The [ProjectStateChangedMessage] containing the project state.
   */
  fun projectStateChanged(message: ProjectStateChangedMessage)

  /**
   * Sets the current context items
   *
   * @param message The [ContextCurrentItemsResultMessage] containing the current context items
   */
  fun setCurrentContextItems(message: ContextCurrentItemsResultMessage)

  /**
   * Sets the current context categories
   *
   * @param message The [ContextCategoriesResultMessage] containing the current context categories
   */
  fun setContextItemCategories(message: ContextCategoriesResultMessage)

  /**
   * Sets the current context search results
   *
   * @param message The [ContextItemsSearchResultMessage] containing the current context search results
   */
  fun setContextItemSearchResults(message: ContextItemsSearchResultMessage)

  /**
   * Registers a callback to be invoked when a new message is received from the chat view.
   *
   * @param block A lambda function that takes a [ChatViewMessage] as a parameter and
   * handles the incoming chat message.
   */
  fun onMessage(block: (message: ChatViewMessage) -> Unit)
}
