package com.gitlab.plugin.chat

import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.intellij.openapi.application.ReadAction
import com.intellij.openapi.components.Service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange

@Service(Service.Level.PROJECT)
class ChatRecordContextService(
  private val project: Project,
) {

  private val logger = logger<ChatRecordContextService>()

  fun getChatRecordContextOrNull(
    textRange: TextRange? = null,
    selectedEditor: Editor? = null
  ): ChatRecordContext? {
    return ReadAction.compute<ChatRecordContext?, Throwable> {
      val editor = selectedEditor ?: getEditor() ?: run {
        logger.warn("No editor is selected.")
        return@compute null
      }

      val document = editor.document
      val documentText = document.charsSequence
      val documentLength = documentText.length

      val range = textRange ?: run {
        val caret = editor.caretModel.primaryCaret
        TextRange(caret.selectionStart, caret.selectionEnd)
      }

      if (range.startOffset > documentLength || range.endOffset > documentLength) {
        logger.warn("Position is out of bounds.")
        return@compute null
      }

      val fileName = getFileName(editor) ?: run {
        logger.warn("Could not determine file name")
        return@compute null
      }

      val selectedText = documentText.substring(range.startOffset, range.endOffset)
      val contentAboveCursor = documentText.take(range.startOffset).toString()
      val contentBelowCursor = documentText.drop(range.endOffset).toString()

      val fileContext = ChatRecordFileContext(
        fileName = fileName,
        selectedText = selectedText,
        contentAboveCursor = contentAboveCursor,
        contentBelowCursor = contentBelowCursor
      )

      return@compute ChatRecordContext(fileContext)
    }
  }

  private fun getEditor(): Editor? =
    FileEditorManager.getInstance(project).selectedTextEditor

  private fun getFileName(editor: Editor): String? =
    editor.virtualFile?.path?.removePrefix(project.basePath.orEmpty())
}
