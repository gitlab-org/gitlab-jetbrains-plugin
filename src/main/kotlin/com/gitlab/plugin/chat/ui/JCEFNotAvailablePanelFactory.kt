package com.gitlab.plugin.chat.ui

import com.gitlab.plugin.actions.chat.DUO_CHAT_TOOL_WINDOW_ID
import com.gitlab.plugin.ui.GitLabIcons
import com.intellij.icons.AllIcons
import com.intellij.ide.DataManager
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.ui.JBColor
import java.awt.*
import javax.swing.*
import javax.swing.event.HyperlinkEvent

@Suppress("MagicNumber", "LongMethod")
object JCEFNotAvailablePanelFactory {
  fun create(): JPanel {
    val chooseRuntime = ActionManager.getInstance().getAction("ChooseRuntime")
    val registry = ActionManager.getInstance().getAction("ShowRegistry")

    val jcefSupportingRuntimeExampleSrc = getResourceImage("jcef_supporting_runtime_example_v17_3.png")
    val registryKeyExampleSrc = getResourceImage("registry_key_example_v17_3.png")
    val runtimeVersionMatrixSrc = getResourceImage("runtime_version_matrix_v17_3.png")

    val titlePanel = JPanel().apply {
      add(
        JLabel("GitLab Duo Chat", GitLabIcons.NotificationIcon, JLabel.RIGHT).apply {
          font = font.deriveFont(18f)
          iconTextGap = 10

          alignmentX = Component.CENTER_ALIGNMENT
        }
      )
    }

    val contentPanel = JPanel().apply {
      layout = BoxLayout(this, BoxLayout.Y_AXIS)
      alignmentX = Component.CENTER_ALIGNMENT

      add(
        JLabel("Unable to launch Duo Chat. Browser Required.", AllIcons.General.ShowInfos, JLabel.RIGHT).apply {
          border = BorderFactory.createCompoundBorder(
            BorderFactory.createLineBorder(JBColor.namedColor("Label.foreground"), 1, true),
            BorderFactory.createEmptyBorder(3, 3, 3, 3)
          )

          font = font.deriveFont(14f)
          iconTextGap = 10

          alignmentX = Component.CENTER_ALIGNMENT
        }
      )

      add(
        JEditorPane(
          "text/html",
          """
            <p>This issue can happen if your IDE is running on a Java runtime<br/> that doesn't support the Java Chromium Embedded Framework (JCEF).</p>
            <p><b>To install a JCEF-supporting runtime</b></p>
            <ol>
              <li>In your IDE, open <b><a href="https://registry">Help> Find Action: Registry</a></b>.</li>
              <li style="margin-top: 5px">Scroll to or search for <code>ide.browser.jcef.sandbox.enable</code>, and clear the checkbox:
                <div style="margin-top: 4px;">
                  <img src="$registryKeyExampleSrc" width="400" style="display: block; margin: 0 auto;" />
                </div>
              </li>
              <li style="margin-top: 5px">Close the dialog, and restart your IDE.</li>
              <li style="margin-top: 5px">Open <b><a href="https://choose-runtime">Help > Find Action: Choose Boot runtime for the IDE</a></b>.</li>
              <li style="margin-top: 5px">In <b>IDE and Java Versions</b>, identify your IDE version.</li>
              <li style="margin-top: 5px">
                In <b>Choose Boot Runtime for the IDE</b>, for <b>New</b>, select the boot runtime for your IDE version:
                <div style="margin-top: 4px;">
                  <img src="$runtimeVersionMatrixSrc" width="400" style="display: block; margin: 0 auto;" />
                </div>
                <div style="margin-top: 6px;">
                  <img src="$jcefSupportingRuntimeExampleSrc" width="400" style="display: block; margin: 0 auto;" />
                </div>
              </li>
              <li style="margin-top: 5px">Restart your IDE.</li>
            </ol>
          """.trimIndent()
        ).apply {
          font = font.deriveFont(14f)
          putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true)

          isEditable = false
          isOpaque = false

          alignmentX = Component.CENTER_ALIGNMENT

          border = BorderFactory.createEmptyBorder(0, 20, 0, 0)

          addHyperlinkListener { event ->
            if (event.eventType == HyperlinkEvent.EventType.ACTIVATED) {
              when (event.url.host) {
                "registry" -> registry.actionPerformed(
                  AnActionEvent.createFromDataContext(
                    DUO_CHAT_TOOL_WINDOW_ID,
                    null,
                    DataManager.getInstance().getDataContext(this)
                  )
                )
                else -> chooseRuntime.actionPerformed(
                  AnActionEvent.createFromDataContext(
                    DUO_CHAT_TOOL_WINDOW_ID,
                    null,
                    DataManager.getInstance().getDataContext(this)
                  )
                )
              }
            }
          }
        }
      )
    }

    return JPanel().apply {
      layout = BorderLayout(0, 10)
      border = BorderFactory.createEmptyBorder(60, 0, 0, 0)

      add(titlePanel, BorderLayout.PAGE_START)
      add(contentPanel, BorderLayout.CENTER)
    }
  }

  private fun getResourceImage(fileName: String): String? =
    javaClass.classLoader.getResource("assets/$fileName")?.toString()
}
