package com.gitlab.plugin.chat.codesnippets

import com.gitlab.plugin.chat.quickchat.QUICK_CHAT_SESSION_KEY
import com.gitlab.plugin.chat.view.model.InsertCodeSnippetMessage
import com.gitlab.plugin.util.code.CodeFormatter
import com.gitlab.plugin.util.code.CodeFormattingContext
import com.intellij.openapi.application.readAction
import com.intellij.openapi.application.runInEdt
import com.intellij.openapi.application.runUndoTransparentWriteAction
import com.intellij.openapi.components.Service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiDocumentManager

@Service(Service.Level.PROJECT)
class InsertCodeSnippetService(val project: Project) {
  suspend fun insertCodeSnippet(message: InsertCodeSnippetMessage) {
    FileEditorManager.getInstance(project).selectedTextEditor?.apply {
      val codeFormatter = CodeFormatter()

      val (startOffset, endOffset) = readAction {
        when {
          (selectionModel.hasSelection()) -> selectionModel.selectionStart to selectionModel.selectionEnd
          else -> caretModel.offset to caretModel.offset
        }
      }

      val psiFile = readAction {
        PsiDocumentManager.getInstance(this@InsertCodeSnippetService.project).getPsiFile(document)
      } ?: return add(startOffset, endOffset, message.snippet)

      val context = readAction {
        CodeFormattingContext(
          this@InsertCodeSnippetService.project,
          editor = this,
          prefix = document.charsSequence.take(startOffset).toString(),
          suffix = document.charsSequence.drop(endOffset).toString(),
          language = psiFile.language
        )
      }

      val code = codeFormatter.format(message.snippet, context)

      add(startOffset, endOffset, code)
    }
  }

  private fun Editor.add(startOffset: Int, endOffset: Int, code: String) {
    runInEdt {
      runUndoTransparentWriteAction {
        if (startOffset != endOffset) {
          document.replaceString(startOffset, endOffset, code)
        } else {
          document.insertString(startOffset, code)
        }
      }

      getUserData(QUICK_CHAT_SESSION_KEY)?.selectionChanged(this)
    }
  }
}
