package com.gitlab.plugin.chat.quickchat.markdown

import org.commonmark.parser.IncludeSourceSpans
import org.commonmark.parser.Parser

val markdownParser: Parser by lazy {
  Parser.builder().includeSourceSpans(IncludeSourceSpans.BLOCKS_AND_INLINES).build()
}
