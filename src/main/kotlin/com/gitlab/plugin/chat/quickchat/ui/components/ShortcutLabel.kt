package com.gitlab.plugin.chat.quickchat.ui.components

import com.intellij.openapi.actionSystem.KeyboardShortcut
import com.intellij.openapi.keymap.KeymapUtil
import java.awt.Cursor
import java.awt.FlowLayout
import java.awt.Font
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.BorderFactory
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.UIManager

private typealias Callback = () -> Unit

@Suppress("MagicNumber")
class ShortcutLabel(
  shortcut: String,
  text: String,
  action: Callback? = null
) : JPanel(FlowLayout(FlowLayout.CENTER, 2, 0)) {
  init {
    border = BorderFactory.createEmptyBorder(2, 1, 2, 1)
    background = UIManager.getColor("Label.background")

    add(
      JLabel(KeymapUtil.getShortcutText(KeyboardShortcut.fromString(shortcut))).apply {
        font = font.deriveFont(11f)
        foreground = UIManager.getColor("Label.foreground")
      }
    )

    add(
      JLabel(text).apply {
        font = Font(font.name, Font.ITALIC, 11)
        foreground = UIManager.getColor("Label.disabledForeground")
        // Ensure italicized text doesn't get last pixel cut off
        border = BorderFactory.createEmptyBorder(0, 1, 0, 1)
      }
    )

    if (action != null) {
      addMouseListener(ShortcutActionMouseAdapter(action))
    }
  }

  inner class ShortcutActionMouseAdapter(private val action: Callback) : MouseAdapter() {
    override fun mouseClicked(e: MouseEvent) {
      action()
    }

    override fun mouseEntered(e: MouseEvent?) {
      cursor = Cursor(Cursor.HAND_CURSOR)
    }

    override fun mouseExited(e: MouseEvent?) {
      cursor = Cursor.getDefaultCursor()
    }
  }
}
