package com.gitlab.plugin.chat.quickchat

import com.gitlab.plugin.chat.quickchat.messages.QuickChatUIMessage
import com.gitlab.plugin.chat.quickchat.ui.QuickChatUI
import com.gitlab.plugin.chat.view.abstraction.ChatView
import com.gitlab.plugin.chat.view.model.*
import com.gitlab.plugin.telemetry.tracked.actions.trackQuickChatSlashCommand
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.util.removeUserData

class QuickChatView(
  private val editor: Editor,
  private val quickChatUI: QuickChatUI,
) : ChatView {
  init {
    quickChatUI.onMessageReceived { message ->
      handleUIMessage(message)
    }
  }

  private var messageHandler: (message: ChatViewMessage) -> Unit = {}

  override fun show() {
    // Nothing to do for now in the context of quick chat.
  }

  override fun clear() {
    // Nothing to do for now in the context of quick chat.
  }

  override fun updateRecord(message: UpdateRecordMessage) {
    quickChatUI.dispatch(message)
  }

  override fun addRecord(message: NewRecordMessage) {
    quickChatUI.dispatch(message)
  }

  override fun projectStateChanged(message: ProjectStateChangedMessage) {
    // Nothing to do for now in the context of quick chat.
  }

  override fun setCurrentContextItems(message: ContextCurrentItemsResultMessage) {
    quickChatUI.dispatch(message)
  }

  override fun setContextItemCategories(message: ContextCategoriesResultMessage) {
    quickChatUI.dispatch(message)
  }

  override fun setContextItemSearchResults(message: ContextItemsSearchResultMessage) {
    // Nothing to do for now in the context of quick chat.
  }

  override fun onMessage(block: (message: ChatViewMessage) -> Unit) {
    messageHandler = block

    // This will be called once after the UI is displayed and the controller is created.
    messageHandler(AppReadyMessage)
  }

  private fun handleUIMessage(message: QuickChatUIMessage) {
    when (message) {
      is QuickChatUIMessage.Close -> {
        editor.getUserData(QUICK_CHAT_SESSION_KEY)?.close()
        editor.removeUserData(QUICK_CHAT_SESSION_KEY)
      }
      is QuickChatUIMessage.NewUserPrompt -> {
        messageHandler.invoke(NewPromptMessage(message.prompt))
        trackQuickChatSlashCommand(message.prompt)
      }
      is QuickChatUIMessage.InsertCodeSnippet -> {
        messageHandler.invoke(InsertCodeSnippetMessage(message.snippet))
      }
    }
  }
}
