package com.gitlab.plugin.chat.model

import com.gitlab.plugin.chat.context.AiContextItem

data class NewUserPromptRequest(
  val content: String,
  val type: ChatRecord.Type,
  val context: ChatRecordContext? = null,
  val aiContextItems: List<AiContextItem>? = null
)
