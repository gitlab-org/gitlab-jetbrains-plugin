package com.gitlab.plugin.inspection

import com.intellij.codeInsight.daemon.DaemonCodeAnalyzer
import com.intellij.codeInsight.daemon.impl.DaemonCodeAnalyzerEx
import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.components.Service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange

@Service(Service.Level.PROJECT)
class FileProblemsService(private val project: Project) {

  init {
    project.messageBus.connect().subscribe(
      DaemonCodeAnalyzer.DAEMON_EVENT_TOPIC,
      object : DaemonCodeAnalyzer.DaemonListener {
        override fun daemonFinished() {
          isDataOutdated = true
        }
      }
    )
  }

  private val logger = logger<FileProblemsService>()

  private var isDataOutdated = true

  private val currentFileErrors = mutableSetOf<CurrentFileProblem>()

  fun getCurrentFileErrors(): Set<CurrentFileProblem> {
    if (isDataOutdated) {
      val editor = FileEditorManager.getInstance(project).selectedTextEditor ?: run {
        logger.warn("No editor selected")
        return emptySet()
      }

      val document = editor.document

      currentFileErrors.clear()

      DaemonCodeAnalyzerEx.processHighlights(
        document,
        project,
        HighlightSeverity.ERROR,
        0,
        document.textLength,
      ) { highlightInfo ->
        currentFileErrors.add(
          CurrentFileProblem(
            highlightInfo.description,
            TextRange(highlightInfo.startOffset, highlightInfo.endOffset),
            document.getLineNumber(highlightInfo.startOffset)
          )
        )

        true
      }

      isDataOutdated = false
    }

    return currentFileErrors.toSet()
  }
}
