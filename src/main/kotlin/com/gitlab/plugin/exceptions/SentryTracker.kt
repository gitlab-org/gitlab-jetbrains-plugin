package com.gitlab.plugin.exceptions

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.util.IdeMetadata
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.util.application
import io.sentry.Sentry
import io.sentry.SentryEvent
import io.sentry.SentryOptions

@Service(Service.Level.APP)
class SentryTracker {
  private val duoPersistentSettings by lazy { application.service<DuoPersistentSettings>() }
  private val exceptionSanitizer by lazy { ExceptionSanitizer() }

  init {
    val ideMetadata = IdeMetadata.get()

    Sentry.init { options ->
      options.dsn = "https://edf6c228bc1e6ad9809a80dbc56fcb72@new-sentry.gitlab.net/35"
      options.tracesSampleRate = 1.0
      options.environment = when (BuildConfig.RELEASE_CHANNEL) {
        "alpha" -> "alpha"
        else -> "stable"
      }
      options.release = "${ideMetadata.extensionName} ${ideMetadata.extensionVersion}"
      options.isSendDefaultPii = false

      // no breadcrumbs until we have advanced scrubbing in place to prevent private information from leaking
      options.maxBreadcrumbs = 1
      options.beforeSend = SentryOptions.BeforeSendCallback { event, _ -> prepareEvent(event) }
    }
  }

  fun trackException(e: Throwable) {
    if (!duoPersistentSettings.telemetryEnabled || !isSentryTrackingEnabled()) {
      return
    }

    val ideMetadata = IdeMetadata.get()
    Sentry.setExtra("ide", "${ideMetadata.vendor} ${ideMetadata.name} ${ideMetadata.version}")
    Sentry.captureException(exceptionSanitizer.sanitize(e))
  }

  private fun prepareEvent(event: SentryEvent): SentryEvent? {
    if (!duoPersistentSettings.telemetryEnabled || !isSentryTrackingEnabled()) {
      return null
    }

    event.serverName = null
    event.contexts.remove("culture")
    event.contexts.remove("device")
    event.contexts.remove("app")

    return event
  }
}
