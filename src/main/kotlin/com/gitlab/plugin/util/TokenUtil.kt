package com.gitlab.plugin.util

import com.gitlab.plugin.authentication.GitLabAuthorizationToken
import com.intellij.credentialStore.CredentialAttributes
import com.intellij.credentialStore.Credentials
import com.intellij.credentialStore.generateServiceName
import com.intellij.ide.passwordSafe.PasswordSafe
import com.intellij.openapi.diagnostic.logger
import kotlinx.serialization.json.Json
import java.time.Instant

object TokenUtil {
  private val logger = logger<TokenUtil>()

  fun getToken(): String? {
    val credentialAttributes = createTokenCredentialAttributes()
    return PasswordSafe.instance.getPassword(credentialAttributes)
  }

  fun setToken(token: String) {
    val credentialAttributes = createTokenCredentialAttributes()
    PasswordSafe.instance.set(credentialAttributes, Credentials("", token))
  }

  private fun createTokenCredentialAttributes(): CredentialAttributes =
    CredentialAttributes(generateServiceName(GitLabUtil.SERVICE_NAME, "GitLab Personal Access Token"))

  fun getOAuthToken(): GitLabAuthorizationToken? {
    val serializedOAuthToken = PasswordSafe.instance.getPassword(oAuthCredentialAttributes)
    return if (serializedOAuthToken.isNullOrEmpty()) {
      logger.info("Loaded OAuth token is null.")
      null
    } else {
      val token = Json.decodeFromString<GitLabAuthorizationToken>(serializedOAuthToken)
      logger.info("Loaded OAuth token expires at ${Instant.ofEpochSecond(token.tokenExpirationTimestamp)}")
      return token
    }
  }

  fun setOAuthToken(token: GitLabAuthorizationToken?) {
    val jsonOAuthToken = token?.let { Json.encodeToString(GitLabAuthorizationToken.serializer(), it) }.orEmpty()
    PasswordSafe.instance.set(oAuthCredentialAttributes, Credentials("", jsonOAuthToken))
  }

  private val oAuthCredentialAttributes =
    CredentialAttributes(generateServiceName(GitLabUtil.SERVICE_NAME, "GitLab OAuth Token"))
}
