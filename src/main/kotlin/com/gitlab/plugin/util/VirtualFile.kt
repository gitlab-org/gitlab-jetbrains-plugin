package com.gitlab.plugin.util

import com.intellij.openapi.vfs.VirtualFile
import com.intellij.openapi.vfs.toNioPathOrNull

val VirtualFile.uri
  get() = toNioPathOrNull()?.toUri()?.toString() ?: url
