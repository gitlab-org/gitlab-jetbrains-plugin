package com.gitlab.plugin.util.project

import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project

@Suppress("TooGenericExceptionCaught")
inline fun <reified T : Any> Project.tryGetService(): T? {
  return try {
    service<T>()
  } catch (t: Throwable) {
    logger<Project>().warn("Could not get GitLab project service (${T::class.java}).", t)
    null
  }
}
