package com.gitlab.plugin.authentication

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.util.GitLabUtil
import com.gitlab.plugin.util.removeTrailingSlash
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.components.service
import com.intellij.util.application

const val DEFAULT_GITLAB_1PASSWORD_SECRET_REFERENCE = "op://Private/GitLab Personal Access Token/token"

@State(name = "GitLabAccount", storages = [Storage(value = "gitlab.xml")], reportStatistic = false)
class DuoPersistentSettings : PersistentStateComponent<DuoPersistentSettings.State> {
  data class State(
    var url: String = GitLabUtil.GITLAB_DEFAULT_URL,

    @Deprecated(
      "To be removed in the future once everyone is using version > 3.1.0",
      ReplaceWith("codeSuggestionsEnabled")
    )
    var enabled: Boolean = true,

    var codeSuggestionsEnabled: Boolean = true,
    var telemetryEnabled: Boolean = true,
    var trackingUrl: String = BuildConfig.SNOWPLOW_COLLECTOR_URL,
    var ignoreCertificateErrors: Boolean = false,
    var duoChatEnabled: Boolean = true,
    var oauthEnabled: Boolean = false,
    var integrate1PasswordCLI: Boolean = false,
    var integrate1PasswordCLISecretReference: String = "",
    var integrate1PasswordAccount: String? = null
  )

  private var state: State = State()

  override fun getState(): State = state

  override fun loadState(state: State) {
    this.state = state
  }

  fun toggleCodeSuggestions(): Boolean {
    this.codeSuggestionsEnabled = !this.codeSuggestionsEnabled

    return this.codeSuggestionsEnabled
  }

  var url
    get() = state.url
    set(value) {
      state.url = value.removeTrailingSlash()
    }

  var codeSuggestionsEnabled: Boolean
    get() {
      state.codeSuggestionsEnabled = state.enabled
      return state.codeSuggestionsEnabled
    }
    set(value) {
      state.codeSuggestionsEnabled = value
      state.enabled = value
    }

  var telemetryEnabled
    get() = state.telemetryEnabled
    set(value) {
      state.telemetryEnabled = value
    }

  var trackingUrl
    get() = state.trackingUrl
    set(value) {
      state.trackingUrl = value
    }

  var ignoreCertificateErrors
    get() = state.ignoreCertificateErrors
    set(value) {
      state.ignoreCertificateErrors = value
    }

  var duoChatEnabled
    get() = state.duoChatEnabled
    set(value) {
      state.duoChatEnabled = value
    }

  var oauthEnabled
    get() = state.oauthEnabled
    set(value) {
      state.oauthEnabled = value
    }

  var integrate1PasswordCLI
    get() = state.integrate1PasswordCLI
    set(value) {
      state.integrate1PasswordCLI = value
    }
  var integrate1PasswordCLISecretReference
    get() = state.integrate1PasswordCLISecretReference
    set(value) {
      state.integrate1PasswordCLISecretReference = value
    }
  var integrate1PasswordAccount
    get() = state.integrate1PasswordAccount
    set(value) {
      state.integrate1PasswordAccount = value
    }

  fun gitlabRealm() = if (url.matches(GITLAB_COM_REGEX)) GITLAB_REALM_SAAS else GITLAB_REALM_SELF_MANAGED

  companion object {
    const val GITLAB_REALM_SAAS = "saas"
    const val GITLAB_REALM_SELF_MANAGED = "self-managed"
    private val GITLAB_COM_REGEX = Regex("https?://gitlab\\.com")

    fun getInstance(): DuoPersistentSettings = application.service()
  }
}
