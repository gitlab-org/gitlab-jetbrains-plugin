package com.gitlab.plugin.git

import com.gitlab.plugin.services.GitRepositoryService
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import git4idea.commands.Git
import git4idea.commands.GitCommand
import git4idea.commands.GitLineHandler

@Service(Service.Level.PROJECT)
class GitDiffProvider(private val project: Project) {
  private val gitLabRepositoryService by lazy { project.service<GitRepositoryService>() }

  fun getDiffWithHead(repositoryUri: String): String? {
    val repository = gitLabRepositoryService.findRepository(repositoryUri) ?: return null

    val diffHandler = GitLineHandler(project, repository.root, GitCommand.DIFF)
    diffHandler.addParameters("HEAD")

    return Git.getInstance().runCommand(diffHandler).outputAsJoinedString
  }

  fun getDiffWithBranch(repositoryUri: String, branch: String): String? {
    val repository = gitLabRepositoryService.findRepository(repositoryUri) ?: return null

    val diffHandler = GitLineHandler(project, repository.root, GitCommand.DIFF)
    diffHandler.addParameters(branch)

    return Git.getInstance().runCommand(diffHandler).outputAsJoinedString
  }
}
