package com.gitlab.plugin.lsp.capabilities

import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.util.uri
import com.google.gson.JsonObject
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFileManager
import com.intellij.openapi.vfs.newvfs.BulkFileListener
import com.intellij.openapi.vfs.newvfs.events.VFileCopyEvent
import com.intellij.openapi.vfs.newvfs.events.VFileCreateEvent
import com.intellij.openapi.vfs.newvfs.events.VFileDeleteEvent
import com.intellij.openapi.vfs.newvfs.events.VFileEvent
import com.intellij.openapi.vfs.newvfs.events.VFileMoveEvent
import com.intellij.openapi.vfs.newvfs.events.VFilePropertyChangeEvent
import com.intellij.util.messages.MessageBusConnection
import org.eclipse.lsp4j.*
import org.eclipse.lsp4j.jsonrpc.messages.Either
import java.nio.file.FileSystems
import java.nio.file.PathMatcher
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap
import kotlin.io.path.Path

@Suppress("MagicNumber", "CyclomaticComplexMethod", "NestedBlockDepth")
class DidChangeWatchedFileCapability(
  private val project: Project,
  private val sendBatchDelayInMs: Long = 5000L // Default to 5 seconds
) : LanguageServerCapability, BulkFileListener {
  private val logger = logger<DidChangeWatchedFileCapability>()

  private val registrar = HashMap<String, List<FileSystemWatcher>>()
  private val watchers
    get() = registrar.values.distinct().flatten()

  private var connection: MessageBusConnection? = null

  private val processBatchedEvents: MutableList<FileEvent> = Collections.synchronizedList(mutableListOf())
  private lateinit var batchSendScheduler: ScheduledExecutorService

  override fun register(id: String, options: JsonObject) {
    if (registrar.containsKey(id)) {
      return
    }

    // Start listening to VFS Changes on registering the first watcher
    if (registrar.isEmpty()) {
      connection = project.messageBus.connect().also { conn ->
        conn.subscribe(VirtualFileManager.VFS_CHANGES, this)
      }

      batchSendScheduler = Executors.newScheduledThreadPool(1)
      batchSendScheduler.scheduleAtFixedRate({
        synchronized(processBatchedEvents) {
          logger.debug("batchSendScheduler command running.")

          if (processBatchedEvents.isNotEmpty()) {
            logger.debug("Sending virtual file system events batch to language server.")
            project.service<GitLabLanguageServerService>().lspServer?.workspaceService?.didChangeWatchedFiles(
              DidChangeWatchedFilesParams(processBatchedEvents.toList())
            )
            processBatchedEvents.clear()
          }
        }
      }, 0, sendBatchDelayInMs, TimeUnit.MILLISECONDS)
    }

    registrar[id] = options.toDidChangeWatchedFilesRegistrationOptions().watchers
  }

  override fun unregister(id: String) {
    registrar.remove(id)

    if (registrar.isEmpty()) {
      connection?.dispose().also { connection = null }

      batchSendScheduler.shutdownNow()
    }
  }

  override fun unregisterAll() {
    val watchers = registrar.keys.toList()

    watchers.forEach { id -> unregister(id) }
  }

  override fun before(events: MutableList<out VFileEvent>) {
    events.forEach { event ->
      for (watcher in watchers) {
        if (watcher.globPatternMatcher().matches(event.nioPath)) {
          val lspFileEvent = when {
            event is VFileMoveEvent -> FileEvent(event.file.uri, FileChangeType.Deleted)
            event is VFilePropertyChangeEvent && event.propertyName == "name" -> FileEvent(
              event.file.uri,
              FileChangeType.Deleted
            )
            else -> null
          } ?: continue

          if (watcher.isInterestedBy(lspFileEvent)) {
            processBatchedEvents.add(lspFileEvent)
          }
        }
      }
    }
  }

  override fun after(events: MutableList<out VFileEvent>) {
    events.forEach { event ->
      for (watcher in watchers) {
        if (watcher.globPatternMatcher().matches(event.nioPath)) {
          val lspFileEvent = when {
            event is VFileCreateEvent -> event.file?.let { FileEvent(it.uri, FileChangeType.Created) }
            event is VFileDeleteEvent -> FileEvent(event.file.uri, FileChangeType.Deleted)
            event is VFileCopyEvent -> event.findCreatedFile()?.let { FileEvent(it.uri, FileChangeType.Created) }
            event is VFileMoveEvent -> FileEvent(event.file.uri, FileChangeType.Created)
            event is VFilePropertyChangeEvent && event.propertyName == "name" -> FileEvent(
              event.file.uri,
              FileChangeType.Created
            )
            else -> event.file?.let { FileEvent(it.uri, FileChangeType.Changed) }
          } ?: continue

          if (watcher.isInterestedBy(lspFileEvent)) {
            processBatchedEvents.add(lspFileEvent)
          }
        }
      }
    }
  }

  private val VFileEvent.nioPath
    get() = Path(path)

  private fun FileSystemWatcher.globPatternMatcher(): PathMatcher {
    return FileSystems.getDefault().getPathMatcher("glob:${globPattern.left}")
  }

  private fun FileSystemWatcher.isInterestedBy(event: FileEvent): Boolean {
    // Watchers are interested in all WatchKind is none are specified
    val kindValue = kind ?: return true

    return when (event.type) {
      FileChangeType.Created -> (kindValue and WatchKind.Create) == WatchKind.Create
      FileChangeType.Deleted -> (kindValue and WatchKind.Delete) == WatchKind.Delete
      else -> (kindValue and WatchKind.Change) == WatchKind.Change
    }
  }
}

fun JsonObject.toDidChangeWatchedFilesRegistrationOptions(): DidChangeWatchedFilesRegistrationOptions {
  val watchers = this["watchers"]?.asJsonArray?.mapNotNull { element ->
    val watcher = element.asJsonObject

    val globPattern = watcher["globPattern"]?.asJsonPrimitive?.asString ?: return@mapNotNull null
    FileSystemWatcher(Either.forLeft(globPattern), watcher["kind"]?.asInt)
  }.orEmpty()

  return DidChangeWatchedFilesRegistrationOptions(watchers)
}
