package com.gitlab.plugin.lsp.capabilities

import com.google.gson.JsonObject

interface LanguageServerCapability {
  fun register(id: String, options: JsonObject)
  fun unregister(id: String)
  fun unregisterAll()
}
