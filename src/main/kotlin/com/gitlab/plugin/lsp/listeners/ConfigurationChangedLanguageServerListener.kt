package com.gitlab.plugin.lsp.listeners

import com.gitlab.plugin.lsp.params.CodeCompletion
import com.gitlab.plugin.lsp.params.DuoChatFeature
import com.gitlab.plugin.lsp.params.Settings
import com.gitlab.plugin.lsp.params.toDto
import com.gitlab.plugin.lsp.services.CaCertificateService
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.lsp.settings.LanguageServerSettings
import com.gitlab.plugin.workspace.DuoConfigurationChangedListener
import com.gitlab.plugin.workspace.WorkspaceSettings
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.util.application
import org.eclipse.lsp4j.DidChangeConfigurationParams

class ConfigurationChangedLanguageServerListener(
  private val project: Project
) : DuoConfigurationChangedListener, LanguageServerSettingsChangedListener {
  override fun onConfigurationChange(settings: WorkspaceSettings) {
    if (settings.token.isNotBlank()) {
      project.service<GitLabLanguageServerService>().lspServer?.workspaceService?.didChangeConfiguration(
        DidChangeConfigurationParams(
          Settings(
            baseUrl = settings.url,
            token = settings.token,
            telemetry = settings.telemetry.toDto(),
            ignoreCertificateErrors = settings.ignoreCertificateErrors,
            duoChat = DuoChatFeature(enabled = settings.duoChatEnabled),
            codeCompletion = CodeCompletion(enabled = settings.codeSuggestionsEnabled)
          )
        )
      )
    }
  }

  override fun onLanguageServerSettingsChanged() {
    val languageServerSettings = project.service<LanguageServerSettings>()

    project.service<GitLabLanguageServerService>().lspServer?.workspaceService?.didChangeConfiguration(
      DidChangeConfigurationParams(
        Settings(
          logLevel = languageServerSettings.state.workspaceSettings.logLevel,
          openTabsContext = languageServerSettings.state.workspaceSettings.codeCompletion.enableOpenTabsContext,
          codeCompletion = languageServerSettings.state.workspaceSettings.codeCompletion.toDto(),
          httpAgentOptions = languageServerSettings.state.workspaceSettings.httpAgentOptions.toDto().also {
            if (languageServerSettings.state.workspaceSettings.httpAgentOptions.pass) {
              it.ca = application.service<CaCertificateService>().caCertificateFile?.absolutePath
            }
          },
          featureFlags = languageServerSettings.state.workspaceSettings.featureFlags.toDto()
        )
      )
    )
  }
}
