package com.gitlab.plugin.lsp.listeners

import com.intellij.util.messages.Topic
import com.intellij.util.messages.Topic.ProjectLevel

interface LanguageServerStartedListener {
  companion object {
    @ProjectLevel
    val LANGUAGE_SERVER_STARTED_TOPIC = Topic(
      LanguageServerStartedListener::class.java.name,
      LanguageServerStartedListener::class.java
    )
  }

  fun onLanguageServerStarted()
}
