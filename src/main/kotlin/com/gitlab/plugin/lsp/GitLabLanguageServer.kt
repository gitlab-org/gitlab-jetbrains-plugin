package com.gitlab.plugin.lsp

import com.gitlab.plugin.chat.context.AiContextCategory
import com.gitlab.plugin.chat.context.AiContextItem
import com.gitlab.plugin.chat.context.AiContextSearchQuery
import com.gitlab.plugin.lsp.messages.CodeSuggestionsTelemetryParams
import com.gitlab.plugin.lsp.params.*
import com.gitlab.plugin.lsp.webview.ThemeProvider
import org.eclipse.lsp4j.CompletionItem
import org.eclipse.lsp4j.CompletionList
import org.eclipse.lsp4j.jsonrpc.messages.Either
import org.eclipse.lsp4j.jsonrpc.services.JsonNotification
import org.eclipse.lsp4j.jsonrpc.services.JsonRequest
import org.eclipse.lsp4j.services.LanguageServer
import java.util.concurrent.CompletableFuture

@Suppress("TooManyFunctions")
interface GitLabLanguageServer : LanguageServer {
  @JsonNotification("$/gitlab/didChangeDocumentInActiveEditor")
  fun didChangeDocumentInActiveEditor(uri: String)

  @JsonNotification("$/gitlab/telemetry")
  fun telemetry(params: CodeSuggestionsTelemetryParams)

  @JsonRequest("textDocument/inlineCompletion")
  fun inlineCompletion(params: InlineCompletionParams): CompletableFuture<Either<List<CompletionItem>, CompletionList>>

  @JsonRequest("$/gitlab/ai-context/query")
  fun aiContextQuery(params: AiContextSearchQuery): CompletableFuture<List<AiContextItem>>

  @JsonRequest("$/gitlab/ai-context/add")
  fun aiContextAdd(item: AiContextItem): CompletableFuture<Boolean>

  @JsonRequest("$/gitlab/ai-context/retrieve")
  fun aiContextRetrieve(): CompletableFuture<List<AiContextItem>>

  @JsonRequest("$/gitlab/ai-context/remove")
  fun aiContextRemove(item: AiContextItem): CompletableFuture<Boolean>

  @JsonRequest("$/gitlab/ai-context/current-items")
  fun aiContextCurrentItems(): CompletableFuture<List<AiContextItem>>

  @JsonRequest("$/gitlab/ai-context/get-provider-categories")
  fun aiContextGetProviderCategories(): CompletableFuture<List<AiContextCategory>>

  @JsonRequest("$/gitlab/ai-context/clear")
  fun aiContextClear(): CompletableFuture<Boolean>

  @JsonRequest("$/gitlab/validateConfiguration")
  fun validateConfiguration(settings: Settings): CompletableFuture<List<FeatureStateParams>>

  @JsonRequest("$/gitlab/webview-metadata")
  fun getWebViewInfo(): CompletableFuture<List<WebViewInfo>>

  @JsonNotification("$/gitlab/theme/didChangeTheme")
  fun didChangeTheme(params: ThemeProvider.Theme)

  @JsonNotification("$/gitlab/plugin/notification")
  fun sendNewPrompt(newPromptParams: NewPromptParams)
}
