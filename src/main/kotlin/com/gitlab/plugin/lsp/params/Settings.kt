package com.gitlab.plugin.lsp.params

import com.gitlab.plugin.codesuggestions.telemetry.Event
import org.eclipse.lsp4j.WorkspaceFolder

data class Settings(
  val baseUrl: String? = null,
  val token: String? = null,
  val ignoreCertificateErrors: Boolean? = null,
  val projectPath: String? = null,
  val telemetry: Telemetry? = null,
  val logLevel: String? = null,
  val openTabsContext: Boolean? = null,
  val codeCompletion: CodeCompletion? = null,
  val httpAgentOptions: HttpAgentOptions? = null,
  val featureFlags: FeatureFlags? = null,
  val workspaceFolders: List<WorkspaceFolder>? = null,
  val duoChat: DuoChatFeature? = null,
)

data class Telemetry(
  val enabled: Boolean,
  val trackingUrl: String? = null,
  val actions: List<Map<String, String>> = Event.Type.entries.map { action -> mapOf("action" to action.action) }
)

fun com.gitlab.plugin.workspace.Telemetry.toDto(): Telemetry {
  return Telemetry(this.enabled, this.trackingUrl)
}

data class CodeCompletion(
  val additionalLanguages: Set<String>? = null,
  val disabledSupportedLanguages: Set<String>? = null,
  var enableSecretRedaction: Boolean? = true,
  val enabled: Boolean? = null
)

fun com.gitlab.plugin.lsp.settings.CodeCompletion.toDto(): CodeCompletion {
  return CodeCompletion(
    this.additionalLanguages,
    this.disabledSupportedLanguages.filterNot { additionalLanguages.contains(it) }.toSet(),
  )
}

data class HttpAgentOptions(
  var ca: String?,
  var cert: String?,
  var certKey: String?
)

fun com.gitlab.plugin.lsp.settings.HttpAgentOptions.toDto(): HttpAgentOptions {
  return HttpAgentOptions(this.ca, this.cert, this.certKey)
}

data class FeatureFlags(
  val streamCodeGenerations: Boolean,
  val codeSuggestionsClientDirectToGateway: Boolean = true
)

fun com.gitlab.plugin.lsp.settings.FeatureFlags.toDto(): FeatureFlags {
  return FeatureFlags(streamCodeGenerations)
}

data class DuoChatFeature(
  val enabled: Boolean? = null
)
