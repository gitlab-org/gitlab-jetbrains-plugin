package com.gitlab.plugin.lsp.webview

import com.gitlab.plugin.lsp.params.LSRequestResponseParams
import com.gitlab.plugin.lsp.params.NewPromptParams
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.util.io.await

@Service(Service.Level.PROJECT)
class LanguageServerDuoChatWebViewService(private val project: Project) {
  private val languageServerService by lazy { project.service<GitLabLanguageServerService>() }
  private var isReady = false
  private val awaitingMessages: MutableList<NewPromptParams> = mutableListOf()

  suspend fun getWebViewInfo(): String {
    val lspServer = languageServerService.lspServer ?: return ""
    val response = lspServer.getWebViewInfo().await()
    val duoChatWebViewInfo = response?.find { webViewInfo -> webViewInfo.id == LSRequestResponseParams.WEBVIEW_PLUGIN_ID }

    return duoChatWebViewInfo?.uris?.get(0).orEmpty()
  }

  fun sendThemeChangeNotification(theme: ThemeProvider.Theme) {
    val lspServer = languageServerService.lspServer ?: return

    lspServer.didChangeTheme(theme)
  }

  fun sendNewPromptLSNotification(newPromptParams: NewPromptParams) {
    when {
      isReady -> {
        val lspServer = languageServerService.lspServer ?: return
        lspServer.sendNewPrompt(newPromptParams)
      }
      else -> awaitingMessages.add(newPromptParams)
    }
  }

  fun markLSCommunicationAsReady() {
    isReady = true

    while (awaitingMessages.isNotEmpty()) {
      val lspServer = languageServerService.lspServer ?: return
      lspServer.sendNewPrompt(awaitingMessages.removeFirst())
    }
  }
}
