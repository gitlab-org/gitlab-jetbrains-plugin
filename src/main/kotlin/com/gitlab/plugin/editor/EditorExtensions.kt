package com.gitlab.plugin.editor

import com.gitlab.plugin.language.Language
import com.gitlab.plugin.lsp.settings.LanguageServerSettings
import com.intellij.openapi.application.ReadAction
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.fileTypes.FileType
import com.intellij.psi.PsiDocumentManager

fun Editor.getLanguage(): Language? {
  val project = project ?: return null

  val fileType = ReadAction.compute<FileType?, Throwable> {
    PsiDocumentManager.getInstance(project).getPsiFile(document)?.fileType
  } ?: return null

  return Language.getInstance(fileType)
}

fun Editor.isCodeSuggestionsEnabledForLanguage(): Boolean? {
  val language = getLanguage() ?: return null

  val codeCompletionState =
    project?.service<LanguageServerSettings>()?.state?.workspaceSettings?.codeCompletion ?: return null

  return if (language.isJetBrainsSupported) {
    codeCompletionState.disabledSupportedLanguages.none { language.matches(it) }
  } else {
    codeCompletionState.additionalLanguages.contains(language.id)
  }
}
