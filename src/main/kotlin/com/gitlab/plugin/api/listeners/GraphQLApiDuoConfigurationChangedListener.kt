package com.gitlab.plugin.api.listeners

import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.workspace.DuoConfigurationChangedListener
import com.gitlab.plugin.workspace.WorkspaceSettings
import com.intellij.openapi.components.service
import com.intellij.util.application

class GraphQLApiDuoConfigurationChangedListener : DuoConfigurationChangedListener {

  override fun onConfigurationChange(settings: WorkspaceSettings) {
    if (settings.token.isNotBlank()) {
      application.service<GraphQLApi>().reload()
    }
  }
}
