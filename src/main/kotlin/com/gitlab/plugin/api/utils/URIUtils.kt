package com.gitlab.plugin.api.utils

import java.net.URI

fun URI.getOrigin(): String {
  return "$scheme://${host}${if (port != -1) ":$port" else ""}"
}
