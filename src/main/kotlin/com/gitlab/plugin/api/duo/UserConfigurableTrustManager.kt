package com.gitlab.plugin.api.duo

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.lsp.services.CaCertificateService
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.util.application
import com.intellij.util.net.ssl.ConfirmingTrustManager
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.X509TrustManager

/**
 * A trust manager which can be configured by the user when additional certificate settings were provided.
 */
class UserConfigurableTrustManager(
  private val duoPersistentSettings: DuoPersistentSettings,
  private val trustManagerChain: TrustManagerChain
) : X509TrustManager {
  private val defaultTrustManagers: List<X509TrustManager> by lazy {
    trustManagerChain.getChain()
  }

  private val logger = logger<UserConfigurableTrustManager>()

  override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {
    checkTrusted(defaultTrustManagers) { trustManager ->
      trustManager.checkClientTrusted(chain, authType)
    }
  }

  override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {
    if (duoPersistentSettings.ignoreCertificateErrors) {
      logger.debug("Not checking server certificate is trusted due to user configuration.")
      return
    }

    checkTrusted(defaultTrustManagers) { trustManager ->
      when (trustManager) {
        is ConfirmingTrustManager -> trustManager.checkServerTrusted(
          chain,
          authType,
          ConfirmingTrustManager.CertificateConfirmationParameters.askConfirmation(
            true,
            GitLabBundle.message("settings.ui.gitlab.ignore-certificate-lengthy-description"),
            null
          )
        )
        else -> trustManager.checkServerTrusted(chain, authType)
      }
    }

    chain?.firstOrNull()?.run(::getVerifyingCertificate)?.let {
      application.service<CaCertificateService>().updateCaCertificate(it)
    }
  }

  override fun getAcceptedIssuers(): Array<X509Certificate> =
    defaultTrustManagers.flatMap { it.acceptedIssuers.toList() }.toTypedArray()

  private fun checkTrusted(
    trustManagers: List<X509TrustManager>,
    action: (X509TrustManager) -> Unit
  ) {
    trustManagers.forEach { trustManager ->
      try {
        action.invoke(trustManager)
        // one of the trust managers succeeded, so we're done
        return
      } catch (ignored: CertificateException) {
        // go to the next trust manager
        return@forEach
      }
    }
    // If we've exhausted all trust managers without success, throw an exception
    throw CertificateException(
      "Unable to establish trust for the provided certificate chain." +
        " Please check your SSL certificate settings."
    )
  }

  private fun getVerifyingCertificate(chainCertificate: X509Certificate): X509Certificate? {
    for (issuer in acceptedIssuers) {
      if (issuer.subjectX500Principal == chainCertificate.issuerX500Principal) {
        return issuer
      }
    }
    return null
  }
}
