package com.gitlab.plugin.codesuggestions.telemetry

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.codesuggestions.render.UpdatableInlineCompletionGrayElement
import com.gitlab.plugin.exceptions.ExceptionTracker
import com.intellij.codeInsight.inline.completion.InlineCompletionEventType
import com.intellij.codeInsight.inline.completion.elements.InlineCompletionSkipTextElement
import com.intellij.codeInsight.inline.completion.logs.InlineCompletionUsageTracker
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import org.junit.jupiter.api.assertDoesNotThrow

class TelemetryInlineCompletionEventListenerTest : DescribeSpec({
  val telemetryService: TelemetryService = mockk()
  val exceptionTracker: ExceptionTracker = mockk(relaxUnitFun = true)
  val listener = TelemetryInlineCompletionEventListener(telemetryService)

  beforeEach {
    mockApplication().apply {
      every { getService(ExceptionTracker::class.java) } returns exceptionTracker
    }
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("onShow") {
    val event: InlineCompletionEventType.Show = mockk()

    beforeEach {
      every { telemetryService.shown() } just runs

      every { event.element } returns mockk<UpdatableInlineCompletionGrayElement> {
        every { isPartiallyAccepted } returns false
      }
    }

    it("sends a shown telemetry event with the current context") {
      listener.onShow(event)

      verify(exactly = 1) {
        telemetryService.shown()
      }
    }

    it("does not sends a shown telemetry event for other elements") {
      every { event.element } returns mockk<InlineCompletionSkipTextElement>()

      listener.onShow(event)

      verify(exactly = 0) {
        telemetryService.shown()
      }
    }

    it("does not sends a shown telemetry event for partially accepted elements") {
      every { event.element } returns mockk<UpdatableInlineCompletionGrayElement> {
        every { isPartiallyAccepted } returns true
      }

      listener.onShow(event)

      verify(exactly = 0) {
        telemetryService.shown()
      }
    }

    it("should not re-throw exceptions") {
      val exception = RuntimeException("Test exception")
      every { telemetryService.shown() } throws exception

      assertDoesNotThrow { listener.onShow(event) }

      verify { exceptionTracker.handle(exception) }
    }
  }

  describe("onHide") {
    val event: InlineCompletionEventType.Hide = mockk()

    beforeEach {
      every { telemetryService.rejected() } just runs
    }

    context("when suggestion was displayed") {
      beforeEach {
        every { event.isCurrentlyDisplaying } returns true
      }

      context("and suggestion was not accepted") {
        beforeEach {
          every { event.finishType } returns InlineCompletionUsageTracker.ShownEvents.FinishType.ESCAPE_PRESSED
        }

        it("sends a rejected telemetry event with the current context") {
          listener.onHide(event)

          verify(exactly = 1) {
            telemetryService.rejected()
          }
        }
      }

      context("and suggestion was accepted") {
        beforeEach {
          every { event.finishType } returns InlineCompletionUsageTracker.ShownEvents.FinishType.SELECTED
        }

        it("does not send a rejected telemetry event") {
          listener.onHide(event)

          verify(exactly = 0) {
            telemetryService.rejected()
          }
        }
      }
    }

    context("when suggestion was not displayed") {
      beforeEach {
        every { event.isCurrentlyDisplaying } returns false
      }

      it("does not send a rejected telemetry event") {
        listener.onHide(event)

        verify(exactly = 0) {
          telemetryService.rejected()
        }
      }
    }

    it("should not re-throw exceptions") {
      every { event.isCurrentlyDisplaying } returns true
      every { event.finishType } returns InlineCompletionUsageTracker.ShownEvents.FinishType.ESCAPE_PRESSED

      val exception = RuntimeException("Test exception")
      every { telemetryService.rejected() } throws exception

      assertDoesNotThrow { listener.onHide(event) }

      verify { exceptionTracker.handle(exception) }
    }
  }

  describe("onInsert") {
    val event: InlineCompletionEventType.Insert = mockk()

    beforeEach {
      every { telemetryService.accepted() } just runs
    }

    it("sends an accepted telemetry event with the current context") {
      listener.onInsert(event)

      verify(exactly = 1) {
        telemetryService.accepted()
      }
    }

    it("should not re-throw exceptions") {
      val exception = RuntimeException("Test exception")
      every { telemetryService.accepted() } throws exception

      assertDoesNotThrow { listener.onInsert(event) }

      verify { exceptionTracker.handle(exception) }
    }
  }
})
