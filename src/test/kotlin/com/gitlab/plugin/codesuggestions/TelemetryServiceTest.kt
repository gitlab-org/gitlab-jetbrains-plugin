package com.gitlab.plugin.codesuggestions

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.codesuggestions.telemetry.Event
import com.gitlab.plugin.codesuggestions.telemetry.TelemetryService
import com.gitlab.plugin.codesuggestions.telemetry.destination.LanguageServerTelemetryDestination
import com.gitlab.plugin.lsp.settings.LanguageServerSettings
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import com.intellij.testFramework.registerOrReplaceServiceInstance
import com.intellij.util.application
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import junit.framework.TestCase
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class TelemetryServiceTest : BasePlatformTestCase() {
  private lateinit var telemetryService: TelemetryService
  private lateinit var languageServerTelemetryDestination: LanguageServerTelemetryDestination

  private val context = Event.Context(
    trackingId = "uuid",
    requestId = "request-id",
    modelEngine = "test-engine",
    modelName = "test-name",
    language = "test-lang"
  )

  @BeforeEach
  override fun setUp() {
    super.setUp()

    val duoPersistentSettings = mockk<DuoPersistentSettings>(relaxed = true)
    every { duoPersistentSettings.telemetryEnabled } returns true
    application.registerOrReplaceServiceInstance(
      DuoPersistentSettings::class.java,
      duoPersistentSettings,
      testRootDisposable
    )

    val project = mockk<Project>(relaxed = true)

    val languageServerSettings = mockk<LanguageServerSettings>(relaxed = true)
    every { project.service<LanguageServerSettings>() } returns languageServerSettings

    languageServerTelemetryDestination = mockk(relaxed = true)

    telemetryService = spyk(TelemetryService(project), recordPrivateCalls = true)
    every { telemetryService getProperty "destinations" } returns listOf(languageServerTelemetryDestination)
  }

  @AfterEach
  override fun tearDown() {
    disposeRootDisposable()
    super.tearDown()
  }

  @Test
  fun `data contains all event types`() {
    assert(true)
    val dataTestCases = Event.Type.entries
    TestCase.assertEquals(Event.Type.entries.size, dataTestCases.size)
  }

  @Test
  fun `new context generates valid UUID`() {
    val uuid = telemetryService.newContext().trackingId

    val uuidPattern = "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$".toRegex()
    TestCase.assertTrue(uuid.matches(uuidPattern))

    val secondUuid = telemetryService.newContext().trackingId
    TestCase.assertNotSame(uuid, secondUuid)
  }

  @Test
  fun `Broadcasts event to all destinations when telemetry is enabled`() {
    Event.Type.entries.forEach { eventType ->
      telemetryService.update(context)
      when (eventType) {
        Event.Type.ACCEPTED -> telemetryService.accepted()
        Event.Type.SHOWN -> telemetryService.shown()
        Event.Type.REJECTED -> telemetryService.rejected()
      }

      verify { languageServerTelemetryDestination.receive(Event(eventType, context)) }
    }
  }

  @Test
  fun `Update stores given context in current context`() {
    val requestedContext = context.copy(prefixLength = 5, suffixLength = 6)

    telemetryService.update(requestedContext)

    TestCase.assertEquals(requestedContext, telemetryService.currentContext)
  }
}
