package com.gitlab.plugin.codesuggestions.services

import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.codesuggestions.SuggestionContext
import com.gitlab.plugin.codesuggestions.SwitchInlineCompletionSuggestionMode
import com.gitlab.plugin.codesuggestions.render.UpdatableInlineCompletionGrayElement
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*

class SuggestionsTrackerTest : DescribeSpec({
  val context = mockk<SuggestionContext>()
  val element = mockk<UpdatableInlineCompletionGrayElement>(relaxed = true)

  lateinit var tracker: SuggestionsTracker

  beforeEach {
    tracker = SuggestionsTracker(context, element, streamId = null)

    val application = mockApplication()
    every { application.isDispatchThread } returns true
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("should be streaming only if it has a stream id") {
    val trackerWithoutStreamId = SuggestionsTracker(context, element, streamId = null)
    trackerWithoutStreamId.isStreaming shouldBe false

    val trackerWithStreamId = SuggestionsTracker(context, element, streamId = "123")
    trackerWithStreamId.isStreaming shouldBe true
  }

  it("should initially have an empty response") {
    tracker.response shouldBe null
  }

  it("should not update element if there is no response") {
    tracker.switch(SwitchInlineCompletionSuggestionMode.NEXT)

    verify(exactly = 0) { element.update(any()) }
  }

  it("should not update index if a suggestion has been partially accepted") {
    tracker.response = Completion.Response(
      choices = listOf(
        Completion.Response.Choice("return 1;"),
        Completion.Response.Choice("return 2;"),
      )
    )
    every { element.isPartiallyAccepted } returns true

    tracker.switch(SwitchInlineCompletionSuggestionMode.PREVIOUS)

    verify(exactly = 0) { element.update(any()) }
  }

  it("should not update index if there is only one response") {
    tracker.response = Completion.Response(
      choices = listOf(
        Completion.Response.Choice("return 1;")
      )
    )

    tracker.switch(SwitchInlineCompletionSuggestionMode.NEXT)
    verify(exactly = 0) { element.update(any()) }

    tracker.switch(SwitchInlineCompletionSuggestionMode.PREVIOUS)
    verify(exactly = 0) { element.update(any()) }
  }

  it("should go to the next suggestion") {
    tracker.response = Completion.Response(
      choices = listOf(
        Completion.Response.Choice("return 1;"),
        Completion.Response.Choice("return 2;"),
        Completion.Response.Choice("return 3;"),
      )
    )

    tracker.switch(SwitchInlineCompletionSuggestionMode.NEXT)
    verify(exactly = 1) { element.update("return 2;") }

    tracker.switch(SwitchInlineCompletionSuggestionMode.NEXT)
    verify(exactly = 1) { element.update("return 3;") }

    tracker.switch(SwitchInlineCompletionSuggestionMode.NEXT)
    verify(exactly = 1) { element.update("return 1;") }
  }

  it("should go to the previous suggestion") {
    tracker.response = Completion.Response(
      choices = listOf(
        Completion.Response.Choice("return 1;"),
        Completion.Response.Choice("return 2;"),
        Completion.Response.Choice("return 3;"),
      )
    )

    tracker.switch(SwitchInlineCompletionSuggestionMode.PREVIOUS)
    verify(exactly = 1) { element.update("return 3;") }

    tracker.switch(SwitchInlineCompletionSuggestionMode.PREVIOUS)
    verify(exactly = 1) { element.update("return 2;") }

    tracker.switch(SwitchInlineCompletionSuggestionMode.PREVIOUS)
    verify(exactly = 1) { element.update("return 1;") }
  }
})
