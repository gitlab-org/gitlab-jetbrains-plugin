package com.gitlab.plugin.util

import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.authentication.PatProvider
import com.gitlab.plugin.authentication.TokenProviderType
import com.gitlab.plugin.lsp.params.Settings
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.workspace.DuoConfigurationChangedListener
import com.gitlab.plugin.workspace.Telemetry
import com.gitlab.plugin.workspace.WorkspaceSettings
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import com.intellij.testFramework.registerOrReplaceServiceInstance
import com.intellij.util.application
import io.mockk.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class DuoConfigurationChangedListenerTest : BasePlatformTestCase() {
  @BeforeEach
  override fun setUp() {
    super.setUp()
  }

  @AfterEach
  override fun tearDown() {
    super.tearDown()
    clearAllMocks()
    unmockkAll()
  }

  @Test
  fun `TokenUpdatedDuoConfigurationListener receives new settings on configuration update event`() {
    val patProvider = mockk<PatProvider>(relaxed = true)
    val gitLabUserService = mockk<GitLabUserService>(relaxed = true)

    application.registerOrReplaceServiceInstance(PatProvider::class.java, patProvider, testRootDisposable)
    application.registerOrReplaceServiceInstance(GitLabUserService::class.java, gitLabUserService, testRootDisposable)

    val configurationChangeListener = application.messageBus
      .syncPublisher(DuoConfigurationChangedListener.DID_CHANGE_CONFIGURATION_TOPIC)

    val workspaceSettings = getWorkspaceSettings()

    configurationChangeListener.onConfigurationChange(workspaceSettings)

    coVerify(exactly = 1) { patProvider.cacheToken(workspaceSettings.token) }
    coVerify(exactly = 1) { gitLabUserService.invalidateAndFetchCurrentUser() }
  }

  @Test
  fun `GraphQLApiDuoConfigurationChangedListener receives new settings on configuration update event`() {
    val graphQlApi = mockk<GraphQLApi>(relaxed = true)

    application.registerOrReplaceServiceInstance(GraphQLApi::class.java, graphQlApi, testRootDisposable)

    val configurationChangeListener = application.messageBus
      .syncPublisher(DuoConfigurationChangedListener.DID_CHANGE_CONFIGURATION_TOPIC)

    val workspaceSettings = getWorkspaceSettings()

    configurationChangeListener.onConfigurationChange(workspaceSettings)

    verify(exactly = 1) { graphQlApi.reload() }
  }

  @Test
  fun `ConfigurationChangedLanguageServerListener receives new settings on configuration update event`() {
    val gitLabLanguageServerService = mockk<GitLabLanguageServerService>(relaxed = true)

    project.registerOrReplaceServiceInstance(
      GitLabLanguageServerService::class.java,
      gitLabLanguageServerService,
      testRootDisposable
    )

    val configurationChangeListener = application.messageBus
      .syncPublisher(DuoConfigurationChangedListener.DID_CHANGE_CONFIGURATION_TOPIC)

    val workspaceSettings = getWorkspaceSettings()

    configurationChangeListener.onConfigurationChange(workspaceSettings)

    verify(atLeast = 1) {
      gitLabLanguageServerService.lspServer?.workspaceService?.didChangeConfiguration(
        match {
          val settings = it.settings as Settings
          settings.token == workspaceSettings.token &&
            settings.ignoreCertificateErrors == workspaceSettings.ignoreCertificateErrors &&
            settings.telemetry?.enabled == workspaceSettings.telemetry.enabled &&
            settings.telemetry?.trackingUrl == workspaceSettings.telemetry.trackingUrl
        }
      )
    }
  }

  private fun getWorkspaceSettings(): WorkspaceSettings {
    return WorkspaceSettings(
      url = "https://gitlab.com",
      codeSuggestionsEnabled = true,
      duoChatEnabled = true,
      tokenProviderType = TokenProviderType.PAT,
      token = "default_token",
      telemetry = Telemetry(enabled = true),
      ignoreCertificateErrors = false
    )
  }
}
