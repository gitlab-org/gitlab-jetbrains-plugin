package com.gitlab.plugin.actions.status

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.editor.getLanguage
import com.gitlab.plugin.editor.isCodeSuggestionsEnabledForLanguage
import com.gitlab.plugin.language.Language
import com.gitlab.plugin.lsp.listeners.LanguageServerSettingsChangedListener
import com.gitlab.plugin.lsp.settings.CodeCompletion
import com.gitlab.plugin.lsp.settings.LanguageServerSettings
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.actionSystem.Presentation
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import io.mockk.*
import junit.framework.TestCase
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ToggleCodeSuggestionsPerLanguageTest : BasePlatformTestCase() {

  private lateinit var action: ToggleCodeSuggestionsPerLanguage
  private lateinit var event: AnActionEvent
  private lateinit var presentation: Presentation
  private lateinit var project: Project
  private lateinit var editor: Editor
  private lateinit var languageServerSettings: LanguageServerSettings
  private lateinit var codeCompletion: CodeCompletion

  @BeforeEach
  override fun setUp() {
    super.setUp()

    action = ToggleCodeSuggestionsPerLanguage()
    event = mockk(relaxed = true)
    presentation = Presentation()
    project = mockk()
    editor = mockk(relaxed = true)
    languageServerSettings = mockk()
    codeCompletion = CodeCompletion()

    mockkObject(DuoPersistentSettings)
    every { DuoPersistentSettings.getInstance().codeSuggestionsEnabled } returns true

    every { event.project } returns project
    every { event.presentation } returns presentation
    every { event.getData(CommonDataKeys.EDITOR) } returns editor
    every { editor.project } returns project
    every { project.save() } just runs

    every {
      project
        .messageBus
        .syncPublisher(LanguageServerSettingsChangedListener.LANGUAGE_SERVER_SETTINGS_CHANGED_TOPIC)
        .onLanguageServerSettingsChanged()
    } just runs

    every { project.service<LanguageServerSettings>() } returns languageServerSettings
    every { languageServerSettings.state.workspaceSettings.codeCompletion } returns codeCompletion

    mockkStatic("com.gitlab.plugin.editor.EditorExtensionsKt")
  }

  @AfterEach
  override fun tearDown() {
    unmockkAll()
    super.tearDown()
  }

  @Test
  fun `test update for supported language`() {
    val language = Language(
      id = "kt",
      name = "Kotlin",
      fileExtensions = listOf("kt", "kts"),
      isJetBrainsSupported = true
    )

    every { editor.getLanguage() } returns language
    every { editor.isCodeSuggestionsEnabledForLanguage() } returns true

    action.update(event)

    assertEquals("Disable Code Suggestions for Kotlin", event.presentation.text)
    assertTrue(event.presentation.isEnabledAndVisible)
  }

  @Test
  fun `test update for unsupported language`() {
    val language = Language(
      id = "unsupported",
      name = "Unsupported",
      fileExtensions = listOf("unsupported")
    )

    every { editor.getLanguage() } returns language
    every { editor.isCodeSuggestionsEnabledForLanguage() } returns false

    action.update(event)

    assertEquals("Enable Code Suggestions for Unsupported", event.presentation.text)
    assertTrue(event.presentation.isEnabledAndVisible)
  }

  @Test
  fun `test actionPerformed for supported language`() {
    val language = Language(
      id = "java",
      name = "Java",
      fileExtensions = listOf("java"),
      isJetBrainsSupported = true
    )

    every { editor.getLanguage() } returns language

    action.actionPerformed(event)

    TestCase.assertTrue(codeCompletion.disabledSupportedLanguages.contains("java"))
  }

  @Test
  fun `test actionPerformed for unsupported language`() {
    val language = Language(
      id = "txt",
      name = "Plaintext",
      fileExtensions = listOf("txt")
    )

    every { editor.getLanguage() } returns language

    action.actionPerformed(event)

    TestCase.assertTrue(codeCompletion.additionalLanguages.contains("txt"))
  }

  @Test
  fun `test update when code suggestions are disabled`() {
    every { DuoPersistentSettings.getInstance().codeSuggestionsEnabled } returns false

    action.update(event)

    assertFalse(event.presentation.isEnabledAndVisible)
  }

  @Test
  fun `test update when editor is null`() {
    every { event.getData(CommonDataKeys.EDITOR) } returns null

    action.update(event)

    assertFalse(event.presentation.isEnabledAndVisible)
  }

  @Test
  fun `test update when getLanguage returns null`() {
    every { editor.getLanguage() } returns null

    action.update(event)

    assertFalse(event.presentation.isEnabledAndVisible)
  }

  @Test
  fun `test update when isCodeSuggestionsEnabledForLanguage returns null`() {
    val language = Language("kt", "Kotlin", listOf("kt", "kts"))
    every { editor.getLanguage() } returns language
    every { editor.isCodeSuggestionsEnabledForLanguage() } returns null

    action.update(event)

    assertFalse(event.presentation.isEnabledAndVisible)
  }
}
