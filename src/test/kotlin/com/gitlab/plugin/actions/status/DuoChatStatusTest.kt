package com.gitlab.plugin.actions.status

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.lsp.params.FeatureStateCheckParams
import com.gitlab.plugin.services.DuoChatStateService
import com.gitlab.plugin.ui.GitLabIcons
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.shouldBe
import io.mockk.*

class DuoChatStatusTest : DescribeSpec({
  val project = mockk<Project>()
  val duoChatStateService = mockk<DuoChatStateService>(relaxUnitFun = true)

  lateinit var status: DuoChatStatus
  lateinit var event: AnActionEvent

  beforeSpec {
    mockApplication()
    mockkObject(DuoPersistentSettings)
  }

  beforeEach {
    stubRunReadAction()
    event = mockAnActionEvent()
    status = DuoChatStatus()

    every { event.project } returns project
    every { project.service<DuoChatStateService>() } returns duoChatStateService
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  describe("update") {
    describe("with chat disabled in settings") {
      beforeEach {
        every { duoChatStateService.getEngagedCheck() } returns FeatureStateCheckParams("check-id")
        every { duoChatStateService.duoChatEnabled } returns false
      }

      it("displays as disabled state to user") {
        status.update(event)

        event.presentation.icon.shouldBe(GitLabIcons.Actions.DuoChatDisabled)
        event.presentation.isEnabled.shouldBeFalse()
        event.presentation.text.shouldBeEqual("Duo Chat: Disabled (check-id)")
      }
    }

    describe("with chat enabled in settings") {
      beforeEach {
        every { duoChatStateService.getEngagedCheck() } returns null
        every { duoChatStateService.duoChatEnabled } returns true
      }

      it("displays as enabled state to user") {
        status.update(event)

        event.presentation.icon.shouldBe(GitLabIcons.Actions.DuoChatEnabled)
        event.presentation.isEnabled.shouldBeFalse()
        event.presentation.text.shouldBeEqual("Duo Chat: Enabled")
      }
    }
  }
})
