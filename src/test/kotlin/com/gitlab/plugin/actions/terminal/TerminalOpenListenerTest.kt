package com.gitlab.plugin.actions.terminal

import com.intellij.openapi.Disposable
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Disposer
import com.intellij.openapi.wm.ToolWindowManager
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import org.jetbrains.plugins.terminal.ShellTerminalWidget
import org.jetbrains.plugins.terminal.TerminalToolWindowManager

class TerminalOpenListenerTest : DescribeSpec({
  describe("TerminalOpenListener") {
    val mockProject = mockk<Project>()
    val mockToolWindowManager = mockk<ToolWindowManager>()
    val mockTerminalToolWindowManager = mockk<TerminalToolWindowManager>()
    val mockDisposable = mockk<Disposable>()
    val mockTerminalActionsService = mockk<TerminalActionsService>()

    beforeEach {
      mockkStatic(Disposer::class)
      mockkStatic(TerminalToolWindowManager::class)
      mockkStatic(ShellTerminalWidget::class)
      every { Disposer.newDisposable() } returns mockDisposable
      every { TerminalToolWindowManager.getInstance(any()) } returns mockTerminalToolWindowManager
      every { mockProject.service<TerminalActionsService>() } returns mockTerminalActionsService
      every { mockTerminalActionsService.areTerminalActionsEnabled() } returns true
    }

    afterEach {
      clearAllMocks()
      unmockkAll()
    }

    describe("toolWindowsRegistered") {
      it("adds Duo action to terminal context menu when Terminal is registered") {
        val ids = mutableListOf("Terminal")
        val listener = TerminalOpenListener(mockProject)

        every { mockTerminalToolWindowManager.addNewTerminalSetupHandler(any(), any()) } just Runs

        listener.toolWindowsRegistered(ids, mockToolWindowManager)

        verify(exactly = 1) { mockTerminalToolWindowManager.addNewTerminalSetupHandler(any(), mockDisposable) }
      }

      it("does not add Duo action when Terminal is not registered") {
        val ids = mutableListOf("SomeOtherWindow")
        val listener = TerminalOpenListener(mockProject)

        listener.toolWindowsRegistered(ids, mockToolWindowManager)

        verify(exactly = 0) { mockTerminalToolWindowManager.addNewTerminalSetupHandler(any(), any()) }
      }
    }
  }
})
