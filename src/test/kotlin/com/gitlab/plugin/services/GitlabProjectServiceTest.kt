package com.gitlab.plugin.services

import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import git4idea.repo.GitRemote
import git4idea.repo.GitRepository
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlin.collections.ArrayList

class GitlabProjectServiceTest : DescribeSpec({
  beforeContainer {
    mockkConstructor(GitRepositoryService::class)
    mockkConstructor(Project::class)
  }

  afterContainer {
    unmockkAll()
  }

  describe("GitlabProjectService") {
    val mockGitRepositoryService = mockk<GitRepositoryService>()
    val mockGitRepository = mockk<GitRepository>()
    val project = mockk<Project>()

    beforeEach {
      clearMocks(mockGitRepositoryService)
      every { project.service<GitRepositoryService>() } returns mockGitRepositoryService
      coEvery { mockGitRepositoryService.fetchRepository() } returns mockGitRepository
    }

    describe("getCurrentProjectPath") {
      it("returns the path for the current project - HTTP format") {
        val gitlabProjectService = GitLabProjectService(project)

        val remoteUrls = ArrayList<String>()
        remoteUrls.add("https://gitlab.com/test/test-project.git")

        val gitRemote =
          GitRemote("testRemote", remoteUrls, ArrayList<String>(), ArrayList<String>(), ArrayList<String>())
        val remotes = ArrayList<GitRemote>()
        remotes.add(gitRemote)

        coEvery { mockGitRepository.remotes } returns remotes

        val response = gitlabProjectService.getCurrentProjectPath()
        response.shouldNotBeNull()
        response.shouldBe("test/test-project")
      }
      it("returns null when the repository is null") {
        coEvery { mockGitRepositoryService.fetchRepository() } returns null
        val gitlabProjectService = GitLabProjectService(project)

        val response = gitlabProjectService.getCurrentProjectPath()
        response.shouldBeNull()
      }

      it("returns the path for the current project - SSH format") {
        val gitlabProjectService = GitLabProjectService(project)
        val remoteUrls = ArrayList<String>()
        remoteUrls.add("git@gitlab.com:gitlab-org/editor-extensions/gitlab-jetbrains-plugin.git")

        val gitRemote =
          GitRemote("testRemote", remoteUrls, ArrayList<String>(), ArrayList<String>(), ArrayList<String>())
        val remotes = ArrayList<GitRemote>()
        remotes.add(gitRemote)

        coEvery { mockGitRepository.remotes } returns remotes

        val response = gitlabProjectService.getCurrentProjectPath()
        response.shouldNotBeNull()
        response.shouldBe("gitlab-org/editor-extensions/gitlab-jetbrains-plugin")
      }

      it("returns the path for the current project - SSH format with a port") {
        val gitlabProjectService = GitLabProjectService(project)
        val remoteUrls = ArrayList<String>()
        remoteUrls.add("ssh://username:password@host:2222/test/test-path.git")

        val gitRemote =
          GitRemote("testRemote", remoteUrls, ArrayList<String>(), ArrayList<String>(), ArrayList<String>())
        val remotes = ArrayList<GitRemote>()
        remotes.add(gitRemote)

        coEvery { mockGitRepository.remotes } returns remotes

        val response = gitlabProjectService.getCurrentProjectPath()
        response.shouldNotBeNull()
        response.shouldBe("test/test-path")
      }

      it("returns the path for the current project - SSH without a port") {
        val gitlabProjectService = GitLabProjectService(project)
        val remoteUrls = ArrayList<String>()
        remoteUrls.add("ssh://username:password@host/test/test-path.git")

        val gitRemote =
          GitRemote("testRemote", remoteUrls, ArrayList<String>(), ArrayList<String>(), ArrayList<String>())
        val remotes = ArrayList<GitRemote>()
        remotes.add(gitRemote)

        coEvery { mockGitRepository.remotes } returns remotes

        val response = gitlabProjectService.getCurrentProjectPath()
        response.shouldNotBeNull()
        response.shouldBe("test/test-path")
      }
    }
  }
})
