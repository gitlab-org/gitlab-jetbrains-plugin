package com.gitlab.plugin.ui

import com.gitlab.plugin.api.mockApplicationInfo
import com.gitlab.plugin.api.mockPlugin
import com.gitlab.plugin.util.GitLabUtil
import com.gitlab.plugin.util.launchUrl
import com.intellij.openapi.diagnostic.IdeaLoggingEvent
import com.intellij.openapi.diagnostic.SubmittedReportInfo
import com.intellij.openapi.util.SystemInfo
import com.intellij.util.Consumer
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.ints.shouldBeLessThan
import io.kotest.matchers.string.shouldContain
import io.ktor.http.*
import io.mockk.*
import java.awt.Component

class GitLabIssueErrorReporterTest : DescribeSpec({
  val errorReporter = GitLabIssueErrorReporter()

  describe("submit") {
    mockkObject(GitLabUtil)

    val event: IdeaLoggingEvent = mockk()
    val parentComponent: Component = mockk()
    val consumer: Consumer<SubmittedReportInfo> = mockk()

    val additionalInfo = "More details"
    val stackTrace = "This is a stack trace"
    val pluginVersion = "0.5.1"
    val applicationName = "Full application name"
    val applicationBuild = "Build string"

    beforeEach {
      mockApplicationInfo(applicationName = applicationName, buildNumber = applicationBuild)
      mockPlugin(version = pluginVersion)

      mockkStatic(::launchUrl)
      every { launchUrl(any()) } just runs

      every { event.throwableText } returns stackTrace
      every { consumer.consume(any()) } just runs
    }

    afterEach { clearAllMocks() }
    afterSpec { unmockkAll() }

    it("opens a browser with the given URL") {
      errorReporter.submit(arrayOf(event), additionalInfo, parentComponent, consumer)

      verify(exactly = 1) {
        launchUrl(any())
      }
    }

    describe("issue URL") {
      it("has the correct path and params") {
        val javaVersion = System.getProperty("java.version", "unknown")
        val javaVmName = System.getProperty("java.vm.name", "unknown")
        val javaVendor = System.getProperty("java.vendor", "unknown")
        val osNameAndVersion = SystemInfo.getOsNameAndVersion()
        val osArch = SystemInfo.OS_ARCH

        val urlSlot = slot<String>()
        every { launchUrl(capture(urlSlot)) } just runs

        errorReporter.submit(arrayOf(event), additionalInfo, parentComponent, consumer)

        val expectedDescription = """
          ## Debug information

          <details><summary>Stack trace</summary>

          ```
          $stackTrace
          ```

          </details>

          ### Environment

          - Plugin version: $pluginVersion
          - IDE: $applicationName; Build: $applicationBuild
          - JDK: $javaVersion; VM: $javaVmName; Vendor: $javaVendor
          - OS: $osNameAndVersion; Arch: $osArch

          ### Additional information

          $additionalInfo
        """.trimIndent()

        urlSlot.captured.decodeURLPart().let {
          it shouldContain "gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/new"
          it shouldContain "issuable_template=Bug"
          it shouldContain "issue[description]=$expectedDescription"
        }
      }
    }

    context("when the issue URL is over header character limit") {
      val longStackTrace =
        "at com.gitlab.plugin.ui.GitLabIssueErrorReporterTest\$1\$1\$7\$2.invokeSuspend(GitLabIssueErrorReporterTest.kt:88)".let {
          it.repeat(HEADER_CHARACTER_LIMIT / it.length + 1)
        }

      beforeEach {
        every { event.throwableText } returns longStackTrace
      }

      it("trims the throwableText") {
        val urlSlot = slot<String>()
        every { launchUrl(capture(urlSlot)) } just runs

        errorReporter.submit(arrayOf(event), additionalInfo + "a".repeat(5000), parentComponent, consumer)

        urlSlot.captured.length shouldBeLessThan HEADER_CHARACTER_LIMIT
      }
    }
  }
})
