package com.gitlab.plugin.telemetry.tracked.actions

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.mockApplicationInfo
import com.gitlab.plugin.api.mockPlugin
import com.gitlab.plugin.services.GitLabApplicationService
import com.gitlab.plugin.telemetry.StandardContext
import com.snowplowanalytics.snowplow.tracker.Tracker
import com.snowplowanalytics.snowplow.tracker.events.Structured
import com.snowplowanalytics.snowplow.tracker.payload.SelfDescribingJson
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.maps.shouldContainAll
import io.mockk.*

class TrackQuickChatSlashCommandKtTest : DescribeSpec({
  val tracker: Tracker = mockk()

  mockkObject(StandardContext, GitLabApplicationService, GitLabBundle)
  mockApplicationInfo()
  mockPlugin()

  beforeEach {
    every { StandardContext.build(any()) } returns SelfDescribingJson(
      "standard_context",
      mapOf("extra" to "extra-val")
    )

    every { GitLabApplicationService.getInstance().snowplowTracker } returns tracker
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  listOf(
    ("/refactor blah" to "/refactor"),
    ("/explain" to "/explain"),
    ("/tests" to "/tests"),
    ("/fix make the tests pass" to "/fix"),
    ("tell me a joke" to "general_message")
  ).forEach { (message, expected) ->
    it("should correctly extract the command of the message") {
      val event = slot<Structured>()
      every { tracker.track(capture(event)) } returns emptyList()

      trackQuickChatSlashCommand(message)

      event.captured.payload.map shouldContainAll mapOf(
        "se_ca" to "gitlab_quick_chat",
        "se_ac" to "message_sent",
        "se_la" to expected,
      )
    }
  }

  it("should ignore messages that do not produce responses") {
    trackQuickChatSlashCommand("/reset")
    trackQuickChatSlashCommand("/clear")
    trackQuickChatSlashCommand("/clean")

    verify(exactly = 0) { tracker.track(any()) }
  }
})
