package com.gitlab.plugin.telemetry.tracked.actions

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.api.mockApplicationInfo
import com.gitlab.plugin.api.mockPlugin
import com.gitlab.plugin.authentication.TokenProviderType
import com.gitlab.plugin.services.GitLabApplicationService
import com.gitlab.plugin.telemetry.StandardContext
import com.intellij.openapi.components.service
import com.intellij.util.application
import com.snowplowanalytics.snowplow.tracker.Tracker
import com.snowplowanalytics.snowplow.tracker.events.Structured
import com.snowplowanalytics.snowplow.tracker.payload.SelfDescribingJson
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.maps.shouldContainAll
import io.mockk.*

class TrackAuthenticationMethodKtTest : DescribeSpec({
  val tracker: Tracker = mockk()

  mockkObject(StandardContext, GitLabApplicationService, GitLabBundle)
  mockApplicationInfo()
  mockPlugin()
  mockApplication()

  beforeEach {
    every { StandardContext.build(any()) } returns SelfDescribingJson(
      "standard_context",
      mapOf("extra" to "extra-val")
    )

    every { application.service<GitLabApplicationService>().snowplowTracker } returns tracker
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  it("tracks PAT authentication type") {
    val event = slot<Structured>()
    every { tracker.track(capture(event)) } returns emptyList()

    trackAuthenticationType(TokenProviderType.PAT)

    event.captured.payload.map shouldContainAll mapOf(
      "se_ca" to "gitlab_authentication",
      "se_ac" to "PAT",
      "se_la" to "selected_authentication_type",
    )
  }

  it("tracks OAuth authentication type") {
    val event = slot<Structured>()
    every { tracker.track(capture(event)) } returns emptyList()

    trackAuthenticationType(TokenProviderType.OAUTH)

    event.captured.payload.map shouldContainAll mapOf(
      "se_ca" to "gitlab_authentication",
      "se_ac" to "OAUTH",
      "se_la" to "selected_authentication_type",
    )
  }

  it("tracks 1Password authentication type") {
    val event = slot<Structured>()
    every { tracker.track(capture(event)) } returns emptyList()

    trackAuthenticationType(TokenProviderType.ONE_PASSWORD)

    event.captured.payload.map shouldContainAll mapOf(
      "se_ca" to "gitlab_authentication",
      "se_ac" to "ONE_PASSWORD",
      "se_la" to "selected_authentication_type",
    )
  }
})
