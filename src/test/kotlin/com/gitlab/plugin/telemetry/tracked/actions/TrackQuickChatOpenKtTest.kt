package com.gitlab.plugin.telemetry.tracked.actions

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.mockApplicationInfo
import com.gitlab.plugin.api.mockPlugin
import com.gitlab.plugin.chat.quickchat.QuickChatOpenMethod
import com.gitlab.plugin.services.GitLabApplicationService
import com.gitlab.plugin.telemetry.StandardContext
import com.snowplowanalytics.snowplow.tracker.Tracker
import com.snowplowanalytics.snowplow.tracker.events.Structured
import com.snowplowanalytics.snowplow.tracker.payload.SelfDescribingJson
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.maps.shouldContainAll
import io.mockk.*

class TrackQuickChatOpenKtTest : DescribeSpec({
  val tracker: Tracker = mockk()

  mockkObject(StandardContext, GitLabApplicationService, GitLabBundle)
  mockApplicationInfo()
  mockPlugin()

  beforeEach {
    every { StandardContext.build(any()) } returns SelfDescribingJson(
      "standard_context",
      mapOf("extra" to "extra-val")
    )

    every { GitLabApplicationService.getInstance().snowplowTracker } returns tracker
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  it("should track quick chat open by shortcut") {
    val event = slot<Structured>()
    every { tracker.track(capture(event)) } returns emptyList()

    trackQuickChatOpen(QuickChatOpenMethod.SHORTCUT)

    event.captured.payload.map shouldContainAll mapOf(
      "se_ca" to "gitlab_quick_chat",
      "se_ac" to "shortcut",
      "se_la" to "open_quick_chat",
    )
  }

  it("should track quick chat open by button click") {
    val event = slot<Structured>()
    every { tracker.track(capture(event)) } returns emptyList()

    trackQuickChatOpen(QuickChatOpenMethod.CLICK_BUTTON)

    event.captured.payload.map shouldContainAll mapOf(
      "se_ca" to "gitlab_quick_chat",
      "se_ac" to "click_button",
      "se_la" to "open_quick_chat",
    )
  }
})
