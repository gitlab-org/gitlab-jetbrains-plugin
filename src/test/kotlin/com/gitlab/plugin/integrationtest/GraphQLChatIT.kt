package com.gitlab.plugin.integrationtest

import app.cash.turbine.test
import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.api.Optional
import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.api.mockDefaultTrustManagerChain
import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.authentication.GitLabTokenProviderManager
import com.gitlab.plugin.exceptions.ExceptionTracker
import com.gitlab.plugin.graphql.ChatMutation_17_4_and_earlierMutation
import com.gitlab.plugin.graphql.ChatMutation_17_5_and_laterMutation
import com.gitlab.plugin.graphql.ChatSubscription
import com.gitlab.plugin.graphql.ChatWithAdditionalContextSubscription
import com.gitlab.plugin.graphql.scalars.AiModelID
import com.gitlab.plugin.graphql.scalars.UserID
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler
import com.intellij.openapi.components.service
import com.intellij.util.asSafely
import com.intellij.util.net.HttpConfigurable
import io.kotest.common.runBlocking
import io.kotest.core.annotation.EnabledIf
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldNotBeEmpty
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.string.shouldNotBeEmpty
import io.mockk.*
import java.util.*
import kotlin.test.assertNotNull
import kotlin.time.Duration.Companion.seconds

@EnabledIf(IntegrationTestEnvironment.Configured::class)
class GraphQLChatIT : DescribeSpec({
  lateinit var apolloClient: ApolloClient

  fun buildGraphQLApi(
    env: IntegrationTestEnvironment = IntegrationTestEnvironment.forSpec(GraphQLChatIT::class),
    ignoreCertificateErrors: Boolean = false,
    exceptionHandler: CreateNotificationExceptionHandler = mockk<CreateNotificationExceptionHandler>()
  ): GraphQLApi {
    mockDefaultTrustManagerChain()
    val application = mockApplication()

    every { DuoPersistentSettings.getInstance().url } returns env.gitlabHost
    every { application.service<DuoPersistentSettings>() } returns DuoPersistentSettings.getInstance().also {
      every { it.ignoreCertificateErrors } returns ignoreCertificateErrors
      every { it.integrate1PasswordCLI } returns false
    }

    assertNotNull(env.personalAccessToken, "personalAccessToken must not be null")
    val tokenProviderManager = mockk<GitLabTokenProviderManager>()
    every { application.service<GitLabTokenProviderManager>() } returns tokenProviderManager
    every { tokenProviderManager.getToken() } returns env.personalAccessToken.toString()

    val apolloClientFactory = ApolloClientFactory()
    every { application.service<ApolloClientFactory>() } returns apolloClientFactory
    every { application.service<ExceptionTracker>() } returns mockk<ExceptionTracker>(relaxUnitFun = true)

    apolloClient = apolloClientFactory.create()
    every { exceptionHandler.handleException(any()) } returns Unit

    return GraphQLApi.create(apolloClient, exceptionHandler)
  }

  beforeSpec {
    mockDuoContextServicePersistentSettings()
  }

  beforeEach {
    mockkStatic(HttpConfigurable::class)
    every { HttpConfigurable.getInstance() } returns mockk<HttpConfigurable>()
  }

  afterSpec {
    unmockkAll()
  }

  describe("[behind proxy]") {
    val proxiedGitLab = IntegrationTestEnvironment(
      personalAccessToken = System.getenv("TEST_ACCESS_TOKEN"),
      gitlabHost = System.getenv("TEST_GITLAB_PROXY_HOST") ?: "https://localhost:8443",
      kclass = GraphQLChatIT::class,
    )

    beforeEach {
      mockDuoContextServicePersistentSettings()
    }

    afterEach {
      clearAllMocks()
    }

    afterSpec {
      unmockkAll()
    }

    it("returns the current user if ignoring certificate errors").config(enabledIf = proxiedGitLab.running()) {
      val subject = buildGraphQLApi(env = proxiedGitLab, ignoreCertificateErrors = true)

      runBlocking {
        assertNotNull(subject.getCurrentUser())
      }
    }

    it("throws an error that is handled by the exceptionHandler when not ignoring certificate errors")
      .config(enabledIf = proxiedGitLab.running()) {
        val exceptionHandler = mockk<CreateNotificationExceptionHandler>()
        val subject = buildGraphQLApi(
          env = proxiedGitLab,
          ignoreCertificateErrors = false,
          exceptionHandler
        )

        runBlocking {
          val actual = subject.getCurrentUser()
          actual.shouldBeNull()
          verify(exactly = 1) { exceptionHandler.handleException(any()) }
        }
      }
  }

  describe("aiCompletionResponses") {
    lateinit var graphqlApi: GraphQLApi

    beforeEach {
      graphqlApi = buildGraphQLApi(ignoreCertificateErrors = false)
    }

    it("receives AI messages from the subscription for GitLab version 17.4 and earlier") {
      val currentUser = runBlocking { assertNotNull(graphqlApi.getCurrentUser(), "no current user found") }
      val sid = UUID.randomUUID().toString()
      val sendChatResponse = runBlocking {
        val mutation = ChatMutation_17_4_and_earlierMutation(
          question = "Hi Duo from local testing. What can I ask?",
          resourceId = Optional.present(AiModelID(currentUser.id)),
          clientSubscriptionId = sid,
        )
        apolloClient.mutation(mutation)
          .execute()
          .dataAssertNoErrors
          .aiAction
      }
      val subscriptionFlow = graphqlApi.chatSubscription(sid, false, UserID(currentUser.id))
      sendChatResponse.shouldNotBeNull()
      sendChatResponse.errors.shouldBeEmpty()

      val aiCompletionResponses = mutableListOf<ChatSubscription.AiCompletionResponse>()
      subscriptionFlow?.test(60.seconds) {
        // First item seems to always be null
        skipItems(1)

        while (true) {
          val response = awaitItem().dataAssertNoErrors.asSafely<ChatSubscription.Data>()?.aiCompletionResponse
          response.shouldNotBeNull()
          response.content.shouldNotBeEmpty()

          aiCompletionResponses.add(response)

          // Final result should have chunkId: null so we know we're done here
          if (response.chunkId == null) break
        }

        // There may still be chunks coming out of order
        cancelAndIgnoreRemainingEvents()
      }

      aiCompletionResponses.shouldNotBeEmpty()
    }

    it("receives AI messages from the subscription for GitLab version 17.5 and later") {
      val currentUser = runBlocking { assertNotNull(graphqlApi.getCurrentUser(), "no current user found") }
      val sid = UUID.randomUUID().toString()
      val sendChatResponse = runBlocking {
        val mutation = ChatMutation_17_5_and_laterMutation(
          question = "Hi Duo from local testing. What can I ask?",
          resourceId = Optional.present(AiModelID(currentUser.id)),
          clientSubscriptionId = sid,
          currentFileContext = Optional.absent(),
          additionalContext = Optional.absent(),
          platformOrigin = "jetbrains_ide"
        )
        apolloClient.mutation(mutation)
          .execute()
          .dataAssertNoErrors
          .aiAction
      }
      val subscriptionFlow = graphqlApi.chatSubscription(sid, true, UserID(currentUser.id))
      sendChatResponse.shouldNotBeNull()
      sendChatResponse.errors.shouldBeEmpty()

      val responses = mutableListOf<ChatWithAdditionalContextSubscription.AiCompletionResponse>()
      subscriptionFlow?.test(60.seconds) {
        // First item seems to always be null
        skipItems(1)

        while (true) {
          val response = awaitItem()
            .dataAssertNoErrors
            .asSafely<ChatWithAdditionalContextSubscription.Data>()
            ?.aiCompletionResponse

          response.shouldNotBeNull()
          response.content.shouldNotBeEmpty()

          responses.add(response)

          // Final result should have chunkId: null so we know we're done here
          if (response.chunkId == null) break
        }

        // There may still be chunks coming out of order
        cancelAndIgnoreRemainingEvents()
      }

      responses.shouldNotBeEmpty()
    }
  }
})
