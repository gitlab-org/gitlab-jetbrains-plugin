package com.gitlab.plugin.api.utils

import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import java.net.URI

class URIUtilsKtTest : DescribeSpec({
  describe("getOrigin") {
    it("returns the correct origin for a base url") {
      val baseUrl = "https://example.com"

      URI(baseUrl).getOrigin() shouldBe "https://example.com"
    }

    it("returns the correct origin for a base url with a port") {
      val baseUrl = "https://example.com:8443"

      URI(baseUrl).getOrigin() shouldBe "https://example.com:8443"
    }

    it("returns the correct origin without path when the base url has a relative path") {
      val baseUrl = "https://example.com/gitlab"

      URI(baseUrl).getOrigin() shouldBe "https://example.com"
    }

    it("returns the correct origin for a base url with port and path") {
      val baseUrl = "https://example.com:8443/gitlab"

      URI(baseUrl).getOrigin() shouldBe "https://example.com:8443"
    }
  }
})
