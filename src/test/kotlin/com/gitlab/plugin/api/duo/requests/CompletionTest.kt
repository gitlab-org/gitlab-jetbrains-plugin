package com.gitlab.plugin.api.duo.requests

import com.google.gson.JsonPrimitive
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import org.eclipse.lsp4j.Command

class CompletionTest : DescribeSpec({
  describe("Response") {
    val model = Completion.Response.Model()

    describe("hasSuggestions") {
      context("when choices is an empty list") {
        it("returns false") {
          Completion.Response(emptyList(), model).hasSuggestions() shouldBe false
        }
      }

      context("when all choices are empty strings") {
        val choices = listOf(Completion.Response.Choice(""), Completion.Response.Choice(" "))

        it("returns false") {
          Completion.Response(choices, model).hasSuggestions() shouldBe false
        }
      }

      describe("when choices contains at least one non-empty string") {
        val choices = listOf(Completion.Response.Choice("A code suggestion"))

        it("returns true") {
          Completion.Response(choices, model).hasSuggestions() shouldBe true
        }
      }

      describe("when choices contains at least one streaming suggestions") {
        val choices = listOf(
          Completion.Response.Choice(
            text = "",
            command = Command().apply {
              command = START_STREAMING_COMMAND
              arguments = listOf(JsonPrimitive("stream-id"), JsonPrimitive("unique-tracking-id"))
            }
          )
        )

        it("returns true") {
          Completion.Response(choices, model).hasSuggestions() shouldBe true
        }
      }
    }

    describe("firstSuggestion") {
      val choices = listOf(Completion.Response.Choice("A code suggestion"))

      it("returns the text of the first choice") {
        Completion.Response(choices, model).firstSuggestion shouldBe "A code suggestion"
      }
    }

    it("should get streaming suggestion") {
      val choices = listOf(
        Completion.Response.Choice(
          text = "",
          command = Command().apply {
            command = START_STREAMING_COMMAND
            arguments = listOf(JsonPrimitive("stream-id"), JsonPrimitive("unique-tracking-id"))
          }
        ),
        Completion.Response.Choice("A non-streaming code suggestion")
      )

      val response = Completion.Response(choices, model)

      response.getStreamingSuggestion()?.streamId shouldBe "stream-id"
    }
  }
})
