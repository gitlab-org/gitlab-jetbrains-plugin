package com.gitlab.plugin.git

import com.gitlab.plugin.services.DuoEnabledForGitLabProjectService
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import git4idea.repo.GitRemote
import git4idea.repo.GitRepository
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class GitRepositoryRemoteChangedListenerTest : DescribeSpec({
  val anyRemotes = listOf<GitRemote>()

  val project = mockk<Project>()

  var duoEnabledForGitLabProjectService = DuoEnabledForGitLabProjectService(project)

  val forceRefreshSlot = slot<Boolean>()

  val listener = GitRepositoryRemoteChangedListener(project)

  beforeEach {
    duoEnabledForGitLabProjectService = mockk<DuoEnabledForGitLabProjectService>()
    every { project.service<DuoEnabledForGitLabProjectService>() } returns duoEnabledForGitLabProjectService

    every { duoEnabledForGitLabProjectService.isDuoEnabledAtGitLabProjectLevel(capture(forceRefreshSlot)) } returns true
  }

  afterEach {
    clearAllMocks()
  }

  it("should invalidate and fetch duo enabled on first config update") {
    val repository = mockk<GitRepository> {
      every { remotes } returns anyRemotes
    }

    listener.notifyConfigChanged(repository)

    verify { duoEnabledForGitLabProjectService.isDuoEnabledAtGitLabProjectLevel(true) }
  }

  it("should invalidate and fetch duo enabled if remote urls have changed") {
    val repository = mockk<GitRepository> {
      every { remotes } returns listOf(remote("https://gitlab.com/test/test.git"))
    }
    listener.notifyConfigChanged(repository)

    val updatedRepository = mockk<GitRepository> {
      every { remotes } returns listOf(remote("https://gitlab.my-instance.com/test/test.git"))
    }
    listener.notifyConfigChanged(updatedRepository)

    verify(exactly = 2) { duoEnabledForGitLabProjectService.isDuoEnabledAtGitLabProjectLevel(true) }
  }

  it("should not invalidate and fetch duo enabled if remote urls have not changed") {
    val repository = mockk<GitRepository> {
      every { remotes } returns listOf(remote("https://gitlab.com/test/test.git"))
    }

    listener.notifyConfigChanged(repository)
    listener.notifyConfigChanged(repository)

    verify(exactly = 1) { duoEnabledForGitLabProjectService.isDuoEnabledAtGitLabProjectLevel(true) }
  }
})

private fun remote(url: String = "https://gitlab.com/test/test.git") = mockk<GitRemote> {
  every { firstUrl } returns url
}
