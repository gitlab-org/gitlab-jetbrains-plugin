package com.gitlab.plugin.workspace

import com.gitlab.plugin.authentication.OnePasswordTokenProvider
import com.gitlab.plugin.authentication.TokenProviderType
import com.intellij.openapi.components.service
import com.intellij.util.application

class WorkspaceSettingsBuilder(
  var url: String = "https://gitlab.com",
  var codeSuggestionEnabled: Boolean = true,
  var duoChatEnabled: Boolean = true,
  var tokenProviderType: TokenProviderType = TokenProviderType.PAT,
  var token: String = "my-awesome-token",
  var telemetry: Telemetry = Telemetry(
    enabled = true,
    trackingUrl = "https://telemetry.gitlab.com"
  ),
  var ignoreCertificateErrors: Boolean = false
) {
  companion object {
    operator fun invoke(body: WorkspaceSettingsBuilder.() -> Unit): WorkspaceSettings {
      return WorkspaceSettingsBuilder().apply(body).build()
    }
  }

  fun url(value: String) = apply {
    this.url = value
  }

  fun token(value: String) = apply {
    this.tokenProviderType = TokenProviderType.PAT
    this.token = value
  }

  fun onePassword(secret: String) = apply {
    this.tokenProviderType = TokenProviderType.ONE_PASSWORD
    this.token = application.service<OnePasswordTokenProvider>().getTokenBySecretReference(secret)
  }

  fun telemetry(telemetry: Telemetry) = apply {
    this.telemetry = telemetry
  }

  fun ignoreCertificateErrors(value: Boolean) = apply {
    this.ignoreCertificateErrors = value
  }

  fun build(): WorkspaceSettings {
    return WorkspaceSettings(
      url,
      codeSuggestionEnabled,
      duoChatEnabled,
      token,
      telemetry,
      ignoreCertificateErrors,
      tokenProviderType
    )
  }
}
