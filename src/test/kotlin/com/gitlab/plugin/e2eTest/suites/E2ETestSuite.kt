package com.gitlab.plugin.e2eTest.suites

import com.gitlab.plugin.e2eTest.tests.*
import org.junit.platform.suite.api.SelectClasses
import org.junit.platform.suite.api.Suite

@Suite
@SelectClasses(
  PluginDefaultsTest::class,
  AuthenticateTest::class,
  CodeSuggestionTest::class,
  DuoChatTest::class,
  QuickChatTest::class
)
class E2ETestSuite
