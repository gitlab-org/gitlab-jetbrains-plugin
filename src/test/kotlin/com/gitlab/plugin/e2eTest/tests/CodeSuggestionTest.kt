package com.gitlab.plugin.e2eTest.tests

import com.automation.remarks.junit5.Video
import com.gitlab.plugin.e2eTest.pages.IdeaFrame
import com.gitlab.plugin.e2eTest.pages.duoSettings
import com.gitlab.plugin.e2eTest.pages.idea
import com.gitlab.plugin.e2eTest.steps.EditorSteps
import com.gitlab.plugin.e2eTest.steps.ProjectSteps
import com.gitlab.plugin.e2eTest.utils.RemoteRobotExtension
import com.gitlab.plugin.e2eTest.utils.StepsLogger
import com.gitlab.plugin.e2eTest.utils.requestCodeSuggestion
import com.gitlab.plugin.e2eTest.utils.verifyCodeSuggestion
import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.fixtures.JLabelFixture
import com.intellij.remoterobot.fixtures.TextEditorFixture
import com.intellij.remoterobot.search.locators.byXpath
import com.intellij.remoterobot.stepsProcessing.step
import com.intellij.remoterobot.utils.keyboard
import com.intellij.remoterobot.utils.waitFor
import com.intellij.remoterobot.utils.waitForIgnoringError
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import java.awt.Point
import java.awt.event.KeyEvent
import java.time.Duration.*
import kotlin.test.assertTrue

@Suppress("ClassName")
@ExtendWith(RemoteRobotExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CodeSuggestionTest {

  init {
    StepsLogger.init()
  }

  @BeforeAll
  fun waitForIde(remoteRobot: RemoteRobot) = with(remoteRobot) {
    waitForIgnoringError(ofMinutes(3)) { callJs("true") }
    ProjectSteps(this).createNewProject()

    idea {
      step("Ensure GitLab Duo is enabled") {
        assert(isDuoEnabled())
      }

      openDuoSettings()
      duoSettings {
        setLanguageServerLogLevelToDebug()
      }
      button("OK").click()
    }
  }

  @AfterAll
  fun closeProject(remoteRobot: RemoteRobot) = ProjectSteps(remoteRobot).closeProject()

  @Nested
  inner class `with valid token set` {

    @Test
    @Video
    fun `simple code generation in new file`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      idea {
        ProjectSteps(remoteRobot).createNewFile("Java Class", "loop.java")
        EditorSteps(remoteRobot).clearContent()
      }

      val prompt = "// loop 5 times\\n"
      requestCodeSuggestion(remoteRobot, prompt)

      verifyCodeSuggestion(remoteRobot, prompt, "for")
    }

    @Test
    @Video
    fun `code completion for a line`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      idea {
        ProjectSteps(remoteRobot).createNewFile("Java Class", "Vehicle.java")
        EditorSteps(remoteRobot).clearContent()
      }

      val prompt = "public class Vehicle {\\nprivate String make;\\n\\npublic String getMake() {\\nreturn "
      requestCodeSuggestion(remoteRobot, prompt, suggestionCanDisplayBeforeTypingEnds = true)

      verifyCodeSuggestion(remoteRobot, prompt, "return (this.)?make;".toRegex())
    }

    @Test
    @Video
    fun `streamed code completion`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      idea {
        ProjectSteps(remoteRobot).createNewFile("Java Class", "StringUtils.java")
        EditorSteps(remoteRobot).clearContent()

        val prompt = "public class StringUtils {\\n// add isPalindrome, isEmpty and isBlank static methods\\n"
        requestCodeSuggestion(remoteRobot, prompt)

        waitFor(
          interval = ofMillis(20),
          duration = ofSeconds(10),
          errorMessage = "code suggestion did not display in chunks"
        ) {
          val text = textEditor().textIncludingGhostText()

          text.contains("boolean isPalindrome") && !text.contains("boolean isBlank")
        }

        verifyCodeSuggestion(
          remoteRobot,
          prompt,
          listOf(
            "boolean isPalindrome",
            "boolean isEmpty",
            "boolean isBlank"
          )
        )
      }
    }

    @Test
    @Video
    fun `code suggestion referencing open tab`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      idea {
        ProjectSteps(remoteRobot).createNewFile("Java Class", "Constants.java")
        EditorSteps(remoteRobot).clearContent()
        val content = "public class Constants {\\n public static final double FOO_XYZ_1 = 1.0;"
        EditorSteps(remoteRobot).addContentToEditor(content)

        ProjectSteps(remoteRobot).createNewFile("Java Class", "Usage.java")
        EditorSteps(remoteRobot).clearContent()
      }

      val prompt = "public class Usage {\\n public double xyz = C"
      requestCodeSuggestion(remoteRobot, prompt, suggestionCanDisplayBeforeTypingEnds = true)

      // Suggestion should reference content in other tab
      verifyCodeSuggestion(remoteRobot, prompt, "FOO_XYZ_1")
    }

    @Test
    @Video
    fun `cycling through code suggestions using shortcut keys`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      idea {
        ProjectSteps(remoteRobot).createNewFile("Java Class", "Magic.java")
        EditorSteps(remoteRobot).clearContent()

        // Fully typing "return" will trigger over-typing and not display the load icon.
        val prompt = "public class Message {\\npublic static String passcode() {\\nr"
        requestCodeSuggestion(remoteRobot, prompt)
        waitForDuoSuggestionReady()

        // Make tooltip is visible
        displayInlineCompletionTooltip()

        // Save first suggestion text
        val textAfterFirstSuggestion = textEditor().textIncludingGhostText()

        // Next suggestion
        keyboard { hotKey(KeyEvent.VK_ALT, KeyEvent.VK_CLOSE_BRACKET) }
        waitForDuoSuggestionReady()

        val textAfterSecondSuggestion = textEditor().textIncludingGhostText()
        waitFor(errorMessage = "next suggestion did not display") {
          val textChanged = textAfterFirstSuggestion != textAfterSecondSuggestion
          val tooltipTextChanged = jLabel(JLabelFixture.byContainsText("2/")).isVisible()

          textChanged && tooltipTextChanged
        }

        // Previous suggestion
        keyboard { hotKey(KeyEvent.VK_ALT, KeyEvent.VK_OPEN_BRACKET) }
        waitFor(errorMessage = "previous suggestion did not display") {
          val textRevertedToOriginal = textEditor().textIncludingGhostText() == textAfterFirstSuggestion
          val tooltipTextChanged = jLabel(JLabelFixture.byContainsText("1/")).isVisible()

          textRevertedToOriginal && tooltipTextChanged
        }

        // Accept suggestion
        keyboard { key(KeyEvent.VK_TAB) }
        assertTrue(textEditor().editor.text.contains("return"))
        waitForDuoSuggestionAccepted()
      }
    }

    @Test
    @Video
    fun `cycling through code suggestions by clicking the tooltip`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      idea {
        ProjectSteps(remoteRobot).createNewFile("Java Class", "Code.java")
        EditorSteps(remoteRobot).clearContent()

        // Fully typing "return" will trigger overtyping and not display the load icon.
        val prompt = "public class Message {\\npublic static String passcode() {\\nr"
        requestCodeSuggestion(remoteRobot, prompt)

        // We cannot know the exact message the AI will return
        waitForDuoSuggestionReady()
        val textAfterFirstSuggestion = textEditor().textIncludingGhostText()

        // Ensure tooltip is visible
        displayInlineCompletionTooltip()

        // Next suggestion, finding with the icon by xPath directly did not work.
        jLabel(byXpath("//div[@defaulticon='arrowExpand.svg']")).click()
        waitForDuoSuggestionReady()

        val textAfterSecondSuggestion = textEditor().textIncludingGhostText()
        waitFor(errorMessage = "next suggestion did not display") {
          val textChanged = textAfterFirstSuggestion != textAfterSecondSuggestion
          val tooltipTextChanged = jLabel(JLabelFixture.byContainsText("2/")).isVisible()

          textChanged && tooltipTextChanged
        }

        // Previous suggestion, finding with the icon by xPath directly did not work.
        jLabel(byXpath("//div[@defaulticon='arrowCollapse.svg']")).click()
        waitFor(errorMessage = "previous suggestion did not display") {
          val textRevertedToOriginal = textEditor().textIncludingGhostText() == textAfterFirstSuggestion
          val tooltipTextChanged = jLabel(JLabelFixture.byContainsText("1/")).isVisible()

          textRevertedToOriginal && tooltipTextChanged
        }

        // Accept suggestion
        keyboard { key(KeyEvent.VK_TAB) }
        assertTrue(textEditor().editor.text.contains("return"))
        waitForDuoSuggestionAccepted()
      }
    }

    private fun TextEditorFixture.textIncludingGhostText(): String {
      return editor.retrieveData().textDataList.joinToString("") { it.text }
    }

    private fun IdeaFrame.displayInlineCompletionTooltip() {
      val caretPosition = textEditor().editor.callJs<Point>(
        script = """
            const editor = local.get('editor')
            const visualPosition = editor.offsetToVisualPosition(${textEditor().editor.caretOffset})
            editor.visualPositionToXY(visualPosition)
        """.trimIndent(),
        runInEdt = true
      )

      textEditor().editor.moveMouse(caretPosition)

      waitFor(errorMessage = "tooltip did not display") {
        jLabel(JLabelFixture.byContainsText("GitLab Duo Code Suggestions")).isVisible()
      }
    }
  }
}
