package com.gitlab.plugin.e2eTest.tests

import com.automation.remarks.junit5.Video
import com.gitlab.plugin.e2eTest.pages.duoChatFrame
import com.gitlab.plugin.e2eTest.pages.idea
import com.gitlab.plugin.e2eTest.steps.ProjectSteps
import com.gitlab.plugin.e2eTest.utils.RemoteRobotExtension
import com.gitlab.plugin.e2eTest.utils.StepsLogger
import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.stepsProcessing.step
import com.intellij.remoterobot.utils.waitFor
import com.intellij.remoterobot.utils.waitForIgnoringError
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import java.time.Duration
import java.time.Duration.ofMinutes

@Suppress("ClassName")
@ExtendWith(RemoteRobotExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DuoChatTest {
  private val fileName = "Second"

  init {
    StepsLogger.init()
  }

  @BeforeAll
  fun waitForIde(remoteRobot: RemoteRobot) {
    waitForIgnoringError(ofMinutes(3)) { remoteRobot.callJs("true") }
    ProjectSteps(remoteRobot).createNewProject()
    remoteRobot.idea {
      step("Ensure GitLab Duo is enabled") {
        assert(isDuoEnabled())
      }

      ProjectSteps(remoteRobot).createNewFile("Java Class", fileName)
      openDuoChat()
    }
  }

  @AfterAll
  fun closeProject(remoteRobot: RemoteRobot) = ProjectSteps(remoteRobot).closeProject()

  @Nested
  inner class `with valid token set` {
    @Test
    @Video
    fun `GitLab Duo Chat simple request`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      idea {
        duoChatFrame {
          sendChat("hi")
          val expectedResponse = "GitLab Duo Chat"

          // We check for "GitLab Duo Chat" here because it is the only part of that response that is deterministic
          // Agent name is defined in https://gitlab.com/gitlab-org/gitlab/-/blob/35aa4271a2b621be73e555af03ac21d51b4f80a2/ee/lib/gitlab/llm/chain/agents/zero_shot/executor.rb#L16
          waitFor(
            Duration.ofSeconds(60),
            errorMessage = "Expected '$expectedResponse' to be present in the response from Duo Chat."
          ) {
            getLastChatMessage().contains(expectedResponse)
          }
        }
      }
    }
  }

  @Nested
  inner class `with pinned files` {
    @Test
    @Video
    fun `adding, removing, and including pinned file`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      val otherFileName = "Main"

      idea {
        duoChatFrame {
          clickChatInput()
          clearChatInput()
          sendChat("/include")
          waitForFileContextMenu()

          selectFile(otherFileName)
          waitFor(
            Duration.ofSeconds(10),
            errorMessage = "Expected '$otherFileName' to be in the pinned context"
          ) {
            getPinnedContextFiles().first().html.contains(otherFileName)
          }

          removePinnedFile(otherFileName)
          waitFor(
            Duration.ofSeconds(10),
            errorMessage = "Expected '$otherFileName' no longer to be in the pinned context"
          ) {
            getPinnedContextSearchResults().isEmpty()
          }

          closeFileContextMenu()
          clearChatInput()
        }
      }
    }

    @Test
    @Video
    fun `sending prompt with pinned file`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      idea {
        duoChatFrame {
          clearChatInput()
          sendChat("/include")

          waitForFileContextMenu()
          selectFile(fileName)
          closeFileContextMenu()
          // Ask for the file name backwards so there's no false positive from the message that originally contained the file
          sendChat("What is the name of this file spelled backwards?")

          val expectedResponse = fileName.reversed()
          waitFor(
            Duration.ofSeconds(30),
            errorMessage = "Expected '$expectedResponse' to be present in the response from Duo Chat."
          ) {
            getLastChatMessage().contains(expectedResponse, ignoreCase = true)
          }
        }
      }
    }
  }
}
