package com.gitlab.plugin.chat.context.managers

import com.gitlab.plugin.chat.context.AiContextCategory
import com.gitlab.plugin.chat.context.AiContextItem
import com.gitlab.plugin.chat.context.AiContextSearchQuery
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import org.junit.jupiter.api.assertDoesNotThrow
import java.util.concurrent.CompletableFuture

class LanguageServerContextManagerTest : DescribeSpec({
  lateinit var contextManager: LanguageServerContextManager

  val project = mockk<Project>()
  val languageServerService = mockk<GitLabLanguageServerService>(relaxed = true)
  val logger = mockk<Logger>(relaxed = true)

  beforeEach {
    mockkStatic(Logger::class)
    every { Logger.getInstance(any<Class<*>>()) } returns logger
    every { project.service<GitLabLanguageServerService>() } returns languageServerService
    contextManager = LanguageServerContextManager(project)
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  describe("query") {
    val query = AiContextSearchQuery(
      category = AiContextCategory.FILE,
      query = "index",
      workspaceFolders = emptyList()
    )

    it("should return empty list when language server returns empty list") {
      every { languageServerService.lspServer?.aiContextQuery(any()) } returns CompletableFuture.completedFuture(emptyList())

      contextManager.query(query) shouldBe emptyList()
    }

    it("should return empty list when language server is null") {
      every { languageServerService.lspServer } returns null

      contextManager.query(query) shouldBe emptyList()
    }

    it("should return empty list when language server throws error") {
      every {
        languageServerService.lspServer?.aiContextQuery(any())
      } throws RuntimeException("Test exception")

      contextManager.query(query) shouldBe emptyList()
    }

    it("should return empty list when language server returns failed future") {
      every {
        languageServerService.lspServer?.aiContextQuery(any())
      } returns CompletableFuture.failedFuture(RuntimeException("Test exception"))

      contextManager.query(query) shouldBe emptyList()
    }

    it("should return results when language server returns results") {
      val expectedContextItems = listOf(
        AiContextItem(
          id = "abc123",
          category = AiContextCategory.FILE,
          content = "Some content",
          metadata = null
        )
      )

      every {
        languageServerService.lspServer?.aiContextQuery(any())
      } returns CompletableFuture.completedFuture(expectedContextItems)

      val searchQuery = AiContextSearchQuery(
        category = AiContextCategory.FILE,
        query = "index",
        workspaceFolders = emptyList()
      )
      val result = contextManager.query(searchQuery)

      result shouldBe expectedContextItems

      verify(exactly = 1) {
        languageServerService.lspServer?.aiContextQuery(searchQuery)
      }
    }
  }

  describe("add") {
    val item = AiContextItem(
      id = "abc123",
      category = AiContextCategory.FILE,
      content = "some content",
      metadata = null
    )

    it("should call the appropriate language server method") {
      every { languageServerService.lspServer?.aiContextAdd(any()) } returns CompletableFuture.completedFuture(true)

      contextManager.add(item)

      verify(exactly = 1) {
        languageServerService.lspServer?.aiContextAdd(item)
      }
    }
  }

  describe("remove") {
    val item = AiContextItem(
      id = "abc123",
      category = AiContextCategory.FILE,
      content = "some content",
      metadata = null
    )

    it("should call the appropriate language server method") {
      every { languageServerService.lspServer?.aiContextRemove(any()) } returns CompletableFuture.completedFuture(true)

      contextManager.remove(item)

      verify(exactly = 1) {
        languageServerService.lspServer?.aiContextRemove(item)
      }
    }
  }

  describe("getCurrentItems") {
    it("should return empty list when language server is null") {
      every { languageServerService.lspServer } returns null

      contextManager.getCurrentItems() shouldBe emptyList()
    }

    it("should return empty list when language server returns failed future") {
      every {
        languageServerService.lspServer?.aiContextCurrentItems()
      } returns CompletableFuture.failedFuture(RuntimeException("Test exception"))

      contextManager.getCurrentItems() shouldBe emptyList()
    }

    it("should return empty list when language server throws error") {
      every {
        languageServerService.lspServer?.aiContextCurrentItems()
      } throws RuntimeException("Test exception")

      contextManager.getCurrentItems() shouldBe emptyList()
    }

    it("should return current items from language server") {
      val expectedItems = listOf(
        AiContextItem(
          id = "item1",
          category = AiContextCategory.FILE,
          content = "Content 1",
          metadata = null
        )
      )

      every {
        languageServerService.lspServer?.aiContextCurrentItems()
      } returns CompletableFuture.completedFuture(expectedItems)

      contextManager.getCurrentItems() shouldBe expectedItems
      verify(exactly = 1) { languageServerService.lspServer?.aiContextCurrentItems() }
    }
  }

  describe("retrieveSelectedContextItemsWithContent") {
    it("should return an empty list when language server is null") {
      every { languageServerService.lspServer } returns null

      contextManager.retrieveSelectedContextItemsWithContent() shouldBe emptyList()
    }

    it("should return an empty list when language server returns failed future") {
      every {
        languageServerService.lspServer?.aiContextRetrieve()
      } returns CompletableFuture.failedFuture(RuntimeException("Test exception"))

      contextManager.retrieveSelectedContextItemsWithContent() shouldBe emptyList()
    }

    it("should return an empty list when language server throws error") {
      every { languageServerService.lspServer?.aiContextRetrieve() } throws RuntimeException("Test error")

      contextManager.retrieveSelectedContextItemsWithContent() shouldBe emptyList()
    }

    it("should return context items from language server") {
      val expectedContextItems = listOf(
        AiContextItem(
          id = "item1",
          category = AiContextCategory.FILE,
          content = "Content 1",
          metadata = null
        ),
        AiContextItem(
          id = "item2",
          category = AiContextCategory.SNIPPET,
          content = "Content 2",
          metadata = null
        )
      )

      every {
        languageServerService.lspServer?.aiContextRetrieve()
      } returns CompletableFuture.completedFuture(expectedContextItems)

      val result = contextManager.retrieveSelectedContextItemsWithContent()

      result shouldBe expectedContextItems

      verify(exactly = 1) { languageServerService.lspServer?.aiContextRetrieve() }
    }
  }

  describe("getAvailableCategories") {
    it("should return empty list when language server is null") {
      every { languageServerService.lspServer } returns null

      contextManager.getAvailableCategories() shouldBe emptyList()
    }

    it("should return empty list when language server returns failed future") {
      every {
        languageServerService.lspServer?.aiContextGetProviderCategories()
      } returns CompletableFuture.failedFuture(RuntimeException("Test exception"))

      contextManager.getAvailableCategories() shouldBe emptyList()
    }

    it("should return empty list when language server throws error") {
      every {
        languageServerService.lspServer?.aiContextGetProviderCategories()
      } throws RuntimeException("Test exception")

      contextManager.getAvailableCategories() shouldBe emptyList()
    }

    it("should return available categories from language server") {
      val expectedCategories = listOf(
        AiContextCategory.FILE
      )

      every {
        languageServerService.lspServer?.aiContextGetProviderCategories()
      } returns CompletableFuture.completedFuture(expectedCategories)

      contextManager.getAvailableCategories() shouldBe expectedCategories
      verify(exactly = 1) { languageServerService.lspServer?.aiContextGetProviderCategories() }
    }
  }

  describe("clearSelectedContextItems") {
    it("should call the appropriate language server method") {
      every { languageServerService.lspServer?.aiContextClear() } returns CompletableFuture.completedFuture(true)

      contextManager.clearSelectedContextItems()

      verify(exactly = 1) {
        languageServerService.lspServer?.aiContextClear()
      }
    }

    it("should catch and log the error when an exception is thrown") {
      every {
        languageServerService.lspServer?.aiContextClear()
      } throws RuntimeException("Test exception")

      assertDoesNotThrow { contextManager.clearSelectedContextItems() }

      verify(exactly = 1) {
        logger.warn("Failed to clear selected AI context items from language server", any<Throwable>())
      }
    }
  }

  describe("shouldIncludeAdditionalContext") {
    it("should be true when category retrieval returns values") {
      every {
        languageServerService.lspServer?.aiContextGetProviderCategories()
      } returns CompletableFuture.completedFuture(listOf(AiContextCategory.FILE))

      contextManager.shouldIncludeAdditionalContext() shouldBe true
    }

    it("should be false when category retrieval returns an empty list") {
      every {
        languageServerService.lspServer?.aiContextGetProviderCategories()
      } returns CompletableFuture.completedFuture(listOf())

      contextManager.shouldIncludeAdditionalContext() shouldBe false
    }

    it("should be false when language server is null") {
      every { languageServerService.lspServer } returns null

      contextManager.shouldIncludeAdditionalContext() shouldBe false
    }

    it("should be false when language server throws error") {
      every {
        languageServerService.lspServer?.aiContextGetProviderCategories()
      } throws RuntimeException("Test exception")

      contextManager.shouldIncludeAdditionalContext() shouldBe false
    }

    it("should be false when language server returns failed future") {
      every {
        languageServerService.lspServer?.aiContextGetProviderCategories()
      } returns CompletableFuture.failedFuture(RuntimeException("Test exception"))

      contextManager.shouldIncludeAdditionalContext() shouldBe false
    }
  }
})
