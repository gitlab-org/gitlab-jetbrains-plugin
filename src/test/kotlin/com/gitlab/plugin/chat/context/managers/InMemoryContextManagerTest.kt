package com.gitlab.plugin.chat.context.managers

import com.gitlab.plugin.chat.context.*
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.collections.shouldContainOnly
import io.kotest.matchers.shouldBe
import java.util.UUID

class InMemoryContextManagerTest : DescribeSpec({
  lateinit var manager: InMemoryContextManager

  beforeEach {
    manager = InMemoryContextManager()
  }

  it("should add context items") {
    val item1 = createContextItem()
    val item2 = createContextItem()

    manager.add(item1)
    manager.add(item2)

    manager.retrieveSelectedContextItemsWithContent() shouldContainOnly listOf(item1, item2)
  }

  it("should remove context items") {
    val item1 = createContextItem().also { manager.add(it) }
    val item2 = createContextItem().also { manager.add(it) }

    manager.remove(item1)

    manager.retrieveSelectedContextItemsWithContent() shouldContainOnly listOf(item2)
  }

  it("should clear the context items") {
    manager.add(createContextItem())
    manager.add(createContextItem())

    manager.clearSelectedContextItems()

    manager.getCurrentItems().size shouldBe 0
  }

  it("should only return snippet category") {
    manager.getAvailableCategories() shouldContainOnly listOf(AiContextCategory.SNIPPET)
  }

  it("should have additional context disabled") {
    manager.shouldIncludeAdditionalContext() shouldBe false
  }
})

private fun createContextItem() = AiContextItem(
  id = UUID.randomUUID().toString(),
  category = AiContextCategory.SNIPPET,
  content = "",
  metadata = AIContextItemMetadata(
    title = "Selection",
    enabled = true,
    subType = AIContextProviderType.SNIPPET
  )
)
