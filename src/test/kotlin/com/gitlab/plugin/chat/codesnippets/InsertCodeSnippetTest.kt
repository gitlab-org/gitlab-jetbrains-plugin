package com.gitlab.plugin.chat.codesnippets

import com.gitlab.plugin.chat.quickchat.QUICK_CHAT_SESSION_KEY
import com.gitlab.plugin.chat.quickchat.QuickChatSession
import com.gitlab.plugin.chat.view.model.InsertCodeSnippetMessage
import com.gitlab.plugin.util.code.CodeFormatter
import com.gitlab.plugin.util.code.CodeFormattingContext
import com.intellij.lang.Language
import com.intellij.openapi.application.readAction
import com.intellij.openapi.application.runInEdt
import com.intellij.openapi.application.runUndoTransparentWriteAction
import com.intellij.openapi.editor.CaretModel
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.SelectionModel
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiFile
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.runBlocking

class InsertCodeSnippetTest : DescribeSpec({
  val project = mockk<Project>()
  val language = mockk<Language>()

  val quickChatSession = mockk<QuickChatSession>(relaxUnitFun = true)

  val fileEditorManager = mockk<FileEditorManager>()
  val psiDocumentManager = mockk<PsiDocumentManager>()

  val editor = mockk<Editor>()
  val psiFile = mockk<PsiFile>()
  val document = mockk<Document>(relaxUnitFun = true)
  val caretModel = mockk<CaretModel>()
  val selectionModel = mockk<SelectionModel>()

  val message = InsertCodeSnippetMessage(snippet = "return 10;")
  val insertCodeSnippetService = InsertCodeSnippetService(project)

  beforeSpec {
    mockkConstructor(CodeFormatter::class)

    mockkStatic(PsiDocumentManager::class)
    mockkStatic(FileEditorManager::getInstance)
    mockkStatic(::runInEdt)

    // To mock runUndoTransparentWriteAction since it's a generic static method.
    mockkStatic("com.intellij.openapi.application.ActionsKt")
    // To mock readAction since it's a generic static method.
    mockkStatic("com.intellij.openapi.application.CoroutinesKt")
  }

  beforeEach {
    every { PsiDocumentManager.getInstance(project) } returns psiDocumentManager
    every { psiDocumentManager.getPsiFile(document) } returns psiFile
    every { psiFile.language } returns language

    every { FileEditorManager.getInstance(project) } returns fileEditorManager
    every { fileEditorManager.selectedTextEditor } returns editor
    every { editor.document } returns document
    every { editor.caretModel } returns caretModel
    every { editor.selectionModel } returns selectionModel
    every { editor.getUserData(QUICK_CHAT_SESSION_KEY) } returns quickChatSession

    every { document.charsSequence } returns """
      fun test(): Int {
        return 42;
      }
    """.trimIndent()

    every { runInEdt(null, any<() -> Unit>()) } answers { secondArg<() -> Unit>()() }
    every { runUndoTransparentWriteAction(any<() -> Unit>()) } answers { firstArg<() -> Unit>()() }
    coEvery { readAction(any<() -> Any>()) } answers { firstArg<() -> Any>()() }

    coEvery { anyConstructed<CodeFormatter>().format(message.snippet, any()) } answers { firstArg<String>() }
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("should not insert the code snippet if no editors are active") {
    every { fileEditorManager.selectedTextEditor } returns null

    runBlocking {
      insertCodeSnippetService.insertCodeSnippet(message)
    }

    verify(exactly = 0) {
      document.replaceString(any(), any(), any())
      document.insertString(any(), any())
    }
  }

  it("should insert the code snippet at cursor offset if there is no selection") {
    every { selectionModel.hasSelection() } returns false
    every { caretModel.offset } returns 32

    runBlocking {
      insertCodeSnippetService.insertCodeSnippet(message)
    }

    verify(exactly = 1) {
      document.insertString(32, message.snippet)
      quickChatSession.selectionChanged(editor)
    }
    val context = slot<CodeFormattingContext>()
    coVerify(exactly = 1) {
      anyConstructed<CodeFormatter>().format(
        message.snippet,
        capture(context)
      )
    }
    context.captured.language shouldBe language
    context.captured.project shouldBe project
    context.captured.editor shouldBe editor
    context.captured.prefix shouldBe "fun test(): Int {\n  return 42;\n}"
    context.captured.suffix shouldBe ""
  }

  it("should replace the selection with the code snippet if there is a selection") {
    every { selectionModel.hasSelection() } returns true
    every { selectionModel.selectionStart } returns 20
    every { selectionModel.selectionEnd } returns 30

    runBlocking {
      insertCodeSnippetService.insertCodeSnippet(message)
    }

    verify(exactly = 1) {
      document.replaceString(20, 30, message.snippet)
      quickChatSession.selectionChanged(editor)
    }

    val context = slot<CodeFormattingContext>()
    coVerify(exactly = 1) {
      anyConstructed<CodeFormatter>().format(
        message.snippet,
        capture(context)
      )
    }
    context.captured.language shouldBe language
    context.captured.project shouldBe project
    context.captured.editor shouldBe editor
    context.captured.prefix shouldBe "fun test(): Int {\n  "
    context.captured.suffix shouldBe "\n}"
  }

  it("should not format the code snippet to insert if it can't find the PsiFile") {
    every { selectionModel.hasSelection() } returns false
    every { psiDocumentManager.getPsiFile(document) } returns null
    every { caretModel.offset } returns 32

    runBlocking {
      insertCodeSnippetService.insertCodeSnippet(message)
    }

    verify(exactly = 1) {
      document.insertString(32, message.snippet)
      quickChatSession.selectionChanged(editor)
    }
    coVerify(exactly = 0) {
      anyConstructed<CodeFormatter>().format(
        message.snippet,
        any()
      )
    }
  }

  it("should not format the code snippet to replace if it can't find the PsiFile") {
    every { selectionModel.hasSelection() } returns true
    every { selectionModel.selectionStart } returns 20
    every { selectionModel.selectionEnd } returns 30
    every { psiDocumentManager.getPsiFile(document) } returns null

    runBlocking {
      insertCodeSnippetService.insertCodeSnippet(message)
    }

    verify(exactly = 1) {
      document.replaceString(20, 30, message.snippet)
      quickChatSession.selectionChanged(editor)
    }

    coVerify(exactly = 0) {
      anyConstructed<CodeFormatter>().format(
        message.snippet,
        any()
      )
    }
  }
})
