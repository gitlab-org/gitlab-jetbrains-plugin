package com.gitlab.plugin.lsp.services

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.lsp.capabilities.LanguageServerCapabilityManager
import com.gitlab.plugin.lsp.listeners.LanguageServerStartedListener
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.intellij.ide.plugins.IdeaPluginDescriptor
import com.intellij.ide.plugins.PluginManagerCore
import com.intellij.openapi.application.ApplicationInfo
import com.intellij.openapi.application.ApplicationNamesInfo
import com.intellij.openapi.components.service
import com.intellij.openapi.extensions.PluginId
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.guessProjectDir
import com.intellij.openapi.util.BuildNumber
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.util.net.HttpConfigurable
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.mockk.*
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlin.io.path.Path
import kotlin.io.path.pathString

class GitLabLanguageServerServiceITTest : DescribeSpec({
  val pluginPath = Path(System.getProperty("user.dir"))

  val pluginId = mockk<PluginId>()
  val project = mockk<Project>(relaxed = true)
  val coroutineScope = TestScope(UnconfinedTestDispatcher())
  val languageServerCapabilityManager = mockk<LanguageServerCapabilityManager>(relaxUnitFun = true)
  val publisher = mockk<LanguageServerStartedListener>(relaxUnitFun = true)
  val ideaPluginDescription = mockk<IdeaPluginDescriptor>()

  val service = GitLabLanguageServerService(project, coroutineScope)

  beforeSpec {
    mockkStatic(PluginId::getId)
    mockkStatic(PluginManagerCore::getPlugin)
    mockkStatic(Project::guessProjectDir)
    mockkStatic(ApplicationInfo::getInstance)
    mockkStatic(ApplicationNamesInfo::getInstance)
    mockkStatic(HttpConfigurable::class)

    mockkConstructor(GitLabNotificationManager::class)
    mockkObject(GitLabBundle)
  }

  beforeEach {
    every { HttpConfigurable.getInstance() } returns mockk<HttpConfigurable>()

    every { GitLabBundle.plugin() } returns mockk<IdeaPluginDescriptor> {
      every { name } returns "GitLab Duo"
      every { version } returns "1.0.0"
    }

    every { ApplicationInfo.getInstance() } returns mockk<ApplicationInfo> {
      every { build } returns BuildNumber("1")
      every { shortCompanyName } returns "GitLab"
    }

    every { ApplicationNamesInfo.getInstance() } returns mockk<ApplicationNamesInfo> {
      every { fullProductName } returns "JetBrains"
    }

    every { PluginId.getId("com.gitlab.plugin") } returns pluginId
    every { PluginManagerCore.getPlugin(pluginId) } returns ideaPluginDescription

    every { ideaPluginDescription.pluginId } returns pluginId
    every {
      ideaPluginDescription.pluginPath
    } returns pluginPath.resolve("build/idea-sandbox/plugins-test/gitlab-jetbrains-plugin")

    every { project.name } returns "example-plugin-name"
    every { project.guessProjectDir() } returns mockk<VirtualFile>().also {
      every { it.url } returns pluginPath.pathString
      every { it.name } returns "gitlab-jetbrains-plugin"
      every { it.path } returns pluginPath.pathString
    }
    every {
      project.messageBus.syncPublisher(LanguageServerStartedListener.LANGUAGE_SERVER_STARTED_TOPIC)
    } returns publisher

    every { project.service<LanguageServerCapabilityManager>() } returns languageServerCapabilityManager

    every { anyConstructed<GitLabNotificationManager>().sendNotification(any(), any(), any(), project) } returns Unit
  }

  afterEach {
    clearAllMocks(answers = false)

    service.stopServer()
  }

  afterSpec {
    unmockkAll()
  }

  it("should be able to start the server and stop the server") {
    service.startServer()
    service.lspServer?.shouldNotBeNull()
    verify(exactly = 1, timeout = 10000) { publisher.onLanguageServerStarted() }

    service.stopServer()
    service.lspServer?.shouldBeNull()
  }

  it("should be able to start the server again after an error") {
    every { ideaPluginDescription.pluginPath } returns pluginPath.resolve("non-existent")
    shouldThrow<IllegalStateException> { service.startServer() }

    every {
      ideaPluginDescription.pluginPath
    } returns pluginPath.resolve("build/idea-sandbox/plugins-test/gitlab-jetbrains-plugin")
    service.startServer()
    service.lspServer?.shouldNotBeNull()
    verify(exactly = 1, timeout = 10000) { publisher.onLanguageServerStarted() }
  }
})
