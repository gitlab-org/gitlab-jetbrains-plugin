package com.gitlab.plugin.lsp.listeners

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.lsp.params.Settings
import com.gitlab.plugin.lsp.params.toDto
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.lsp.settings.LanguageServerSettings
import com.gitlab.plugin.lsp.settings.LanguageServerSettingsState
import com.gitlab.plugin.workspace.Telemetry
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import org.eclipse.lsp4j.DidChangeConfigurationParams

class ConfigurationChangedLanguageServerListenerTest : DescribeSpec({
  val url = "https://gitlab.com"
  val token = "gl-pat123456"
  val project = mockk<Project>()
  val telemetry = Telemetry(
    enabled = true,
    trackingUrl = "https://telemetrytest.gitlab.com"
  )
  val ignoreCertificateErrors = true

  val languageServerSettingsState = mockk<LanguageServerSettingsState>(relaxed = true)
  val languageServerSettings = mockk<LanguageServerSettings>()
  val languageServerService = mockk<GitLabLanguageServerService>(relaxed = true)

  val listener = ConfigurationChangedLanguageServerListener(project)

  beforeEach {
    mockApplication()

    every { project.getService(GitLabLanguageServerService::class.java) } returns languageServerService
    every { project.getService(LanguageServerSettings::class.java) } returns languageServerSettings

    every { languageServerSettings.state } returns languageServerSettingsState
  }

  afterEach {
    clearAllMocks()
  }

  it("should update the language server configuration with the given configuration") {
    val settings = WorkspaceSettingsBuilder {
      url(url)
      token(token)
      telemetry(telemetry)
      ignoreCertificateErrors(ignoreCertificateErrors)
      duoChatEnabled = true
      codeSuggestionEnabled = false
    }

    listener.onConfigurationChange(settings)

    val params = slot<DidChangeConfigurationParams>()
    verify {
      languageServerService
        .lspServer
        ?.workspaceService
        ?.didChangeConfiguration(capture(params))
    }
    val configurationSettings = params.captured.settings as Settings
    configurationSettings.baseUrl shouldBe url
    configurationSettings.token shouldBe token
    configurationSettings.telemetry shouldBe telemetry.toDto()
    configurationSettings.ignoreCertificateErrors shouldBe ignoreCertificateErrors
    configurationSettings.duoChat?.enabled shouldBe true
    configurationSettings.codeCompletion?.enabled shouldBe false
  }

  it("should update configuration related to language server specific settings") {
    listener.onLanguageServerSettingsChanged()

    val params = slot<DidChangeConfigurationParams>()
    verify {
      languageServerService
        .lspServer
        ?.workspaceService
        ?.didChangeConfiguration(capture(params))
    }
    val configurationSettings = params.captured.settings as Settings
    configurationSettings.logLevel shouldBe languageServerSettingsState.workspaceSettings.logLevel
    configurationSettings.openTabsContext shouldBe languageServerSettingsState.workspaceSettings.codeCompletion.enableOpenTabsContext
    configurationSettings.codeCompletion shouldBe languageServerSettingsState.workspaceSettings.codeCompletion.toDto()
    configurationSettings.httpAgentOptions shouldBe languageServerSettingsState.workspaceSettings.httpAgentOptions.toDto()
    configurationSettings.featureFlags shouldBe languageServerSettingsState.workspaceSettings.featureFlags.toDto()
  }
})
