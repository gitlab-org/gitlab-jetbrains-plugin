package com.gitlab.plugin.authentication

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.lsp.GitLabLanguageServer
import com.gitlab.plugin.lsp.params.Settings
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.ProjectManager
import com.intellij.util.application
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import org.eclipse.lsp4j.DidChangeConfigurationParams
import org.eclipse.lsp4j.services.WorkspaceService

class TokenUpdateLanguageServerServiceTest : DescribeSpec({
  lateinit var tokenUpdateService: TokenUpdateLanguageServerService
  lateinit var projectManager: ProjectManager
  lateinit var tokenProviderManager: GitLabTokenProviderManager

  beforeTest {
    tokenUpdateService = TokenUpdateLanguageServerService()
    projectManager = mockk<ProjectManager>()
    tokenProviderManager = mockk<GitLabTokenProviderManager>()

    mockkStatic(ProjectManager::class)
    every { ProjectManager.getInstance() } returns projectManager

    mockApplication()
    every { application.service<GitLabTokenProviderManager>() } returns tokenProviderManager
  }

  afterTest {
    unmockkAll()
  }

  describe("updateTokenLSConfiguration") {
    it("should update token for all open projects") {
      val newToken = "new_token"
      val project1 = mockk<Project>()
      val project2 = mockk<Project>()
      val lsService1 = mockk<GitLabLanguageServerService>()
      val lsService2 = mockk<GitLabLanguageServerService>()
      val lsServer1 = mockk<GitLabLanguageServer>()
      val lsServer2 = mockk<GitLabLanguageServer>()
      val workspaceService1 = mockk<WorkspaceService>()
      val workspaceService2 = mockk<WorkspaceService>()

      every { projectManager.openProjects } returns arrayOf(project1, project2)
      every { project1.service<GitLabLanguageServerService>() } returns lsService1
      every { project2.service<GitLabLanguageServerService>() } returns lsService2
      every { lsService1.lspServer } returns lsServer1
      every { lsService2.lspServer } returns lsServer2
      every { lsServer1.workspaceService } returns workspaceService1
      every { lsServer2.workspaceService } returns workspaceService2
      every { workspaceService1.didChangeConfiguration(any()) } just Runs
      every { workspaceService2.didChangeConfiguration(any()) } just Runs

      tokenUpdateService.updateTokenLSConfiguration(newToken)

      verify {
        workspaceService1.didChangeConfiguration(
          DidChangeConfigurationParams(Settings(token = newToken))
        )
        workspaceService2.didChangeConfiguration(
          DidChangeConfigurationParams(Settings(token = newToken))
        )
      }
    }

    it("should use token from provider when new token is empty") {
      // Arrange
      val project = mockk<Project>()
      val lsService = mockk<GitLabLanguageServerService>()
      val lsServer = mockk<GitLabLanguageServer>()
      val workspaceService = mockk<WorkspaceService>()

      every { projectManager.openProjects } returns arrayOf(project)
      every { project.service<GitLabLanguageServerService>() } returns lsService
      every { lsService.lspServer } returns lsServer
      every { lsServer.workspaceService } returns workspaceService
      every { workspaceService.didChangeConfiguration(any()) } just Runs

      val providerToken = "provider_token"
      every { tokenProviderManager.getToken() } returns providerToken

      tokenUpdateService.updateTokenLSConfiguration("")

      verify {
        workspaceService.didChangeConfiguration(
          DidChangeConfigurationParams(Settings(token = providerToken))
        )
      }
    }
  }
})
