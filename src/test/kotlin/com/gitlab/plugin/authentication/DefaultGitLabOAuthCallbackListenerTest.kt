package com.gitlab.plugin.authentication

import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class DefaultGitLabOAuthCallbackListenerTest : DescribeSpec({
  describe("DefaultGitLabOAuthCallbackListener") {
    it("calls onSuccess when result is accepted") {
      val onSuccess = mockk<() -> Unit>(relaxed = true)
      val onFailure = mockk<() -> Unit>(relaxed = true)
      val listener = DefaultGitLabOAuthCallbackListener(onSuccess, onFailure)

      listener.onOAuthCallback(true)

      verify(exactly = 1) { onSuccess() }
      verify(exactly = 0) { onFailure() }
    }

    it("calls onFailure when result is not accepted") {
      val onSuccess = mockk<() -> Unit>(relaxed = true)
      val onFailure = mockk<() -> Unit>(relaxed = true)
      val listener = DefaultGitLabOAuthCallbackListener(onSuccess, onFailure)

      listener.onOAuthCallback(false)

      verify(exactly = 0) { onSuccess() }
      verify(exactly = 1) { onFailure() }
    }

    it("uses empty lambda for onFailure if not provided") {
      val onSuccess = mockk<() -> Unit>(relaxed = true)
      val listener = DefaultGitLabOAuthCallbackListener(onSuccess)

      listener.onOAuthCallback(false)

      verify(exactly = 0) { onSuccess() }
    }
  }
})
