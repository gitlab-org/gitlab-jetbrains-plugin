package com.gitlab.plugin.authentication

import com.gitlab.plugin.api.mockApplication
import com.intellij.openapi.components.service
import com.intellij.util.application
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import kotlin.test.assertEquals

class GitLabTokenProviderManagerTest : DescribeSpec({
  lateinit var tokenProviderManager: GitLabTokenProviderManager
  lateinit var oAuthTokenProvider: OAuthTokenProvider
  lateinit var patProvider: PatProvider

  beforeTest {
    oAuthTokenProvider = mockk()
    patProvider = mockk()

    mockApplication()
    every { application.service<OAuthTokenProvider>() } returns oAuthTokenProvider
    every { application.service<PatProvider>() } returns patProvider

    tokenProviderManager = GitLabTokenProviderManager()
  }

  afterTest {
    unmockkAll()
  }

  describe("GitLabTokenProviderManager") {
    describe("getToken") {
      it("returns OAuth token when available") {
        every { oAuthTokenProvider.token() } returns "oauth_token"
        every { patProvider.token() } returns "pat_token"

        val result = tokenProviderManager.getToken()

        assertEquals(result, "oauth_token")
      }

      it("returns PAT when OAuth is not available") {
        every { oAuthTokenProvider.token() } returns ""
        every { patProvider.token() } returns "pat_token"

        val result = tokenProviderManager.getToken()

        assertEquals(result, "pat_token")
      }

      it("returns PAT when explicitly requested") {
        every { oAuthTokenProvider.token() } returns "oauth_token"
        every { patProvider.token() } returns "pat_token"

        val result = tokenProviderManager.getToken(TokenProviderType.PAT)

        assertEquals(result, "pat_token")
      }

      it("returns empty string when no tokens are available") {
        every { oAuthTokenProvider.token() } returns ""
        every { patProvider.token() } returns ""

        val result = tokenProviderManager.getToken()

        assertEquals(result, "")
      }
    }
  }
})
