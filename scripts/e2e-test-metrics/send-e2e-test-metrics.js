import {exit} from 'process';
import fs from 'fs';
import {InfluxDB, Point, HttpError, RequestTimedOutError} from '@influxdata/influxdb-client';
import {parse} from 'junit2json';
import {join} from 'path';

const RESULTS_DIR = '../../build/test-results/test'

const INFLUXDB = {
  token: process.env.EXTENSIONS_E2E_TEST_INFLUXDB_TOKEN,
  url: 'https://influxdb.quality.gitlab.net',
  org: 'gitlab-qa',
  bucket: 'extensions-e2e-test-results',
};

const validateInfluxToken = () => {
  if (!INFLUXDB.token) {
    console.error('Error: InfluxDB token is not set. Please set the EXTENSIONS_E2E_TEST_INFLUXDB_TOKEN environment variable.');
    exit(1);
  }
};

const validateResultsDirectory = () => {
  if (!fs.existsSync(RESULTS_DIR)) {
    console.error(`Error: Directory ${RESULTS_DIR} does not exist.`);
    exit(1);
  }
};

const getTestSuiteXmlFiles = () => {
  return fs.readdirSync(RESULTS_DIR).filter(file => file.endsWith('.xml'));
};

const parseTestSuites = async xmlFiles => {
  const suites = [];
  for (const file of xmlFiles) {
    const xmlContent = fs.readFileSync(join(RESULTS_DIR,file), 'utf8');
    const jsonResult = await parse(xmlContent);
    const filteredResult = filterTestLogs(jsonResult);

    if (filteredResult.tests && filteredResult.tests > 0) {
      suites.push(filteredResult);
    }
  }
  return suites;
};

const filterTestLogs = jsonResult => {
  return JSON.parse(
    JSON.stringify(jsonResult, (key, value) => {
      if (key === 'system-err' || key === 'system-out') {
        return undefined;
      }
      return value;
    }),
  );
};

// A point represents a single InfluxDB data record: https://docs.influxdata.com/influxdb/v1/concepts/glossary/#point
const createTestResultPoint = (suite, test, isMergeRequest) => {
  const suiteName = formatSuiteName(suite.name);
  const testName = test.name.replace('(RemoteRobot)', '');

  // Do not push metrics for skipped tests, or tests that failed to initialize
  if (test.skipped !== undefined || testName === 'initializationError') {
    return null;
  }

  return new Point('test-results-jetbrains')
    .tag('suite', suiteName)
    .tag('name', testName)
    .tag('result', test.failure ? 'failed' : 'passed')
    .tag('job_name', 'test-e2e')
    .tag('merge_request', isMergeRequest)
    .tag('stage', 'create')
    .tag('product_group', 'editor_extensions')
    .floatField('run_time', test.time)
    .stringField('id', `${suiteName}_${testName}`.replace(/ /g, '_'))
    .stringField('environment', 'gitlab.com')
    .stringField('job_url', process.env.CI_JOB_URL)
    .stringField('pipeline_url', process.env.CI_PIPELINE_URL)
    .stringField('pipeline_id', process.env.CI_PIPELINE_ID)
    .stringField('job_id', process.env.CI_JOB_ID)
    .stringField('failure_exception', test.failure ? test.failure[0].message : '')
    .timestamp(new Date(suite.timestamp));
};

const formatSuiteName = name => {
  return name.replace('com.gitlab.plugin.e2eTest.tests.', '').replace('$', '_');
};

const handleWritingErrors = error => {
  console.error('Error while writing to InfluxDB:');
  if (error instanceof HttpError) {
    console.error(`HTTP Error (${error.statusCode}): ${error.statusMessage}`);
  } else if (error instanceof RequestTimedOutError) {
    console.error(`Timeout Error: ${error.message}`);
  } else {
    console.error(`Unexpected Error: ${error.message}`);
    console.error(`Stack trace: ${error.stack}`);
  }
  exit(1);
};

async function main() {
  validateInfluxToken();
  validateResultsDirectory();

  const client = new InfluxDB({
    url: INFLUXDB.url,
    token: INFLUXDB.token,
  });

  const writeClient = client.getWriteApi(INFLUXDB.org, INFLUXDB.bucket, 'ns');

  const xmlFiles = getTestSuiteXmlFiles();
  const suites = await parseTestSuites(xmlFiles);
  console.log(`Found ${suites.length} test suites which have test results.`);

  const isMergeRequest = process.env.CI_MERGE_REQUEST_IID ? 'true' : 'false';

  suites.forEach(suite => {
    console.log(`Found ${suite.testcase.length} tests for suite: "${suite.name}"`);

    suite.testcase.forEach(test => {
      const point = createTestResultPoint(suite, test, isMergeRequest);
      if (point) {
        writeClient.writePoint(point);
      }
    });
  });

  try {
    await writeClient.close();
    console.log('Write to InfluxDB completed successfully.');
  } catch (error) {
    handleWritingErrors(error);
  }
}

await main();
