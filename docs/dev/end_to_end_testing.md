# GitLab Duo JetBrains plugin end-to-end tests

Utilises [intellij-ui-test-robot](https://github.com/JetBrains/intellij-ui-test-robot) to test the GitLab Duo JetBrains plugin using a real
instance of IntelliJ. These tests will launch a version of IntelliJ with the GitLab Duo plugin installed from source.

The `platform.version` and `platform.type` set in [gradle.properties](../../gradle.properties) determine which IntelliJ instance is tested.

## Executing tests

To run the end-to-end tests the project property `e2eTests` must be set. By doing this, we execute only the e2e tests and exclude all other tests.
The E2E tests use a [test suite file](../../src/test/kotlin/com/gitlab/plugin/e2eTest/suites/E2ETestSuite.kt), to ensure unauthenticated tests
are performed first, followed by authenticated tests.

Prerequisites:

- The token's user has Code Suggestions and GitLab Duo Chat enabled.
- The E2E tests require these environment variables to be set:
  - `TEST_GITLAB_HOST` (defaults to `https://staging.gitlab.com`)
  - `TEST_ACCESS_TOKEN`

Firstly, launch the IDE for testing, run the `runIdeForUiTests` gradle task, as it is blocking it must be run in a separate process to the tests:

```shell
./gradlew runIdeForUiTests
```

### Run tests from command line

```shell
TEST_GITLAB_HOST="https://staging.gitlab.com" TEST_ACCESS_TOKEN="<insert_token>" ./gradlew -Pe2eTests test --tests E2ETestSuite
```

Using `cleanTest` ensures the tests are run even if they are up-to-date:

```shell
TEST_GITLAB_HOST="https://staging.gitlab.com" TEST_ACCESS_TOKEN="<insert_token>" ./gradlew -Pe2eTests cleanTest test --tests E2ETestSuite
```

To launch IntelliJ and run the end-to-end tests in one command:

```shell
TEST_GITLAB_HOST="https://staging.gitlab.com" TEST_ACCESS_TOKEN="<insert_token>" ./gradlew clean :runIdeForUiTests & ./gradlew -Pe2eTests cleanTest test --tests E2ETestSuite
```

### Run tests from IntelliJ

Update the `Run E2E Tests` gradle run configuration with your `TEST_GITLAB_HOST` and `TEST_ACCESS_TOKEN`.

To execute all e2e tests:

1. Execute `Run IDE for UI Tests`
1. Execute `Run E2E Tests`

To execute a single test you will need to update the `run` text field of `Run E2E Tests` like this.

```shell
:test --tests "com.gitlab.plugin.e2eTest.tests.DuoChatTest" -Pe2eTests
```

## Debug end-to-end test failures

If the `plugin:e2eTest` job fails there are a number of test artifacts produced which will help in determining the cause of the failure.
Also check [Scheduled pipelines](#scheduled-pipelines) to see if the failure is occurring in `main` also.

### Test report

The human-readable test report can be found in the `test-report` directory by opening `index.html`.
It will highlight any tests that have failed and the associated test stack trace. There is also a link to this report from the `plugin:e2eTest` job.

### Screenshots

For each test failure, there will be an associated screenshot in the `failure-screenshots` directory.
There will also be an HTML representation of the IntelliJ application hierarchy at the time of failure.

### Video recordings

Video recordings of test failures are saved in the `video` directory for failed tests. VLC player or equivalent may be required to play the video.

### IDEA logs

The `idea-logs` directory contains the `idea.log` for the entire test run. This allows us to view any application errors that occurred during the test run.

## Scheduled pipelines

We currently run the latest version of the plugin against `staging.gitlab.com` and `gitlab.com` daily. These results can help determine whether a failure in a merge request pipeline is also occurring against `main`.
A link to the latest results for the daily test run can be found at:

[GitLab Duo Plugin for JetBrains / Schedules](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/pipeline_schedules)

## Creating end-to-end tests

Since end-to-end testing is slower and requires more resources, do not write an end-to-end test if a lower-level unit test can cover the functionality.

End-to-end tests should ensure that the key workflows of the plugin, are functioning correctly when integrated with a real GitLab instance.

To ensure the plugin works as expected when features flags are enabled and disabled, consider adding an end-to-end test to cover both options.

### Testing with feature flags/settings

Plugin feature flags and settings can be modified from the `Settings | Tools | GitLab Duo` menu. If a test requires a feature to be
enabled or a setting to be modified it is recommended to `openDuoSettings()` and make modifications in the `BeforeAll` function
of the test.

Each test should also check that the application is in the required state before proceeding. For example, if the plugin is required
to be authenticated, `assert(isDuoEnabled())` can be used.

### Test ordering

In the interest of reducing test duration, the end-to-end tests are run in the order defined in [E2ETestSuite.kt](../../src/test/kotlin/com/gitlab/plugin/e2eTest/suites/E2ETestSuite.kt).

This allows for unauthenticated tests to be executed before authenticated tests, so the IDE does not need to be restarted.

## FAQs

### Why on my Mac is the UI click not working?

On Mac computers, you will need to allow the IDE to control it:
```Security & Privacy``` -> ```Accessibility``` -> ```Allow the apps below to control your computer``` and select the
IDE you are working with.
