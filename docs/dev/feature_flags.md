# Feature flags

We use feature flags to control code availability across different environments.

## Example use case

To keep the plugin deployable while working on a feature that is not ready for release, we might add a feature flag for only local builds.
Upon completion, we would update the feature flag to add alpha releases.
We would then remove the flag once we are satisfied the feature is ready for stable release.

## Limitations

Our current implementation requires code changes for flag updates (such as, no real-time changes without a deploy) and only offers environment-level control.

## Implementation

We use the [`gradle-buildconfig-plugin`](https://github.com/gmazzo/gradle-buildconfig-plugin) to define feature flags as constants at build time in [build.gradle.kts](../../build.gradle.kts).

Set feature flags in the `buildConfig` block:

Example:

```kotlin
buildConfig {
  buildConfigField(
    "Boolean",
    "QUICK_CHAT_ENABLED",
    isLocalBuild || releaseChannel == alphaReleaseChannel || quickChatEnabledEnv.toBoolean()
  )
}
```

On build, this adds a constant to the automatically generated `BuildConfig` object.

The flag is now usable in the code:

```kotlin
import com.gitlab.plugin.BuildConfig

if (BuildConfig.QUICK_CHAT_ENABLED) {
  // Feature-specific code
}
```

## Enabling flags for different environments

### Local builds

Use the `isLocalBuild` environment config variable:

```kotlin
buildConfigField(
  "Boolean",
  "QUICK_CHAT_ENABLED",
  isLocalBuild
)
```

### Alpha releases

Use the `releaseChannel` environment config variable:

```kotlin
buildConfigField(
  "Boolean",
  "QUICK_CHAT_ENABLED",
  releaseChannel == alphaReleaseChannel
)
```

### E2E tests (or other jobs) in pipeline

1. Add a variable to `.gitlab-ci.yml`:

   ```yaml
   plugin:e2eTest:
     variables:
       QUICK_CHAT_ENABLED: true
   ```

1. Add a property to the `EnvConfig` class:

   ```kotlin
   inner class EnvConfig {
     private val quickChatEnabledEnv by lazy { System.getenv("QUICK_CHAT_ENABLED") ?: "" }
   }
   ```

1. Use in `buildConfigField`:

   ```kotlin
   buildConfigField(
     "Boolean",
     "QUICK_CHAT_ENABLED",
     quickChatEnabledEnv.toBoolean()
   )
   ```

For a complete example, see [merge request 951](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/951/diffs#dbcff70658daf80b53ce624f6adcaa529df5ed8d).
