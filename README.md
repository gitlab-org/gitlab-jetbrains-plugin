# GitLab Duo Plugin for JetBrains IDEs

[![Marketplace Version](https://img.shields.io/jetbrains/plugin/v/22325-gitlab-duo)](https://plugins.jetbrains.com/plugin/22325-gitlab-duo/versions)

[This plugin](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin) integrates GitLab Duo with
JetBrains IDEs (IntelliJ, PyCharm, GoLand, Webstorm, Rubymine, etc). It supports Duo Code Suggestions and Duo Chat.

With Duo Code Suggestions, you get:

- **Code completion**, which suggests completions for the current line you are typing.
  These suggestions usually have low latency.
- **Code generation**, which generates code based on a natural-language code comment block.
  Responses for code generation are slower than code completion, and returned in a single block.

With Duo Chat, you get the ability to ask general or GitLab specific questions in the main chat window, as well as the following slash commands:

- **Explain code**, which provides an explanation of selected code snippets from your editor.
- **Refactor code**, which provides refactoring suggestions for selected code snippets from your editor.
- **Generate tests**, which provides test generation suggestions for selected code snippets from your editor.
- **Fix code**, which provides quick fixes for selected code snippets from your editor.

To learn more, see:

- [Code Suggestions documentation](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/)
- [Supported languages](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html#supported-languages)

## Version compatibility

This extension requires:

- JetBrains IDEs: 2023.3 and later.
- GitLab version 16.8 or later.

For version compatibility between versions of the plugin, IDE, and GitLab, check the version compatibility list
on the marketplace listing. It contains a table of plugin versions and their
[supported IDE versions](https://plugins.jetbrains.com/plugin/22325-gitlab-duo/versions).

## Setup

For instructions, see [Configure the extension](https://docs.gitlab.com/ee/editor_extensions/jetbrains_ide/#configure-the-extension).

## Roadmap

To learn more about this project's team, processes, and plans, see
the [Create:Editor Extensions Group](https://handbook.gitlab.com/handbook/engineering/development/dev/create/editor-extensions/)
page in the GitLab handbook.

For a list of all open issues in this project, see the
[issues page](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/)
for this project.

## Troubleshooting

See [JetBrains troubleshooting](https://docs.gitlab.com/ee/editor_extensions/jetbrains_ide/jetbrains_troubleshooting.html)
in the GitLab documentation. If those instructions don't solve your problem, see
[how to report an issue with the plugin](https://docs.gitlab.com/ee/editor_extensions/jetbrains_ide/#report-issues-with-the-plugin).

## Contributing

This extension is open source and [hosted on GitLab](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin). Contributions are more than welcome and subject to the terms set forth in [CONTRIBUTING](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/blob/main/CONTRIBUTING.md) -- feel free to fork and add new features or submit bug reports. See [CONTRIBUTING](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/blob/main/CONTRIBUTING.md) for more information.
