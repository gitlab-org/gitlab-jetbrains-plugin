const pluginVue = require('eslint-plugin-vue');

module.exports = [
  ...pluginVue.configs['flat/recommended'],
  {
    ignores: ['node_modules', 'dist'],
    files: [ '**/*.vue', '**/*.js', '**/*.jsx', '**/*.cjs', '**/*.mjs'],
  }
];
